-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Mar 11, 2018 at 02:03 PM
-- Server version: 10.1.25-MariaDB
-- PHP Version: 5.6.31

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `chutti`
--

-- --------------------------------------------------------

--
-- Table structure for table `city`
--

CREATE TABLE `city` (
  `city_id` int(11) NOT NULL,
  `city_name` varchar(75) NOT NULL,
  `state_id` int(11) NOT NULL,
  `flag` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0-not delete,  1-delete',
  `status` tinyint(1) NOT NULL DEFAULT '1' COMMENT '1-active, 0-inactive',
  `added_by` int(11) NOT NULL,
  `created_date` datetime NOT NULL,
  `modified_date` datetime DEFAULT NULL,
  `ip_address` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `city`
--

INSERT INTO `city` (`city_id`, `city_name`, `state_id`, `flag`, `status`, `added_by`, `created_date`, `modified_date`, `ip_address`) VALUES
(1, 'mumbai', 21, 0, 1, 0, '2018-03-04 16:35:56', '2018-03-04 16:35:56', ''),
(2, 'pune', 21, 0, 1, 0, '2018-03-04 16:36:21', '2018-03-04 16:36:21', ''),
(3, 'thane', 21, 0, 1, 0, '2018-03-04 16:36:39', '2018-03-04 16:36:39', ''),
(4, 'panvel', 21, 0, 1, 0, '2018-03-04 16:37:03', '2018-03-04 16:37:03', ''),
(5, 'indore', 20, 0, 1, 0, '2018-03-04 16:37:39', '2018-03-04 16:37:39', '');

-- --------------------------------------------------------

--
-- Table structure for table `facilities`
--

CREATE TABLE `facilities` (
  `facility_id` int(11) NOT NULL,
  `facility_name` varchar(100) NOT NULL,
  `flag` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0-not delete,  1-delete',
  `status` tinyint(1) NOT NULL DEFAULT '1' COMMENT '1-active, 0-inactive',
  `added_by` int(11) NOT NULL,
  `created_date` datetime NOT NULL,
  `modified_date` datetime DEFAULT NULL,
  `ip_address` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `facilities`
--

INSERT INTO `facilities` (`facility_id`, `facility_name`, `flag`, `status`, `added_by`, `created_date`, `modified_date`, `ip_address`) VALUES
(1, 'swimming pool', 0, 1, 0, '2018-03-08 23:05:02', '2018-03-08 23:05:02', ''),
(2, 'gym', 0, 1, 0, '2018-03-08 23:05:29', '2018-03-08 23:05:29', ''),
(3, 'car parking', 0, 1, 0, '2018-03-11 16:20:21', '2018-03-11 16:20:21', ''),
(4, 'garden', 0, 1, 0, '2018-03-11 16:20:35', '2018-03-11 16:20:35', ''),
(5, 'spa', 0, 1, 0, '2018-03-11 16:20:54', '2018-03-11 16:20:54', ''),
(6, 'pet friendly', 0, 1, 0, '2018-03-11 16:21:20', '2018-03-11 16:21:20', '');

-- --------------------------------------------------------

--
-- Table structure for table `groups`
--

CREATE TABLE `groups` (
  `id` mediumint(8) UNSIGNED NOT NULL,
  `name` varchar(20) NOT NULL,
  `description` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `hotels`
--

CREATE TABLE `hotels` (
  `hotel_id` int(11) NOT NULL,
  `hotel_name` varchar(100) NOT NULL,
  `highlights` text NOT NULL,
  `price` decimal(10,2) DEFAULT NULL,
  `style` varchar(255) NOT NULL,
  `setting` varchar(255) NOT NULL,
  `flag` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0-not delete,  1-delete',
  `status` tinyint(1) NOT NULL DEFAULT '1' COMMENT '1-active, 0-inactive',
  `city_id` int(11) NOT NULL,
  `added_by` int(11) NOT NULL,
  `created_date` datetime NOT NULL,
  `modified_date` datetime DEFAULT NULL,
  `ip_address` varchar(255) NOT NULL,
  `extras` varchar(100) NOT NULL,
  `offers` text NOT NULL,
  `favourite_rooms` text NOT NULL,
  `number_of_views` int(11) NOT NULL,
  `package_id` varchar(50) NOT NULL,
  `facilities_ids` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `hotels`
--

INSERT INTO `hotels` (`hotel_id`, `hotel_name`, `highlights`, `price`, `style`, `setting`, `flag`, `status`, `city_id`, `added_by`, `created_date`, `modified_date`, `ip_address`, `extras`, `offers`, `favourite_rooms`, `number_of_views`, `package_id`, `facilities_ids`) VALUES
(1, 'Verdura Resort', '<p>zdfzdgfxdfggfxdgxdgdgdxg</p>\r\n', '12.00', 'Modern Polizzi hideaway', 'Southern Sicily shoreline', 0, 1, 5, 0, '2018-03-08 17:50:13', '2018-03-08 17:50:16', '', 'Two drinks in the Granita bar', '<p>dgdgsdgxdgxfdgbxfgbfxbx</p>\r\n', '<p>gsdgsdgxdfvbxcvxcgbsdrfhgsrthgsrghsrth</p>\r\n', 0, '6', '0'),
(2, 'Maia Luxury Resort & Spa', '<ul>\r\n	<li>Incredible setting</li>\r\n	<li>Wow-factor facilities</li>\r\n	<li>Huge overwater villas</li>\r\n</ul>\r\n\r\n<p>&lsquo;No news, no shoes&rsquo;: that&rsquo;s the only rule at Soneva Jani, though you could add &lsquo;no blues&rsquo; (of the emotional kind) to the formula, too. Actual blues abound, from sky to sea, in every conceivable hue of melting Maldivian aqua.</p>\r\n', '12000.00', 'Wholly holistic happy hideaway', 'Thatched villas with bay views', 0, 1, 2, 0, '2018-03-08 18:46:38', '2018-03-08 18:46:38', '', 'A 60-minute Gentle Soul Massage each', '<ul>\r\n	<li><a href=\"https://www.mrandmrssmith.com/luxury-hotels/soneva-jani/offers/31137/31137-stay-7-pay-5#hotelroom-list-rooms-container\">Stay 7, pay 5</a></li>\r\n	<li><a href=\"https://www.mrandmrssmith.com/luxury-hotels/soneva-jani/offers/31138/31138-stay-4-pay-3#hotelroom-list-rooms-container\">Stay 4, pay 3</a></li>\r\n</ul>\r\n', '<p>There are simply no duds in this bunch: every single one of these palatial overwater villas is fit for Neptune and his gang. Follow this simple rule: opt for a room with a water slide or add one on. We promise you, flipper on heart, that it&rsquo;s more than worth the additional US$200 daily supplement. Bombs away!</p>\r\n', 0, '4', '0'),
(3, 'Kokomo Private Island Fiji', '<ul>\r\n	<li>Unspoiled private island</li>\r\n	<li>Extensive activities</li>\r\n	<li>Excellent for kids</li>\r\n</ul>\r\n\r\n<p>Kokomo Private Island is like a one-man band &ndash; only the music&#39;s some of the best you&rsquo;ve ever heard, spanning several genres without a single wrong note.</p>\r\n', '12000.00', 'All-out private isle', 'Right on the reef’s edge', 0, 1, 5, 0, '2018-03-11 12:45:56', '2018-03-11 12:45:56', '', 'For singles or couples, a sundowner cocktail each at Walker D\'Plank. For families, a sunset cruise', '<ul>\r\n	<li><a href=\"https://www.mrandmrssmith.com/luxury-hotels/kokomo-private-island-fiji/offers/31522/31522-stay-7-pay-5#hotelroom-list-rooms-container\">Stay 7 Pay 5</a></li>\r\n	<li><a href=\"https://www.mrandmrssmith.com/luxury-hotels/kokomo-private-island-fiji/offers/31523/31523-stay-10-pay-7#hotelroom-list-rooms-container\">Stay 10 Pay 7</a></li>\r\n	<li><a href=\"https://www.mrandmrssmith.com/luxury-hotels/kokomo-private-island-fiji/offers/31524/31524-stay-14-pay-10#hotelroom-list-rooms-container\">Stay 14 Pay 10</a></li>\r\n	<li><a href=\"https://www.mrandmrssmith.com/luxury-hotels/kokomo-private-island-fiji/offers/33849/33849-stay-5-pay-4#hotelroom-list-rooms-container\">Stay 5 Pay 4</a></li>\r\n</ul>\r\n', '<p>At Kokomo, even the entry-level One-Bedroom Villas are impressive, with generously sized living areas, sleek indoor/outdoor bathrooms, private pools and direct beach access. For an unforgettable experience for the whole family, you could plump for one of the luxury residences. Containing between three and six bedrooms, these villas occupy elevated spots among the island&#39;s lush vegetation, and have large, open-plan living areas and impressive views.&nbsp;</p>\r\n', 0, '7', ''),
(4, 'Soneva Fushi', '<ul>\r\n	<li>Rustic hideaway villas</li>\r\n	<li>The fabulous spa</li>\r\n	<li>House-reef snorkelling</li>\r\n</ul>\r\n\r\n<p>Boutique hotel Soneva Fushi&#39;s eco-chic villas in the Maldives are the stuff of Robinson Crusoe fantasies &ndash; only with an extra splash of mega-luxury</p>\r\n', '12000.00', 'Wholly holistic happy hideaway', 'Thatched villas with bay views', 0, 1, 2, 0, '2018-03-11 12:13:52', '2018-03-11 12:13:52', '', 'A 60-minute Gentle Soul Massage each', '<ul>\r\n	<li><a href=\"https://www.mrandmrssmith.com/luxury-hotels/soneva-fushi-1/offers/30089/30089-stay-4-pay-3#hotelroom-list-rooms-container\">Stay 4 Pay 3</a></li>\r\n	<li><a href=\"https://www.mrandmrssmith.com/luxury-hotels/soneva-fushi-1/offers/30090/30090-stay-7-pay-5#hotelroom-list-rooms-container\">Stay 7 Pay 5</a></li>\r\n</ul>\r\n', '<p>Sunsets may be spectacular at Soneva Fushi, but villas on the sunrise side of the island are blessed with uninterrupted views of the ocean. All have an indoor-outdoor feel, with garden bathrooms, cosy outdoor seating areas, and rustic-chic interiors of wood and crisp cottons. There&rsquo;s no communal pool on the island, so make sure you splash out on a villa with a private seawater pool. The one at Retreat Villa (61) extends into the shade of the second-floor open air dining area connecting the two bedroom suites of this sprawling hideaway. The master outdoor bathroom is equally impressive: with a pond, sunken bathtub, steam room and a small private gym, it may well tempt you away from the decadent spa.</p>\r\n', 0, '4', '0'),
(5, 'Emirates One&Only Wolgan Valley', '<ul>\r\n	<li>Native bushland and iconic Blue Mountains escarpment as far as the eye can see</li>\r\n	<li>Emirates&#39; signature Timeless Spa &ndash; for an hour, a morning or the whole day</li>\r\n	<li>Purpose-built equestrian centre with top-notch nags</li>\r\n</ul>\r\n\r\n<p>Emirates One&amp;Only Wolgan Valley takes luxury lodging to a canyon-sized Jurassic Park of sun-blessed escarpments, creature comforts and sumptuous styling.</p>\r\n', '12000.00', 'Outback ultra-luxe', 'No expanse spared', 0, 1, 5, 0, '2018-03-11 12:29:27', '2018-03-11 12:33:57', '', 'A bottle of local Orange or Mudgee wine on arrival', '<ul>\r\n	<li><a href=\"https://www.mrandmrssmith.com/luxury-hotels/emirates-oneandonly-wolgan-valley/offers/25150/25150-romantic-retreat#hotelroom-list-rooms-container\">Romantic retreat</a></li>\r\n	<li><a href=\"https://www.mrandmrssmith.com/luxury-hotels/emirates-oneandonly-wolgan-valley/offers/27531/27531-stay-3-nights-and-save#hotelroom-list-rooms-container\">Stay 3 nights and save</a></li>\r\n	<li><a href=\"https://www.mrandmrssmith.com/luxury-hotels/emirates-oneandonly-wolgan-valley/offers/25493/25493-wolgan-retreat-offer#hotelroom-list-rooms-container\">Wolgan retreat offer</a></li>\r\n	<li><a href=\"https://www.mrandmrssmith.com/luxury-hotels/emirates-oneandonly-wolgan-valley/offers/29090/29090-family-escape-offer#hotelroom-list-rooms-container\">Family Escape Offer</a></li>\r\n	<li><a href=\"https://www.mrandmrssmith.com/luxury-hotels/emirates-oneandonly-wolgan-valley/offers/29111/29111-mountain-discovery-offer#hotelroom-list-rooms-container\">Mountain Discovery Offer</a></li>\r\n	<li><a href=\"https://www.mrandmrssmith.com/luxury-hotels/emirates-oneandonly-wolgan-valley/offers/31997/31997-country-escape#hotelroom-list-rooms-container\">Country Escape</a></li>\r\n</ul>\r\n', '<p>A modern take on Federation-style bungalows (with stained-glass door panels teamed with natural stone, timber and fabrics), all Wolgan&rsquo;s villas sit demurely in a long, non-view-hogging line. Ask for one-bedroom Heritage Villas 17 or 41 at either end of the &lsquo;street&rsquo; for maximum privacy. The three secluded two-bedroom Wollemi Villas and the sole three-bedroom Wolgan Villa sit up the hill closer to the lodge, affording greater wildlife-watching opportunities.</p>\r\n', 0, '5', '0'),
(6, 'Amilla Fushi', '<ul>\r\n	<li>Intriguing architecture</li>\r\n	<li>Fine dining from chef Luke Mangan</li>\r\n	<li>Private infinity pools</li>\r\n</ul>\r\n\r\n<p>Luxury resort Amilla Fushi shakes up Maldivian style, with a raft of ultra-modern houses and residences blessed with private pools, including a treehouse. Superlative dining adds to its contemporary charm.</p>\r\n', '12000.00', 'Thoroughly modern Maldives', 'Blue blue Baa Atoll', 0, 1, 5, 0, '2018-03-11 12:51:38', '2018-03-11 12:51:38', '', 'A cookery lesson with one of the resort\'s talented chefs. GoldSmiths get an additional US$100 resort', '<ul>\r\n	<li><a href=\"https://www.mrandmrssmith.com/luxury-hotels/amilla-fushi/offers/32805/32805-stay-longer-and-save#hotelroom-list-rooms-container\">Stay Longer &amp; Save</a></li>\r\n	<li><a href=\"https://www.mrandmrssmith.com/luxury-hotels/amilla-fushi/offers/32806/32806-stay-longer-and-save#hotelroom-list-rooms-container\">Stay Longer &amp; Save</a></li>\r\n	<li><a href=\"https://www.mrandmrssmith.com/luxury-hotels/amilla-fushi/offers/32807/32807-stay-longer-and-save#hotelroom-list-rooms-container\">Stay Longer &amp; Save</a></li>\r\n	<li><a href=\"https://www.mrandmrssmith.com/luxury-hotels/amilla-fushi/offers/32808/32808-stay-longer-and-save#hotelroom-list-rooms-container\">Stay Longer &amp; Save</a></li>\r\n	<li><a href=\"https://www.mrandmrssmith.com/luxury-hotels/amilla-fushi/offers/32809/32809-stay-longer-and-save#hotelroom-list-rooms-container\">Stay Longer &amp; Save</a></li>\r\n	<li><a href=\"https://www.mrandmrssmith.com/luxury-hotels/amilla-fushi/offers/32810/32810-stay-longer-and-save#hotelroom-list-rooms-container\">Stay Longer &amp; Save</a></li>\r\n	<li><a href=\"https://www.mrandmrssmith.com/luxury-hotels/amilla-fushi/offers/34419/34419-festive-early-bird-2018#hotelroom-list-rooms-container\">Festive Early Bird 2018</a></li>\r\n	<li><a href=\"https://www.mrandmrssmith.com/luxury-hotels/amilla-fushi/offers/34420/34420-festive-early-bird-2018#hotelroom-list-rooms-container\">Festive Early Bird 2018</a></li>\r\n	<li><a href=\"https://www.mrandmrssmith.com/luxury-hotels/amilla-fushi/offers/34421/34421-festive-early-bird-2018#hotelroom-list-rooms-container\">Festive Early Bird 2018</a></li>\r\n</ul>\r\n', '<p>Shaded from view by palm fronds and dressed in shades of aquamarine, gold and navy, the Maldives&rsquo; first luxury stilted treehouse is a step above the rest (sorry); its private pool, hammock and barbecue make it a must-stay. Those eager to befriend the marine life should check in to an Ocean Reef House, which has direct access to world-class snorkelling, alongside an infinity pool and terrace with super sunset views.</p>\r\n', 0, '1', ''),
(7, 'Cayo Espanto', '<ul>\r\n	<li>Private island in the Caribbean Sea</li>\r\n	<li>Exclusive personal butler service</li>\r\n	<li>Totally bespoke food, drink, music, and activities</li>\r\n</ul>\r\n\r\n<p>Embraced by the topaz waters of the Caribbean, Cayo Espanto tops the A-list when it comes to bespoke boutique breaks, with butler-serviced wooden villas snuggled among the palms of a tiny island.</p>\r\n', '12000.00', 'Private island beach houses', 'Caribbean coast of Ambergris Caye', 0, 1, 3, 0, '2018-03-11 12:21:29', '2018-03-11 12:21:55', '', 'A massage each', '<p>dadadad</p>\r\n', '<p>Two-floored, two-bedroomed Casa Estrella captures the feeling of staying in a glamour-drenched tree house, with open-plan living areas, cream-cushioned wicker furniture and a panoramic ocean view. Floor-length turquoise shutters line three of the house&rsquo;s four walls, allowing you to configure the balance of inside/outside space as you see fit. Casa Aurora has the largest pool, and smaller, more private Casa Ventana is set out to sea on its own wooden pontoon.</p>\r\n', 0, '2', '0'),
(8, 'Pretty Beach House', '<ul>\r\n	<li>Secluded forest setting</li>\r\n	<li>All-inclusive fare</li>\r\n	<li>Outdoorsy fun in Bouddi National Park</li>\r\n	<li>\r\n	<p>Bouddi National Park&acirc;&euro;&trade;s ancient forests surround hilltop guesthouse Pretty Beach House, with views to Broken Bay. The natural beauty is bolstered by excellent all-inclusive cuisine and secluded pavilions.</p>\r\n	</li>\r\n</ul>\r\n', '12000.00', 'Epicure-enticing guesthouse', 'Eucalyptus-scented bushland', 0, 1, 5, 0, '2018-03-11 07:49:24', '2018-03-11 07:49:24', '', ' A welcome glass each of French Champagne on arrival', '<p>reraszr</p>\r\n', '<p>Shielded by gardens and gum trees, the Hideaway Pavilion lives up to its name as a secluded den decked out in natural fabrics, recycled timbers, bespoke furnishings and local art. For more elbow-room, check in to the Retreat, a two-floor stay with his &rsquo;n&rsquo; hers bathrooms.</p>\r\n', 0, '9', '0'),
(9, 'Nay Palad Hideaway', '<ul>\r\n	<li>All-inclusive idyll</li>\r\n	<li>Pristine beach</li>\r\n	<li>Unbeatable surfing</li>\r\n</ul>\r\n\r\n<p>Verdant forests, azure waters and a powder-soft stretch of private sands: boutique resort Nay Palad Hideaway ticks all the right boxes for a romantic getaway.</p>\r\n', '12000.00', 'Crafty weaver’s nest', 'Pacific playground', 0, 1, 5, 0, '2018-03-08 18:58:36', '2018-03-08 18:58:54', '', 'A piece of fibre jewellery for each guest, made by the Compostela Project', '<p>sdaDzSdf</p>\r\n', '<p>Set in glorious tropical gardens, the resort&rsquo;s villas showcase Nay Palad Hideaway&rsquo;s origins as a maker of iconic outdoor furniture. Everything here, from the hanging day-beds to the sculptural carved headboards, is handcrafted from local materials. All the villas are soothing and airy, with indoor-outdoor showers and a front glass wall to let in the light; opt for one of the Deluxe Villas for unbeatable ocean views.</p>\r\n', 0, '8', '0');

-- --------------------------------------------------------

--
-- Table structure for table `images`
--

CREATE TABLE `images` (
  `image_id` int(11) NOT NULL,
  `image_name` varchar(255) NOT NULL,
  `hotel_id` int(25) NOT NULL,
  `flag` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0-not delete,  1-delete',
  `status` tinyint(1) NOT NULL DEFAULT '1' COMMENT '1-active, 0-inactive',
  `added_by` int(11) NOT NULL,
  `created_date` datetime NOT NULL,
  `modified_date` datetime DEFAULT NULL,
  `ip_address` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `images`
--

INSERT INTO `images` (`image_id`, `image_name`, `hotel_id`, `flag`, `status`, `added_by`, `created_date`, `modified_date`, `ip_address`) VALUES
(1, 'Chrysanthemum.jpg', 1, 1, 1, 0, '2018-03-08 17:08:47', '2018-03-08 17:16:47', ''),
(2, 'img1.jpg', 1, 0, 1, 0, '2018-03-08 17:08:47', '2018-03-08 17:49:40', ''),
(3, 'img2.jpg', 1, 0, 1, 0, '2018-03-08 17:50:38', '2018-03-08 17:50:38', ''),
(4, 'img3.jpg', 1, 0, 1, 0, '2018-03-08 17:50:38', '2018-03-08 17:50:38', ''),
(5, 'img4.jpg', 1, 0, 1, 0, '2018-03-08 17:50:38', '2018-03-08 17:50:38', ''),
(6, 'img5.jpg', 2, 0, 1, 0, '2018-03-08 17:53:19', '2018-03-08 17:53:19', ''),
(7, 'img6.jpg', 2, 0, 1, 0, '2018-03-08 17:53:19', '2018-03-08 17:53:19', ''),
(8, 'img7.jpg', 2, 0, 1, 0, '2018-03-08 17:53:19', '2018-03-08 17:53:19', ''),
(9, 'img8.jpg', 2, 0, 1, 0, '2018-03-08 17:53:19', '2018-03-08 17:53:19', ''),
(10, 'img9.jpg', 3, 0, 1, 0, '2018-03-08 17:55:58', '2018-03-08 17:55:58', ''),
(11, 'img10.jpg', 3, 0, 1, 0, '2018-03-08 17:55:58', '2018-03-08 17:55:58', ''),
(12, 'img11.jpg', 3, 0, 1, 0, '2018-03-08 17:55:58', '2018-03-08 17:55:58', ''),
(13, 'img12.jpg', 3, 0, 1, 0, '2018-03-08 17:55:58', '2018-03-08 17:55:58', ''),
(14, 'img13.jpg', 4, 0, 1, 0, '2018-03-08 18:48:56', '2018-03-08 18:48:56', ''),
(15, 'img21.jpg', 4, 0, 1, 0, '2018-03-08 18:48:56', '2018-03-08 18:48:56', ''),
(16, 'img31.jpg', 4, 0, 1, 0, '2018-03-08 18:48:56', '2018-03-08 18:48:56', ''),
(17, 'img41.jpg', 4, 0, 1, 0, '2018-03-08 18:48:56', '2018-03-08 18:48:56', ''),
(18, 'img51.jpg', 5, 0, 1, 0, '2018-03-08 18:52:11', '2018-03-08 18:52:11', ''),
(19, 'img61.jpg', 5, 0, 1, 0, '2018-03-08 18:52:11', '2018-03-08 18:52:11', ''),
(20, 'img71.jpg', 5, 0, 1, 0, '2018-03-08 18:52:11', '2018-03-08 18:52:11', ''),
(21, 'img81.jpg', 5, 0, 1, 0, '2018-03-08 18:52:11', '2018-03-08 18:52:11', ''),
(22, 'img91.jpg', 6, 0, 1, 0, '2018-03-08 18:54:00', '2018-03-08 18:54:00', ''),
(23, 'img101.jpg', 6, 0, 1, 0, '2018-03-08 18:54:00', '2018-03-08 18:54:00', ''),
(24, 'img111.jpg', 6, 0, 1, 0, '2018-03-08 18:54:00', '2018-03-08 18:54:00', ''),
(25, 'img121.jpg', 6, 0, 1, 0, '2018-03-08 18:54:00', '2018-03-08 18:54:00', ''),
(26, 'img14.jpg', 7, 0, 1, 0, '2018-03-08 18:56:13', '2018-03-08 18:56:13', ''),
(27, 'img22.jpg', 7, 0, 1, 0, '2018-03-08 18:56:13', '2018-03-08 18:56:13', ''),
(28, 'img32.jpg', 7, 0, 1, 0, '2018-03-08 18:56:13', '2018-03-08 18:56:13', ''),
(29, 'img42.jpg', 7, 0, 1, 0, '2018-03-08 18:56:13', '2018-03-08 18:56:13', ''),
(30, 'img92.jpg', 9, 0, 1, 0, '2018-03-08 18:59:15', '2018-03-08 18:59:15', ''),
(31, 'img102.jpg', 9, 0, 1, 0, '2018-03-08 18:59:15', '2018-03-08 18:59:15', ''),
(32, 'img112.jpg', 9, 0, 1, 0, '2018-03-08 18:59:15', '2018-03-08 18:59:15', ''),
(33, 'img122.jpg', 9, 0, 1, 0, '2018-03-08 18:59:15', '2018-03-08 18:59:15', ''),
(34, 'img113.jpg', 8, 0, 1, 0, '2018-03-11 07:50:02', '2018-03-11 07:50:02', ''),
(35, 'img93.jpg', 8, 0, 1, 0, '2018-03-11 07:50:02', '2018-03-11 07:50:02', '');

-- --------------------------------------------------------

--
-- Table structure for table `login_attempts`
--

CREATE TABLE `login_attempts` (
  `id` int(11) UNSIGNED NOT NULL,
  `ip_address` varchar(45) NOT NULL,
  `login` varchar(100) NOT NULL,
  `time` int(11) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `login_attempts`
--

INSERT INTO `login_attempts` (`id`, `ip_address`, `login`, `time`) VALUES
(1, '::1', 'admin@25chutti.com', 1520694798);

-- --------------------------------------------------------

--
-- Table structure for table `packages`
--

CREATE TABLE `packages` (
  `package_id` int(11) NOT NULL,
  `package_name` varchar(255) NOT NULL,
  `package_logo` varchar(255) NOT NULL,
  `flag` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0-not delete,  1-delete',
  `status` tinyint(1) NOT NULL DEFAULT '1' COMMENT '1-active, 0-inactive',
  `added_by` int(11) NOT NULL,
  `created_date` datetime NOT NULL,
  `modified_date` datetime DEFAULT NULL,
  `ip_address` varchar(255) NOT NULL,
  `hotel_ids` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `packages`
--

INSERT INTO `packages` (`package_id`, `package_name`, `package_logo`, `flag`, `status`, `added_by`, `created_date`, `modified_date`, `ip_address`, `hotel_ids`) VALUES
(1, 'Family-friendly hotels', '2.png', 0, 1, 0, '2018-03-08 09:04:38', '2018-03-08 09:04:38', '', ''),
(2, 'Hotels Available For Easter Weekend', '1.png', 0, 1, 0, '2018-03-08 09:05:49', '2018-03-08 09:05:49', '', ''),
(3, 'Child-friendly', '3.png', 0, 1, 0, '2018-03-08 09:06:12', '2018-03-08 09:06:12', '', ''),
(4, 'Budget boutique hotel', '4.png', 0, 1, 0, '2018-03-08 09:06:39', '2018-03-09 18:16:21', '', ''),
(5, 'City hotels and villas', '5.png', 0, 1, 0, '2018-03-08 09:07:15', '2018-03-08 09:07:15', '', ''),
(6, 'Honeymoon Hotels & Romantic Luxury Stays', '6.png', 0, 1, 0, '2018-03-08 09:08:10', '2018-03-08 09:08:10', '', ''),
(7, 'Gourmet hotel breaks', '7.png', 0, 1, 0, '2018-03-08 09:10:47', '2018-03-08 09:10:47', '', ''),
(8, 'Spa hotels', '10.png', 0, 1, 0, '2018-03-08 09:11:25', '2018-03-08 09:11:25', '', ''),
(9, 'Private pools', '9.png', 0, 1, 0, '2018-03-08 09:11:53', '2018-03-08 09:11:53', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `state`
--

CREATE TABLE `state` (
  `state_id` int(11) NOT NULL,
  `state_name` varchar(255) NOT NULL,
  `state_logo` varchar(255) NOT NULL,
  `flag` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0-delete, 1-not delete',
  `status` tinyint(1) NOT NULL DEFAULT '1' COMMENT '1-active, 0-inactive',
  `added_by` int(15) NOT NULL,
  `created_date` datetime NOT NULL,
  `modified_date` datetime DEFAULT NULL,
  `ip_address` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `state`
--

INSERT INTO `state` (`state_id`, `state_name`, `state_logo`, `flag`, `status`, `added_by`, `created_date`, `modified_date`, `ip_address`) VALUES
(1, 'Andaman and Nicobar Islands', '', 0, 1, 0, '2018-01-08 17:05:57', '2018-01-08 03:09:21', ''),
(2, 'Andhra Pradesh', '', 0, 1, 0, '2018-01-08 17:05:57', '2018-01-08 03:09:21', ''),
(3, 'Arunachal Pradesh', '', 0, 1, 0, '2018-01-08 17:05:57', '2018-01-08 03:09:21', ''),
(4, 'Assam', 'hotel71.jpg', 0, 1, 0, '2018-01-08 17:05:57', '2018-03-08 09:02:51', ''),
(5, 'Bihar', '', 0, 1, 0, '2018-01-08 17:05:57', '2018-01-08 03:09:21', ''),
(6, 'Chandigarh', '', 0, 1, 0, '2018-01-08 17:05:57', '2018-01-08 03:09:21', ''),
(7, 'Chhattisgarh', '', 0, 1, 0, '2018-01-08 17:05:57', '2018-01-08 03:09:21', ''),
(8, 'Dadra and Nagar Haveli', '', 0, 1, 0, '2018-01-08 17:05:57', '2018-01-08 03:09:21', ''),
(9, 'Daman and Diu', '', 0, 1, 0, '2018-01-08 17:05:57', '2018-01-08 03:09:21', ''),
(10, 'Delhi', 'hotel3.jpg', 0, 1, 0, '2018-01-08 17:05:57', '2018-03-08 09:00:47', ''),
(11, 'Goa', 'hotel6.jpg', 0, 1, 0, '2018-01-08 03:09:21', '2018-03-08 09:01:54', ''),
(12, 'Gujarat', 'hotel5.jpg', 0, 1, 0, '2018-01-08 03:09:21', '2018-03-08 09:01:29', ''),
(13, 'Haryana', '', 0, 1, 0, '2018-01-08 03:09:21', '2018-01-08 03:09:21', ''),
(14, 'Himachal Pradesh', 'hotel8.jpg', 0, 1, 0, '2018-01-08 03:09:21', '2018-03-08 09:03:25', ''),
(15, 'Jammu and Kashmir', '', 0, 1, 0, '2018-01-08 03:09:21', '2018-01-08 03:09:21', ''),
(16, 'Jharkhand', '', 0, 1, 0, '2018-01-08 03:09:21', '2018-01-08 03:09:21', ''),
(17, 'Karnataka', '', 0, 1, 0, '2018-01-08 03:09:21', '2018-01-08 03:09:21', ''),
(18, 'Kerala', 'hotel9.jpg', 0, 1, 0, '2018-01-08 03:09:21', '2018-03-08 09:03:47', ''),
(19, 'Lakshadweep', '', 0, 1, 0, '2018-01-08 03:09:21', '2018-01-08 03:09:21', ''),
(20, 'Madhya Pradesh', 'hotel2.jpg', 0, 1, 0, '2018-01-08 03:09:21', '2018-03-08 09:00:07', ''),
(21, 'Maharashtra', 'hotel1.jpg', 0, 1, 0, '2018-01-08 03:09:21', '2018-03-08 08:57:43', ''),
(22, 'Manipur', '', 0, 1, 0, '2018-01-08 03:09:21', '2018-01-08 03:09:21', ''),
(23, 'Meghalaya', '', 0, 1, 0, '2018-01-08 03:09:21', '2018-01-08 03:09:21', ''),
(24, 'Mizoram', '', 0, 1, 0, '2018-01-08 03:09:21', '2018-01-08 03:09:21', ''),
(25, 'Nagaland', '', 0, 1, 0, '2018-01-08 03:09:21', '2018-01-08 03:09:21', ''),
(26, 'Odisha', '', 0, 1, 0, '2018-01-08 03:09:21', '2018-01-08 03:09:21', ''),
(27, 'Puducherry', '', 0, 1, 0, '2018-01-08 03:09:21', '2018-01-08 03:09:21', ''),
(28, 'Punjab', '', 0, 1, 0, '2018-01-08 03:09:21', '2018-01-08 03:09:21', ''),
(29, 'Rajasthan', '', 0, 1, 0, '2018-01-08 03:09:21', '2018-01-08 03:09:21', ''),
(30, 'Sikkim', '', 0, 1, 0, '2018-01-08 03:09:21', '2018-01-08 03:09:21', ''),
(31, 'Tamil Nadu', '', 0, 1, 0, '2018-01-08 03:09:21', '2018-01-08 03:09:21', ''),
(32, 'Tripura', '', 0, 1, 0, '2018-01-08 03:09:21', '2018-01-08 03:09:21', ''),
(33, 'Uttar Pradesh', '', 0, 1, 0, '2018-01-08 03:09:21', '2018-01-08 03:09:21', ''),
(34, 'Uttarakhand', '', 0, 1, 0, '2018-01-08 03:09:21', '2018-01-08 03:09:21', ''),
(35, 'West Bengal', 'hotel4.jpg', 0, 1, 0, '2018-01-08 03:09:21', '2018-03-08 09:01:10', '');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) UNSIGNED NOT NULL,
  `ip_address` varchar(45) NOT NULL,
  `username` varchar(100) DEFAULT NULL,
  `password` varchar(255) NOT NULL,
  `salt` varchar(255) DEFAULT NULL,
  `email` varchar(254) NOT NULL,
  `activation_code` varchar(40) DEFAULT NULL,
  `forgotten_password_code` varchar(40) DEFAULT NULL,
  `forgotten_password_time` int(11) UNSIGNED DEFAULT NULL,
  `remember_code` varchar(40) DEFAULT NULL,
  `created_on` int(11) UNSIGNED NOT NULL,
  `last_login` int(11) UNSIGNED DEFAULT NULL,
  `active` tinyint(1) UNSIGNED DEFAULT NULL,
  `first_name` varchar(50) DEFAULT NULL,
  `last_name` varchar(50) DEFAULT NULL,
  `company` varchar(100) DEFAULT NULL,
  `phone` varchar(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `ip_address`, `username`, `password`, `salt`, `email`, `activation_code`, `forgotten_password_code`, `forgotten_password_time`, `remember_code`, `created_on`, `last_login`, `active`, `first_name`, `last_name`, `company`, `phone`) VALUES
(2, '127.0.0.1', 'administrator', '$2a$07$SeBknntpZror9uyftVopmu61qg0ms8Qv1yV6FG.kQOSM.9QhmTo36', '', 'admin@admin.com', '', NULL, NULL, NULL, 1268889823, 1520754503, 1, 'Admin', 'istrator', 'ADMIN', '0');

-- --------------------------------------------------------

--
-- Table structure for table `users_groups`
--

CREATE TABLE `users_groups` (
  `id` int(11) UNSIGNED NOT NULL,
  `user_id` int(11) UNSIGNED NOT NULL,
  `group_id` mediumint(8) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `city`
--
ALTER TABLE `city`
  ADD PRIMARY KEY (`city_id`);

--
-- Indexes for table `facilities`
--
ALTER TABLE `facilities`
  ADD PRIMARY KEY (`facility_id`);

--
-- Indexes for table `groups`
--
ALTER TABLE `groups`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `hotels`
--
ALTER TABLE `hotels`
  ADD PRIMARY KEY (`hotel_id`);

--
-- Indexes for table `images`
--
ALTER TABLE `images`
  ADD PRIMARY KEY (`image_id`);

--
-- Indexes for table `login_attempts`
--
ALTER TABLE `login_attempts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `packages`
--
ALTER TABLE `packages`
  ADD PRIMARY KEY (`package_id`);

--
-- Indexes for table `state`
--
ALTER TABLE `state`
  ADD PRIMARY KEY (`state_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users_groups`
--
ALTER TABLE `users_groups`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `uc_users_groups` (`user_id`,`group_id`),
  ADD KEY `fk_users_groups_users1_idx` (`user_id`),
  ADD KEY `fk_users_groups_groups1_idx` (`group_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `city`
--
ALTER TABLE `city`
  MODIFY `city_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `facilities`
--
ALTER TABLE `facilities`
  MODIFY `facility_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `groups`
--
ALTER TABLE `groups`
  MODIFY `id` mediumint(8) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `hotels`
--
ALTER TABLE `hotels`
  MODIFY `hotel_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `images`
--
ALTER TABLE `images`
  MODIFY `image_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=36;
--
-- AUTO_INCREMENT for table `login_attempts`
--
ALTER TABLE `login_attempts`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `packages`
--
ALTER TABLE `packages`
  MODIFY `package_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `state`
--
ALTER TABLE `state`
  MODIFY `state_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=36;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `users_groups`
--
ALTER TABLE `users_groups`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `users_groups`
--
ALTER TABLE `users_groups`
  ADD CONSTRAINT `fk_users_groups_groups1` FOREIGN KEY (`group_id`) REFERENCES `groups` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_users_groups_users1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
