var num = 200; //number of pixels before modifying styles

$(window).bind('scroll', function () {
    if ($(window).scrollTop() > num && !isMobile.any()) {
        $('.main-menu').attr('id','main-menu-id');
		$('.main-menu ul .menu-links').css('padding','23px 19px');
		$('#hidden-logo').html('<a href="'+base_url+'"><img src="'+base_url+'images/logo.png" width="132" height="50">');
		$('#form-col').addClass('partial-fixed');
		$('#form-col2').addClass('partial-fixed2');
		//$('.mobile-logo').css('display','inline');
		//$('#search').css('z-index','-1');
    } else {
        $('.main-menu').attr('id','');
		$('.main-menu ul li').css('padding','0px 30px');
		$('#hidden-logo').html('');
		$('#form-col').removeClass('partial-fixed');
		$('#form-col2').removeClass('partial-fixed2');
		$('.mobile-logo').hide();
		$('#search').css('z-index','0');
    }
});

$(window).bind('scroll', function () {
    if ($(window).scrollTop() > num && pathname == 'hotel-details') {
        $('.main-menu').hide();
		$('#form-col2').hide();
		
    } 
	else {
		$('.main-menu').show();
        $('#form-col2').show();
		
    }
});

var mum = 500;
var mum1 = 250;

$(window).bind('scroll', function () {
    if ($(window).scrollTop() > mum1 && isMobile.any()) {
		//$('.main-menu').show();
		$('.main-menu').addClass('fixed');
		$('.main-menu').css('display','block');
        $('.one-pager').addClass('sticky');
		
    } 
	else {
		//$('.main-menu').hide();
		$('.main-menu').removeClass('fixed');
		$('.one-pager').removeClass('sticky');
		
    }
});

$(window).bind('scroll', function () {
    if ($(window).scrollTop() > mum) {
		
        $('.one-pager').addClass('fixed');
		
    } 
	else {
		
		$('.one-pager').removeClass('fixed');
		
    }
});


function slideSwitch() {
    var $active = $('#slideshow IMG.active');
  
    if ( $active.length == 0 ) $active = $('#slideshow IMG:last');
  
    // use this to pull the images in the order they appear in the markup
    var $next =  $active.next().length ? $active.next()
        : $('#slideshow IMG:first');
  
    // uncomment the 3 lines below to pull the images in random order
  
    // var $sibs  = $active.siblings();
    // var rndNum = Math.floor(Math.random() * $sibs.length );
    // var $next  = $( $sibs[ rndNum ] );
  
    $active.addClass('last-active');
  
    $next.css({opacity: 0.0})
        .addClass('active')
        .animate({opacity: 1.0}, 1000, function() {
            $active.removeClass('active last-active');
        });
}
  
$(function() {
    setInterval( "slideSwitch()", 5000 );
});

var isMobile = {
	Android: function () {
		return navigator.userAgent.match(/Android/i);
	},
	BlackBerry: function () {
		return navigator.userAgent.match(/BlackBerry/i);
	},
	iOS: function () {
		return navigator.userAgent.match(/iPhone|iPad|iPod/i);
	},
	Opera: function () {
		return navigator.userAgent.match(/Opera Mini/i);
	},
	Windows: function () {
		return navigator.userAgent.match(/IEMobile/i) || navigator.userAgent.match(/WPDesktop/i);
	},
	any: function () {
		return (isMobile.Android() || isMobile.BlackBerry() || isMobile.iOS() || isMobile.Opera() || isMobile.Windows());
	}
};

// mobile menu slide from the left
$('[data-toggle="collapse-side"]').on('click', function() {
	//alert('myerror');
    $navMenuCont = $($(this).data('target'));
    $navMenuCont.animate({'width':'toggle'}, 350);
});


