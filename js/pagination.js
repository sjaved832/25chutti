apply_pagination_bar();
loadHotelData(0);

function update_no_of_records(value) {
    recordsToDisplay = Number(value);
    apply_pagination_bar();
    // var page_no = $("#page_no").val();
    loadHotelData(0);
}
function apply_pagination_bar(){
    $("#pagination_no").empty();
    for (var i = 0; i < totalRecords / recordsToDisplay;i++){
        $("#pagination_no").append("<li data='" + (i * recordsToDisplay) + "' class='pagination_click'>" + (i + 1) + "</li>");

    }
}
var hotelList =[];
function loadHotelData(page){
	
    $.ajax({
        url: base_url + "api/hotel_list?page=" + page + "&limit=" + recordsToDisplay + query,
        success : function(res){
            hotelList = res;
			//console.log(hotelList);
            renderHotelList(hotelList);
        }
    });
}
$(".pagination_click").on("click",function(env){
    var pageNumer = $(this).attr("data");
    loadHotelData(pageNumer);
});


function loadHotel(type) {
    var filteredHotel = hotelList.filter(function(hotelObj){
        return (hotelObj.type == type);
    });
    renderHotelList(filteredHotel);
}
function renderHotelList(hotelList){
    //console.log(hotelList);
    $('#hotelList').empty();
	
	$.each(hotelList,function(key, value)
	{
			var Element = "";
		Element += '<div class="row single">'+
						'<div class="col-md-6 col-sm-6 col-xs-12 imgs-div">'+
							'<div id="myCarousel'+key+'" class="carousel slide" data-ride="carousel">'+
								'<div class="carousel-inner">';
								$.each(value.images,function(key1, value1){
									Element += '<div class="item">' +
										'<img src="'+base_url+upload_url+"/"+value1.image_name+'"  class="img-responsive" style="width:100%;height:300px;">'+
									'</div>';
								});
							    Element += '</div><a class="left carousel-control" href="#myCarousel' + key + '" data-slide="prev">' +
									'<span class="glyphicon glyphicon-chevron-left"></span>'+
									'<span class="sr-only">Previous</span>'+
								'</a>'+
								'<a class="right carousel-control" href="#myCarousel'+key+'" data-slide="next">'+
								'<span class="glyphicon glyphicon-chevron-right"></span>'+
								'<span class="sr-only">Next</span>'+
								'</a>'+
							'</div>'+
						'</div>'+
						'<div class="col-md-6 col-sm-6 col-xs-12 text-div">'+
							'<div class="row basic-info">'+
								'<div class="col-md-8">'+
									'<p class="hotel-title-destination">'+
										'<span class="city">  '+value.city_name+'</span>,'+ 
										'<span class="country">'+value.state_name+'</span>'+'</p>'+                    
									'<a href="#">'+
										'<h2 class="hotel-title-hotelname">'+value.hotel_name+'</h2>'+                
									'</a>'+
								'</div>'+
								'<div class="col-md-4 text-right">'+
									'<p class="hotel-pricing-label text-right">'+
										'Total price'+                   
									'</p>'+
									'<a href="#">'+
										'<h2 class="hotel-pricing-rate"><i class="fa fa-inr"></i>&nbsp;'+value.price+'</h2>'+
									'</a>'+
								'</div>'+
							'</div>'+
							'<div class="row special-row">'+
								'<div class="col-md-2">'+
									'<p class="style">Style&nbsp;</p>'+
								'</div>'+
								'<div class="col-md-10">'+
									'<p class="hotel-maintext-setting">'+value.style+'</p>'+
								'</div>'+
								'<div class="col-md-2">'+
									'<p class="setting">Setting&nbsp;</p>'+
								'</div>'+
								'<div class="col-md-10">'+
									'<p class="hotel-maintext-style">'+value.setting+'</p>'+
								'</div>'+
							'</div>'+
							'<div class="row extra-row">'+
								'<div class="col-md-12">'+
									'<p class="hotel-maintext-extralabel"><span>25 Chutti Extra&nbsp;</span>--'+value.extras+
									'</p>'+  
								'</div>'+       
							'</div>'+
							'<div class="row">'+
								'<div class="col-md-6 col-xs-12 quick" data-visible="0">'+
									'<p>Quickview</p>'+
								'</div>'+
								'<div class="col-md-6 col-xs-12 view" onclick="goTo('+base_url+'hotel-details/'+value.hotel_id+')">'+
									'View Details'+
								'</div>'+
							'</div>'+
						'</div>'+
						'<div class="row quick-row">'+
							'<div class="col-md-4 col-sm-4 col-xs-12" style="background:#fff;">'+
								'<div class="offers-div" style="display: table-cell;">'+
									'<h3 class="offers-heading">Offers</h3>'+
										value.offers+
								'</div>'+
							'</div>'+
							'<div class="col-md-8 col-sm-4 col-xs-12" style="background:#fff;">'+
								'<div class="hotelcard-highlights-secondcol">'+
									'<h3 class="highlights-heading">Highlights</h3>'+
									value.highlights+
									'<h3 id="favrooms">Our favourite rooms</h3>'+
									'<p class="favrooms">'+value.favourite_rooms+'</p>'+
									'<h3 id="location">Location</h3>'+
									'<a href="#" class="location">'+value.city_name+','+value.state_name+'</a>'+
								'</div>'+
							'</div>'+
						'</div>'+
					'</div>';		
								console.log(Element);
					$('#hotelList').append(Element);
						//Yaha baaki ka hotel details ka
	});
     
}
