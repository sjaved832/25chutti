var options = {
    url: function (phrase) {
        return base_url + "api/search?query=" + phrase
    },
    list: {
        match: {
            enabled: true
        }
    },
	categories: [{ //Category hotels
			listLocation: "hotelList",
			header: "-- Hotels --"
		},
		{ //Category states
			listLocation: "stateList",
			header: "-- States --"
		}
	],
	getValue: "text",
	template: {
		type: "links",
		fields: {
			link: "link"
		}
	}
};
$("#search").easyAutocomplete(options);
$("#mobile-search").easyAutocomplete(options);

