        $.ajax({
				url: base_url+'api/state_list',
				type: "GET",
				dataType:'json',
				success:function(res)
				{
					//debugger;
					console.log(res.data);
					var statesElem = $("#states-div");
					var stateList = res.data;
					var html = "";
					var i = 0;
					var class_name = "";
					$.each(stateList , function (index, value)
					{
						if(i > 8){
							class_name = "hide_state";
						}
						if(value.state_logo != "")
						{
							html += '<div class="col-md-4 col-xs-12 offer-cols '+class_name+'">'+
											'<div class="thumbnail1">'+
												'<a href="#">'+
													'<img src="'+base_url+state_url+'/'+value.state_logo+'" class="img-responsive" width="352" height="230">'+
													'<div class="centered"><h4>Offers in '+value.state_name+'</h4></div>'+
												'</a>'+
											'</div>'+
										'</div>';
							i++;
						}
							
					});	
					statesElem.append(html);				
				}
        });
        function goTo(url)
		{
			alert(url);
			debugger;
			//alert(url);
			//location.href = url;   
		}
		
		/*script for fetching packages*/
		$.ajax({
        
			url: base_url+'api/package_list',
			type: "GET",
			dataType:'json',
			success:function(res)
			{
				
				console.log(res.data);
				var packagesElem = $("#packages-div");
				var packageList = res.data;
				var html2 = "";
				var j = 0;
				var class_name = "";
				$.each(packageList , function (index, value)
				{
					if(j > 8){
						class_name = "hide_package";
					}
					var url = base_url +"hotels/"+ value.package_id;
					//alert(url);
					html2 += '<div class="col-md-4 col-xs-12 offer-cols '+class_name+'">'+
								'<div class="img-div">'+
									'<a href="javascript:void(0);" onclick="goTo('+url+');">'+
										'<img src="'+base_url+package_url+'/'+value.package_logo+'" class="img-responsive">'+
											'<div class="centered"><h4>'+value.package_name+'</h4></div>'+
									'</a>'+
								'</div>'+
							  '</div>';
					j++;	
				});	
				packagesElem.append(html2);				
			}
	}); 

	