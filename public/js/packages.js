var handleDataTableButtons = function() 
{
	"use strict";
	0 !== $("#datatable-buttons").length && $("#datatable-buttons").DataTable({
		bProcessing	 	: true,
		ajax: {
						url: base_url+'admin/packages/get_packages_list',
						type: "POST",
						dataType:'json'
					},
		//ajax			 	: "packages/get_packages_list",
		dom: "Bfrtip",
		serverSide     	: true,
		lengthMenu: [
		[ 10, 25, 50, -1 ],
		[ '10 rows', '25 rows', '50 rows', 'Show all' ]
		],
		buttons: [
		{
			extend: "pageLength",
			className: "btn-sm"
		},
		{
			extend: "copy",
			className: "btn-sm"
			}, {
			extend: "csv",
			className: "btn-sm"
			}, {
			extend: "excel",
			className: "btn-sm"
			}, {
			extend: "pdf",
			className: "btn-sm"
			}, {
			extend: "print",
			className: "btn-sm"
		}],
		responsive: !0,
		aoColumns  	 	: [{
			
			mData		: "package_id",
			sName		: "No", 
			sTitle     	: "Sr No." ,
			searchable 	: false,
			orderable  	: false,
			targets    	: 0
		},
		
		{
			mData		: "package_name", // database coloum name
			sName		: "package_name", // database coloum name
			sTitle		: "Package Name", //HTML column name(Title)
			searchable 	: true,
			orderable  	: true
		},
		
		{
			mData		: "package_logo", // database coloum name
			sName		: "package_logo", // database coloum name
			sTitle		: "Card", //HTML column name(Title)
			searchable 	: true,
			orderable  	: true,
			mRender: function ( data , type, row )  
			{
				if(row.package_logo)
				{
					return ' <img src="'+base_url+package_url+'/'+row.package_logo+'" height="42" width="42"> ';
				}
				else
				{
					return "Not Set";
				}
				
			}
		},
		
		{
			mData		: "created_date", // database coloum name
			sName		: "created_date", // database coloum name
			sTitle		: "Created Date", //HTML column name(Title)
			searchable 	: false,
			orderable  	: false,
			targets    	: 0
		},
		
		{
			mData		: "modified_date", // database coloum name
			sName		: "modified_date", // database coloum name
			sTitle		: "Modified Date", //HTML column name(Title)
			searchable 	: false,
			orderable  	: false,
			targets    	: 0
		},
		
		{
			mData: "status", //database coloum name
			sTitle: "Active Status", //HTML column name(Title)
			searchable 	: false,
			orderable  	: false,
			targets    	: 0,
			mRender: function ( data , type, row )  {
				if(row.status==0){
					return '<button class="btn btn-danger btn-xs" type="button" disabled>Inactive</button>';
				}else{
					return '<button class="btn btn-success btn-xs" type="button" >Active</button>';
				}
			}
		},
		
		{
			mData: "package_id", //database coloum name
			sTitle: "Action", //HTML column name(Title)
			searchable 	: false,
			orderable  	: false,
			targets    	: 0,
			mRender: function ( data , type, row )
			{
				return  '<a href="javascript:editd('+row.package_id+');" id="edit_click" class="btn btn-success btn-xs" style="margin-right: 0px;">Edit</a>  <a href="javascript:remove('+row.package_id+');" class="btn btn-danger btn-xs" style="margin-right: 0px;">Remove</a>';				
			}
			
		}
		],
		fnDrawCallback: function ( oSettings ) {
			for ( var i=0, iLen=oSettings.aiDisplay.length ; i<iLen ; i++ )
			{
				$('td:eq(0)', oSettings.aoData[ oSettings.aiDisplay[i] ].nTr ).html( oSettings._iDisplayStart+i+1 );
			}
		},
		order: [[ 1, 'asc' ]],
		
		autoWidth :true,
	})
},
TableManageButtons = function() {
	"use strict";
	return {
		init: function() {
			handleDataTableButtons()
		}
	}
}();
$("#datatable-buttons thead th input[type=text]").on( 'keyup change', function () {
	$("#datatable-buttons").DataTable()
	.search( this.value )
	.draw();
	
});
	
function remove(id)
{
	var confirm_message = confirm("Are you sure you want to delete this record?");
	if(confirm_message==true)
	{
		var ur=base_url+'admin/packages/package_delete';
		$.ajax({
			type: 'post',
			url: ur,
			data: {
				package_id : id
			},
			dataType : 'json'
		})
		.done(function (response) {
			document.getElementById("succmsgDiv").style.display="block";
			document.getElementById("succmsg").innerHTML=response.message;
			$("#succmsgDiv").fadeOut(4000);
			$('#packages_form').trigger("reset");
			$("#id").val(0);
			$("#datatable-buttons").DataTable().ajax.reload();
		});
   }
}	

$(document).ready(function(){
	$(document).on('click', "#add_new_packages_btn", function(){
		if($("#add_new_packages_btn").hasClass("hide_form"))
		{
			$("#packages_div").fadeOut();
			$("#add_new_packages_btn").text("Add packages");
			$("#add_new_packages_btn").removeClass("hide_form");
			$('html, body').animate({
				scrollTop: $("#packages_div").offset().top
			 }, 1000);
		}
		else{
			$("#packages_form")[0].reset(); //Heramb Added
			$("#packages_div").fadeIn();
			$('.error').html('');
			$("#add_new_packages_btn").text("Hide packages Form");
			$("#add_new_packages_btn").addClass("hide_form");
			 $('html, body').animate({
				scrollTop: $("#packages_div").offset().top
			 },1000);
		}
	});
	$(document).on('click','.hide_form',function(){
		 // otable.ajax.reload();
		 window.location.reload();
	});

	$(document).on('click', '#edit_click', function(){  //Heramb Added
		$("#add_new_packages_btn").click();
	});
	
	$(document).on('click','#cancel',function(){
		$("#packages_div").fadeOut();
		$("#datatable-buttons").DataTable().ajax.reload();
	});

});

$(document).ready(function(){
	// validate signup form on keyup and submit
	$('#packages_form').validate({
		submitHandler:function(form)
		{
			var formData = new FormData(form);
			$.ajax({
				type: $(form).attr('method'),
				url: base_url+'admin/packages/save_package',
				data: formData,
				dataType:"json",
				async: false,
				cache: false,
				contentType: false,
				processData: false
			})
			.done(function (response) {
				Form_response(response)
			});
		},
		errorElement: 'span',
		errorClass: 'help-block error',
		rules:
		{
			package_name:"required",
			status:"required"
		},
		messages:
		{	package_name:"Please Enter packages Name",
			status:"Please select status"
		},
		ignore: []
	});
});

function Form_response(response)
{
	if(response.success==true)
	{
		document.getElementById("succmsgDiv").style.display="block";
		document.getElementById("succmsg").innerHTML=response.message;
		$("#succmsgDiv").fadeOut(4000);
		$("#packages_form").validate().resetForm();
		//$('#packages_form').trigger("reset");
		$("#id").val(0);
		$("#packages_div").fadeOut(); //Heramb Added
		$("#add_new_packages_btn").text("Add packages"); //Heramb Added
		$("#datatable-buttons").DataTable().ajax.reload();
	}
	else
	{
		document.getElementById("errormsgDiv").style.display="block";
		document.getElementById("errormsg").innerHTML=response.message;
		$("#errormsgDiv").fadeOut(4000);
	}
}

function editd(id)
{
	var ur=base_url+'admin/packages/package_edit';
	$.ajax({
		type: 'post',
		url: ur,
		data : {
			package_id : id	
		},
		dataType : 'json'
	})
	.done(function (response) {
	$("#id").val(response.package_id);
	$("#package_name").val(response.package_name);
	if(response.package_logo){
		$("#bannern").html("<img src='"+base_url+package_url+'/'+response.package_logo+"' id='bimage'  height='100' width='100'><span class='remove_img_preview'></span>");
		$("#banners").val(response.package_logo);
	}
	if (response.status == '1')
	{	
		$('#packages_div').find(':radio[name=status][value="1"]').iCheck('check');
	}
	else
	{
		$('#packages_div').find(':radio[name=status][value="0"]').iCheck('check');
	}
	$("#packages_div").fadeIn();
	$("#add_new_packages_btn").text("Hide packages Form");
	$("#add_new_packages_btn").addClass("hide_form");
	$('html, body').animate({
		scrollTop: $("#packages_div").offset().top
	},1000);


	});

	var validator = $("#packages_form").validate();
		   validator.resetForm();
		   $("input").parent().removeClass("has-error");
}

$(function() {
    $(document.body).on('change', '.packages_logo' ,function(){
		var productId = $(this).attr('id');
		var files = !!this.files ? this.files : [];
        if (!files.length || !window.FileReader) return; // no file selected, or no FileReader support

        if (/^image/.test( files[0].type)){ // only image file
            var reader = new FileReader(); // instance of the FileReader
            reader.readAsDataURL(files[0]); // read the local file
			reader.onloadend = function(){ // set image data as background of div
				$("#bannern").html("<img src='"+this.result+"' id='bimage'  height='100' width='100'>");	
				$("#banners").val(files[0]['name']);
			}
        }
    });
});

$(document).on('click', '.remove_img_preview',function () {
	var img_id = $(this).parent('div').find('img').attr('id');
	var img_scr = $(this).parent('div').find('img').attr('src');
	var id = $("#id").val();
	
	$("#banner").val("");
	var banner = $("#banners").val();
	$("#banners").val("");
	$.ajax({
		url:base_url+'packages/remove_image',
		method:"POST",
		dataType:'json',
		data :{
			'id' : id,
			'banner'  : banner 
		},
		success : function(response)
		{
			if(response.success==true)
			{
				$("#succmsgDiv").show();   
				$("#succmsg").text(response.message);
				$("#succmsgDiv").fadeOut(4000);	
				setTimeout(function(){location.reload();},3000);
			}
			else
			{
				$("#errormsgDiv").show();   
				$("#errormsg").text(response.message);
				$("#errormsgDiv").fadeOut(4000);
			}
		}
	});
	
	$(this).parent('div').empty();
});