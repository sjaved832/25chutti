$(document).ready(function(){
	$('#email_setup_form').validate({
		submitHandler:function(form)
		{
			var formData = new FormData(form);
			$.ajax({
				type: $(form).attr('method'),
				url: "email_setup/save_email_setup",
				data: formData,
				async: false,
				cache: false,
				contentType: false,
				processData: false
			})
			.done(function (response) {
				Form_response(response)
			});
		},
		errorElement: 'span',
		errorClass: 'help-block error',
	});
});

function Form_response(response)
{
	var res = eval('('+response+')');
	if(res['success']==true)
	{
		$("#succmsgDiv").show();   
		$("#succmsg").text(res['message']);
		$("#succmsgDiv").fadeOut(4000);
	}
	else
	{
		$("#errormsgDiv").show();   
		$("#errormsg").text(res['message']);
		$("#errormsgDiv").fadeOut(4000);
	} 
}

	
$(document).ready(function(){
	$("#submit").text("Add/Update Email Setup");
	editd();
});

function editd()
{
	var ur='email_setup/email_setup_edit';
	$.ajax({
		type: 'post',
		url: ur,
		dataType : 'json'
	})
	.done(function (response) {
		$("#id").val(response.id);
		$("#support").val(response.support);
	});
}