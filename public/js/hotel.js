var handleDataTableButtons = function() {
	"use strict";
	0 !== $("#datatable-buttons").length && $("#datatable-buttons").DataTable({
		bProcessing	 	: true,
		ajax: {
			url: base_url +'admin/hotel/get_hotels_list',
			type: "POST",
			dataType:'json'
		},
		//ajax			 	: "hotel/get_hotel_list",
		dom: "Bfrtip",
		serverSide     	: true,
		lengthMenu: [
		[ 10, 25, 50, -1 ],
		[ '10 rows', '25 rows', '50 rows', 'Show all' ]
		],
		buttons: [
		{
			extend: "pageLength",
			className: "btn-sm"
		},
		{
			extend: "copy",
			className: "btn-sm"
		}, 
		{
			extend: "csv",
			className: "btn-sm"
		}, 
		{
			extend: "excel",
			className: "btn-sm"
		}, 
		{
			extend: "pdf",
			className: "btn-sm"
		}, 
		{
			extend: "print",
			className: "btn-sm"
		}],
		responsive: !0,
		aoColumns  	 	: [{
			
			mData		: "hotel_id",
			sName		: "No", 
			sTitle     	: "Sr No." ,
			searchable 	: false,
			orderable  	: false,
			targets    	: 0
		},
		
		{
			mData		: "hotel_name", // database coloum name
			sName		: "hotel_name", // database coloum name
			sTitle		: "Hotel Name", //HTML column name(Title)
			searchable 	: true,
			orderable  	: true
		},
		
		{
			mData		: "city_name", // database coloum name
			sName		: "city_name", // database coloum name
			sTitle		: "City", //HTML column name(Title)
			searchable 	: true,
			orderable  	: true
		},
		
		{
			mData		: "price", // database coloum name
			sName		: "price", // database coloum name
			sTitle		: "Price", //HTML column name(Title)
			searchable 	: true,
			orderable  	: true
		},
		
		{
			mData: "status", //database coloum name
			sTitle: "Active Status", //HTML column name(Title)
			searchable 	: false,
			orderable  	: false,
			targets    	: 0,
			mRender: function ( data , type, row ){
				if(row.status==1){
						return '<button class="btn btn-success btn-xs" type="button" >Active</button>';
					}
				}
		},
		
		{
			mData: "id", //database coloum name
			sTitle: "Action", //HTML column name(Title)
			searchable 	: false,
			orderable  	: false,
			targets    	: 0,
			mRender: function ( data , type, row )
			{
					return '<a href="javascript:editd(' + row.hotel_id + ');" id="edit_click" class="btn btn-success btn-xs" style="margin-right: 0px;">Edit</a> <br> <a href="javascript:remove(' + row.hotel_id + ');" class="btn btn-danger btn-xs" style="margin-right: 0px;">Remove</a>';

			}
			
		}
		],
		fnDrawCallback: function ( oSettings ) {
			for ( var i=0, iLen=oSettings.aiDisplay.length ; i<iLen ; i++ )
			{
				$('td:eq(0)', oSettings.aoData[ oSettings.aiDisplay[i] ].nTr ).html( oSettings._iDisplayStart+i+1 );
			}
		},
		order: [[ 1, 'asc' ]],
		
		autoWidth :true,
	})
},
TableManageButtons = function() {
	"use strict";
	return {
		init: function() {
			handleDataTableButtons()
		}
	}
}();
$("#datatable-buttons thead th input[type=text]").on( 'keyup change', function () {
	$("#datatable-buttons").DataTable()
	.search( this.value )
	.draw();
	
});
	
function remove(hotel_id)
{
	var confirm_message = confirm("Are you sure you want to delete this record?");
	if(confirm_message==true)
	{
		var ur=base_url+'admin/hotel/hotel_delete';
		$.ajax({
			type: 'post',
			url: ur,
			data: {
				hotel_id: hotel_id
			},
			dataType : 'json'
		})
		.done(function (response) {
			document.getElementById("succmsgDiv").style.display="block";
			document.getElementById("succmsg").innerHTML=response.message;
			$("#succmsgDiv").fadeOut(4000);
			$('#basic_form').trigger("reset");
			$("#id").val(0);
			$("#datatable-buttons").DataTable().ajax.reload();
		});
   }
}	

$(document).ready(function(){
	//console.log();
	$("#hotelsubmit").text("Add Hotel");
	$("#img_submit").text("Add Image");
	$(document).on('click', "#add_new_hotel_btn", function()
	{
		$("#hotel_div").fadeIn();
		$('.error').html('');
		$("#add_new_hotel_btn").text("Hide hotel Form");
		$("#add_new_hotel_btn").addClass("hide_form");
		$('html, body').animate({
			scrollTop: $("#hotel_div").offset().top
			},1000);
	});
	
	$(document).on('click', "#basic_cancel", function()
	{
		$("#hotel_div").fadeOut();
		$("#add_new_hotel_btn").text("Add Hotel");			
		$("#add_new_hotel_btn").removeClass("hide_form");
		$('html, body').animate({
			scrollTop: $("#hotel_div").offset().top
	    }, 1000);
		
	});
	
	
	$(document).on('click', "#add_new_img_btn", function(){
		if($("#add_new_img_btn").hasClass("hide_form1"))
		{
			$("#image_form").fadeOut();
			$("#add_new_img_btn").text("Add Image");
			$("#image_name").attr("multiple","multiple");

			$("#image_name").attr("name","image_name[]");
			$("#add_new_img_btn").removeClass("hide_form1");
			$(".add_btn").css("display", "block");
			$('html, body').animate({
				scrollTop: $("#tab_image").offset().top
			 }, 1000);
		}
		else
		{			
			$("#image_id").val("");
			$("#image_name").val("");
			$("#image_name_image").html("");
			$("#image_name_images").val("");
			$("#image_form").fadeIn();
			$('.error').html('');
			$("#add_new_img_btn").text("Hide Image Form");
			$("#add_new_img_btn").addClass("hide_form1");
			 $('html, body').animate({
				scrollTop: $("#image_form").offset().top
			 },1000);
			

		}
	});
	
	$(document).on('click', "#img_cancel", function()
	{
		if($("#add_new_img_btn").hasClass("hide_form1"))
		{
			$("#image_form").fadeOut();
			$("#add_new_img_btn").text("Add Image");			
			$("#add_new_img_btn").removeClass("hide_form1");
			$(".add_btn").css("display", "block");
			$('html, body').animate({
			    scrollTop: $("#tab_image").offset().top
			 }, 1000);
			 
		}
		else{
			$("#image_id").val("");
			$("#image_name").val("");
			$("#image_name_image").html("");
			$("#image_name_images").val("");
			
			$("#image_form").fadeIn();
			$("#add_new_img_btn").text("Hide Image Form");
			$("#add_new_img_btn").addClass("hide_form1");		
			$('html, body').animate({
			    scrollTop: $("#image_form").offset().top
			 }, 1000);		
		}
	});
	
	
	
	
	$(document).on('click','.hide_form',function(){
		$("#hotel_div").fadeOut();
		$("#add_new_hotel_btn").text("Add Hotel");	
		$("#datatable-buttons").DataTable().ajax.reload();
		//("#basic_form").validate().resetForm();
	});
	
	$(document).on('click','.hide_form1',function(){
		$("#datatable-images").DataTable().ajax.reload();
		//("#basic_form").validate().resetForm();
	});

	$(document).on('click', '#edit_click', function(){  
		$("#hotelsubmit").text("Update Hotel");

		$("#add_new_hotel_btn").click();
	});
	
	$(document).on('click', '#edit_click_img', function(){  
		$("#img_submit").text("Update Image");
		$("#image_name").removeAttr("multiple");

		$("#image_name").attr("name","image_name");
		$("#add_new_img_btn").click();
		$(".add_btn").css("display", "none");
	});

});

$(document).ready(function(){
	// validate signup form on keyup and submit
	$('#basic_form').validate({
		submitHandler:function(form)
		{
			var formData = new FormData(form);
			$.ajax({
				type: $(form).attr('method'),
				url: base_url+'admin/hotel/save_basic_form',
				data: formData,
				async: false,
				cache: false,
				contentType: false,
				processData: false
			})
			.done(function (response) {
				//console.log(response);
				Form_response_basic(response)
			});
		},
		errorElement: 'span',
		errorClass: 'help-block error',
		rules:
		{
			hotel_name: "required",
			style:"required",
			setting:"required",
			city_id:"required",
			status:"required"
		},
		messages:
		{	
			hotel_name:"Please Enter Hotel Name",
			style: "Please Enter Hotel Style",
			setting: "Please Enter Hotel setting",
			city_id: "Please Select Area",
			status:"Please Select Status"
		},
		ignore: []
	});
	
	
	
	// validate signup form on keyup and submit
	$('#details_form').validate({
		submitHandler:function(form)
		{
			//CKEDITOR.instances['description'].updateElement();
			//CKEDITOR.instances['specification'].updateElement();
			//CKEDITOR.instances['terms_conditions'].updateElement();
			
			var formData = new FormData(form);
			$.ajax({
				type: $(form).attr('method'),
				url: base_url+'admin/hotel/save_details_form',
				data: formData,
				async: false,
				cache: false,
				contentType: false,
				processData: false
			})
			.done(function (response) {
				Form_response_details(response)
			});
		},
		errorElement: 'span',
		errorClass: 'help-block error',
		rules:
		{
			highlights: "required",
			offers: "required",
			favourite_rooms:"required"
		},
		messages:
		{	
			highlights: "Please Enter Description",
			offers: "Please Enter Offers",
			favourite_rooms: "Please Enter Favourite Rooms"
		},
		ignore: []
		
	});
	
	
	//image form
	$('#image_form').validate({
		submitHandler:function(form)
		{
			var formData = new FormData(form);
			$.ajax({
				type: $(form).attr('method'),
				url: base_url+'admin/hotel/save_image_form',
				data: formData,
				async: false,
				cache: false,
				contentType: false,
				processData: false
			})
			.done(function (response) {
				Form_response_image(response)
			});
		},
		errorElement: 'span',
		errorClass: 'help-block error',
		rules:
		{
			img_status:"required"
		},
		messages:
		{	 
			img_status:"Please Select Activation Status"
		},/**/
		ignore: []
	});
});


function Form_response_basic(response)
{
	var res = eval('('+response+')');
	if(res['success']==true)
	{
		document.getElementById("succmsgDiv").style.display="block";
		document.getElementById("succmsg").innerHTML=res['message'];
		$('html, body').animate({
			scrollTop: $("#basic_form").offset().top
		},1000);
		$("#succmsgDiv").fadeOut(4000);
		$("#b_hotel_id").val(res['hotel_id']);
		$("#d_hotel_id").val(res['hotel_id']);
		$("#i_hotel_id").val(res['hotel_id']);

		$("#details_link").show();
		$("#image_link").show();
		$("#datatable-buttons").DataTable().ajax.reload();
		$('#datatable-images').dataTable().fnDestroy();
		TableManageButtons1.init(); //Hamid Comment
		$('a[href="#tab_details"]').click();
	}
	else
	{
		document.getElementById("errormsgDiv").style.display="block";
		document.getElementById("errormsg").innerHTML=res['message'];
		$('html, body').animate({
			scrollTop: $("#basic_form").offset().top
		},1000);
		$("#errormsgDiv").fadeOut(4000);
	}
}

function Form_response_details(response)
{
	var res = eval('('+response+')');
	if(res['success']==true)
	{
		//alert(res['hotel_id']);
		document.getElementById("succmsgDiv").style.display="block";
		document.getElementById("succmsg").innerHTML=res['message'];
		$('html, body').animate({
			scrollTop: $("#details_form").offset().top
		},1000);
		$("#succmsgDiv").fadeOut(4000);
		$("#b_hotel_id").val(res['hotel_id']);
		$("#f_hotel_id").val(res['hotel_id']);
		$("#i_hotel_id").val(res['hotel_id']);
		
		$("#details_link").show();
		$("#image_link").show();
		$("#datatable-buttons").DataTable().ajax.reload();
		$('#datatable-images').dataTable().fnDestroy();
		TableManageButtons1.init(); //hamid Comment
		$('a[href="#tab_image"]').click();
	}
	else
	{
		document.getElementById("errormsgDiv").style.display="block";
		document.getElementById("errormsg").innerHTML=res['message'];
		$('html, body').animate({
			scrollTop: $("#details_form").offset().top
		},1000);
		$("#errormsgDiv").fadeOut(4000);
	}
}


function Form_response_image(response)
{
	console.log(response);
	var res = eval('('+response+')');
	//alert(res['success']);
	//alert(res['message']);
	if(res['success']==true)
	{
		document.getElementById("succmsgDiv").style.display="block";
		document.getElementById("succmsg").innerHTML=res['message'];
		$("#image_id").val("");
		
		$("#image_name").val("");
		$("#image_name_image").html("");
		$("#image_name_images").val("");
		$("#image_form").fadeOut();
		$('html, body').animate({
			scrollTop: $("#tab_image").offset().top
		},1000);
		$("#succmsgDiv").fadeOut(4000);
		
		$("#details_link").show();
		$("#image_link").show();
		$("#datatable-images").DataTable().ajax.reload();
	}
	else
	{
		document.getElementById("errormsgDiv").style.display="block";
		document.getElementById("errormsg").innerHTML=res['message'];
		$('html, body').animate({
			scrollTop: $("#image_form").offset().top
		},1000);
		$("#errormsgDiv").fadeOut(4000);
	}
}


function editd(id)
{
	if(!id)
		return setDefaultHotelAdd();
	$("#hotelubmit").text("Update Hotel");
	var ur=base_url+'admin/hotel/hotel_edit';
	$.ajax({
		type: 'post',
		url: ur,
		data : {
			hotel_id : id	
		},
		dataType : 'json'
	})
	.done(function (response) {
		
		$("#b_hotel_id").val(response.hotel_id);
		$("#d_hotel_id").val(response.hotel_id);
		$("#i_hotel_id").val(response.hotel_id);
		$("#hotel_name").val(response.hotel_name);
		$("#price").val(response.price);
		$("#style").val(response.style);
		$("#setting").val(response.setting);
		$("#extras").val(response.extras);
		$("#state_id").val(response.state_id);
		$("#city_id").val(response.city_id);
		$("#package_id").val(response.package_id);
		get_city_by_state(response.state_id,response.city_id);
		
		CKEDITOR.instances['highlights'].destroy();

		CKEDITOR.replace('highlights', {

			// Define the toolbar groups as it is a more accessible solution.
			toolbarGroups: [
			{"name":"basicstyles","groups":["basicstyles"]},
			{"name":"links","groups":["links"]},
			{"name":"paragraph","groups":["list","blocks"]},
			{"name":"document","groups":["mode"]},
			{"name":"insert","groups":["insert"]},
			{"name":"styles","groups":["styles"]},
			{"name":"about","groups":["about"]}
			],
			// Remove the redundant buttons from toolbar groups defined above.
			removeButtons: 'Underline,Strike,Subscript,Superscript,Anchor,Styles,Specialchar'
		});
		
		$("#highlights").val(response.highlights);


		
		CKEDITOR.instances['offers'].destroy();
		CKEDITOR.replace('offers', {
			// Define the toolbar groups as it is a more accessible solution.
			toolbarGroups: [
			{"name":"basicstyles","groups":["basicstyles"]},
			{"name":"links","groups":["links"]},
			{"name":"paragraph","groups":["list","blocks"]},
			{"name":"document","groups":["mode"]},
			{"name":"insert","groups":["insert"]},
			{"name":"styles","groups":["styles"]},
			{"name":"about","groups":["about"]}
			],
			// Remove the redundant buttons from toolbar groups defined above.
			removeButtons: 'Underline,Strike,Subscript,Superscript,Anchor,Styles,Specialchar'
		} );
		
		$("#offers").val(response.offers);


		
		CKEDITOR.instances['favourite_rooms'].destroy();

		CKEDITOR.replace('favourite_rooms', {

			// Define the toolbar groups as it is a more accessible solution.
			toolbarGroups: [
			{"name":"basicstyles","groups":["basicstyles"]},
			{"name":"links","groups":["links"]},
			{"name":"paragraph","groups":["list","blocks"]},
			{"name":"document","groups":["mode"]},
			{"name":"insert","groups":["insert"]},
			{"name":"styles","groups":["styles"]},
			{"name":"about","groups":["about"]}
			],
			// Remove the redundant buttons from toolbar groups defined above.
			removeButtons: 'Underline,Strike,Subscript,Superscript,Anchor,Styles,Specialchar'
		});
		
		$("#favourite_rooms").val(response.favourite_rooms);

		var str = response.facilities_ids;
		//alert(str);		
		var arr = str.split(',');
		
		$.each(arr, function(i, val){

			   $("input[value='" + val + "']").prop('checked', true);

		});
		
		if (response.type == '1')
		{	
			$('#hotel_div').find('input:radio[name=type][value="1"]').prop('checked', true);
			$('input:radio[name=type]').iCheck('update');
		}
		else
		{
			$('#hotel_div').find('input:radio[name=type][value="0"]').prop('checked', true);
			$('input:radio[name=type]').iCheck('update');
		}
		
		
		if (response.status == '1')
		{	
			$('#hotel_div').find('input:radio[name=status][value="1"]').prop('checked', true);
			$('input:radio[name=status]').iCheck('update');
		}
		else
		{
			$('#hotel_div').find('input:radio[name=status][value="0"]').prop('checked', true);
			$('input:radio[name=status]').iCheck('update');
		}
		
		
		$('#datatable-images').dataTable().fnDestroy();
		//TableManageButtons1.init();
		//$("#datatable-images").DataTable().draw();
		$("#hotel_div").fadeIn();
		$("#add_new_hotel_btn").text("Hide Hotel Form");
		$("#add_new_hotel_btn").addClass("hide_form");
		$('html, body').animate({
			scrollTop: $("#hotel_div").offset().top
		},1000);


	});

	var validator = $("#basic_form").validate();
		   validator.resetForm();
		   $("input").parent().removeClass("has-error");
}

function setDefaultHotelAdd(){
	$("#b_hotel_id").val("");
	$("#d_hotel_id").val("");
	$("#i_hotel_id").val("");
	$("#hotel_name").val("");
	$("#price").val('');
	$("#style").val('');
	$("#setting").val('');
	$("#extras").val('');
	$("#state_id").val('');
	$("#city_id").val('');
	CKEDITOR.instances['highlights'].destroy();

	CKEDITOR.replace('highlights', {

		// Define the toolbar groups as it is a more accessible solution.
		toolbarGroups: [{
				"name": "basicstyles",
				"groups": ["basicstyles"]
			},
			{
				"name": "links",
				"groups": ["links"]
			},
			{
				"name": "paragraph",
				"groups": ["list", "blocks"]
			},
			{
				"name": "document",
				"groups": ["mode"]
			},
			{
				"name": "insert",
				"groups": ["insert"]
			},
			{
				"name": "styles",
				"groups": ["styles"]
			},
			{
				"name": "about",
				"groups": ["about"]
			}
		],
		// Remove the redundant buttons from toolbar groups defined above.
		removeButtons: 'Underline,Strike,Subscript,Superscript,Anchor,Styles,Specialchar'
	});

	$("#highlights").val('');



	CKEDITOR.instances['offers'].destroy();
	CKEDITOR.replace('offers', {
		// Define the toolbar groups as it is a more accessible solution.
		toolbarGroups: [{
				"name": "basicstyles",
				"groups": ["basicstyles"]
			},
			{
				"name": "links",
				"groups": ["links"]
			},
			{
				"name": "paragraph",
				"groups": ["list", "blocks"]
			},
			{
				"name": "document",
				"groups": ["mode"]
			},
			{
				"name": "insert",
				"groups": ["insert"]
			},
			{
				"name": "styles",
				"groups": ["styles"]
			},
			{
				"name": "about",
				"groups": ["about"]
			}
		],
		// Remove the redundant buttons from toolbar groups defined above.
		removeButtons: 'Underline,Strike,Subscript,Superscript,Anchor,Styles,Specialchar'
	});

	$("#offers").val('');



	CKEDITOR.instances['favourite_rooms'].destroy();

	CKEDITOR.replace('favourite_rooms', {

		// Define the toolbar groups as it is a more accessible solution.
		toolbarGroups: [{
				"name": "basicstyles",
				"groups": ["basicstyles"]
			},
			{
				"name": "links",
				"groups": ["links"]
			},
			{
				"name": "paragraph",
				"groups": ["list", "blocks"]
			},
			{
				"name": "document",
				"groups": ["mode"]
			},
			{
				"name": "insert",
				"groups": ["insert"]
			},
			{
				"name": "styles",
				"groups": ["styles"]
			},
			{
				"name": "about",
				"groups": ["about"]
			}
		],
		// Remove the redundant buttons from toolbar groups defined above.
		removeButtons: 'Underline,Strike,Subscript,Superscript,Anchor,Styles,Specialchar'
	});

	$("#favourite_rooms").val('');




	$('#hotel_div').find('input:radio[name=status][value="1"]').prop('checked', true);
		$('input:radio[name=status]').iCheck('update');
	


	$('#datatable-images').dataTable().fnDestroy();
	//TableManageButtons1.init();
	//$("#datatable-images").DataTable().draw();
	$("#hotel_div").fadeIn();
	$("#add_new_hotel_btn").text("Hide Hotel Form");
	$("#add_new_hotel_btn").addClass("hide_form");
	$('html, body').animate({
		scrollTop: $("#hotel_div").offset().top
	}, 1000);


}

function change_status(id,status)
{
	var ur=base_url+'admin/hotel/change_status';
	$.ajax({
		type: 'post',
		url: ur,
		data : {
			'id'     : id,
			'status' : status
		},
		dataType : 'json'
	})
	.done(function (response) {
		document.getElementById("succmsgDiv").style.display="block";
		document.getElementById("succmsg").innerHTML=response.message;
		$("#succmsgDiv").fadeOut(4000);
		$("#datatable-buttons").DataTable().ajax.reload();
	});
}

/*get state for hotel*/
$.ajax({
	url: base_url +'api/state_list',
	method:"GET",
	dataType:'json',
	success : function(response)
	{
		
		$.each(response.data, function(key,val)
		{
			$('#state_id').append($('<option>').text(val.state_name).attr('value', val.state_id));
		})
	}
});

function get_city_by_state(state_id,city_id)
{
	$.ajax({
		url: base_url + 'api/city_list_by_state_id?id=' + state_id,
		method:"get",
		dataType:'json',
		success : function(response)
		{
			var city = $('#city_id');
			city.empty();
			
				var option = $('<option/>');
				var atribute = { 'value': '' };
				option.attr(atribute).text("---- Select City-----");
				city.append(option);
			$.each(response.data, function(key,val)
			{
				var option = $('<option/>');
				var atribute = { 'value': val.city_id };
				if (val.city_id == city_id) {
					atribute.selected = "selected";
				}
				option.attr(atribute).text(val.city_name);
				city.append(option);
			});
		}
	});

}

/*get pacjage for hotel*/
$.ajax({
	url: base_url +'api/package_list',
	method:"GET",
	dataType:'json',
	success : function(response)
	{
		
		$.each(response.data, function(key,val)
		{
			$('#package_id').append($('<option>').text(val.package_name).attr('value', val.package_id));
		})
	}
});



$(function() {
    $(document.body).on('change', '.banner' ,function(){
		var productId = $(this).attr('id');
		var files = !!this.files ? this.files : [];
        if (!files.length || !window.FileReader) return; // no file selected, or no FileReader support

        if (/^image/.test( files[0].type)){ // only image file
            var reader = new FileReader(); // instance of the FileReader
            reader.readAsDataURL(files[0]); // read the local file

            reader.onloadend = function(){ // set image data as background of div
				if(productId == 'hotel_logo')
				{
					$("#banner_image").html("<img src='"+this.result+"' id='bimage'  height='100' width='100'>");	
					$("#banner_images").val(files[0]['name']);
				}
				
				/*else if(productId == 'image_name')
				{
					$("#image_name_image").html("<img src='"+this.result+"' id='iiimage'  height='100' width='100'>");	
					$("#image_name_images").val(files[0]['name']);
				}*/	
			}
        }
    });
});

/*$(document).on('click', '.remove_img_preview',function () {
	var img_id = $(this).parent('div').find('img').attr('id');
	var img_scr = $(this).parent('div').find('img').attr('src');
	
	if(img_id=="bimage")
	{
		var id = $("#b_bike_id").val();
		$("#banner").val("");
		var banner = $("#banner_images").val();
		$("#banner_images").val("");
		$.ajax({
			url:base_url+'hotel/remove_image',
			method:"POST",
			dataType:'json',
			data :{
				'id' : id,
				'banner'  : banner 
			},
			success : function(response)
			{
				if(response.success==true)
				{
					$("#succmsgDiv").show();   
					$("#succmsg").text(response.message);
					$("#succmsgDiv").fadeOut(4000);	
				}
				else
				{
					$("#errormsgDiv").show();   
					$("#errormsg").text(response.message);
					$("#errormsgDiv").fadeOut(4000);
				}
			}
		})
	}
	else if(img_id=="ssimage")
	{
		var id = $("#image_id").val();
		$("#small_img").val("");
		var small_img = $("#small_images").val();
		$("#small_images").val("");
		$.ajax({
			url:base_url+'hotel/remove_image',
			method:"POST",
			dataType:'json',
			data :{
				'id' : id,
				'small_img'  : small_img 
			},
			success : function(response)
			{
				if(response.success==true)
				{
					$("#succmsgDiv").show();   
					$("#succmsg").text(response.message);
					$("#succmsgDiv").fadeOut(4000);	
				}
				else
				{
					$("#errormsgDiv").show();   
					$("#errormsg").text(response.message);
					$("#errormsgDiv").fadeOut(4000);
				}
			}
		})
	}
	else if(img_id=="iiimage")
	{
		var id = $("#image_id").val();
		$("#image_name").val("");
		var image_name = $("#image_name_images").val();
		$("#image_name_images").val("");
		$.ajax({
			url:base_url+'hotel/remove_image',
			method:"POST",
			dataType:'json',
			data :{
				'id' : id,
				'image_name'  : image_name 
			},
			success : function(response)
			{
				if(response.success==true)
				{
					$("#succmsgDiv").show();   
					$("#succmsg").text(response.message);
					$("#succmsgDiv").fadeOut(4000);	
				}
				else
				{
					$("#errormsgDiv").show();   
					$("#errormsg").text(response.message);
					$("#errormsgDiv").fadeOut(4000);
				}
			}
		})
	}
	$(this).parent('div').empty();
});*/



var handleDataTableButtons1 = function() {
	"use strict";
	0 !== $("#datatable-images").length && $("#datatable-images").DataTable({
		bProcessing	 	: true,
		ajax: {
			url: base_url+'admin/hotel/get_image_list',
			type: "POST",
			data : {
				
				'hotel_id' : $("#i_hotel_id").val()
			},
			dataType:'json'
		},
		//ajax			 	: "hotel/get_hotel_list",
		dom: "Bfrtip",
		serverSide     	: true,
		lengthMenu: [
		[ 10, 25, 50, -1 ],
		[ '10 rows', '25 rows', '50 rows', 'Show all' ]
		],
		buttons: [
		{
			extend: "pageLength",
			className: "btn-sm"
		},
		{
			extend: "copy",
			className: "btn-sm"
			}, {
			extend: "csv",
			className: "btn-sm"
			}, {
			extend: "excel",
			className: "btn-sm"
			}, {
			extend: "pdf",
			className: "btn-sm"
			}, {
			extend: "print",
			className: "btn-sm"
		}],
		responsive: !0,
		aoColumns  	 	: [{
			
			mData		: "image_id",
			sName		: "No", 
			sTitle     	: "Sr No." ,
			searchable 	: false,
			orderable  	: false,
			targets    	: 0
		},
		
		{
			mData		: "hotel_name", // database coloum name
			sName		: "hotel_name", // database coloum name
			sTitle		: "Hotel", //HTML column name(Title)
			searchable 	: true,
			orderable  	: true
		},
		
		
		{
			mData		: "image_name", // database coloum name
			sName		: "image_name", // database coloum name
			sTitle		: "Image", //HTML column name(Title)
			searchable 	: true,
			orderable  	: true,
			mRender: function ( data , type, row )  
			{
				if(row.image_name)
				{
					return ' <img src="'+base_url+hotel_upload_url+'/'+row.image_name+'" height="42" width="42"> ';
				}
				else
				{
					return "NA";	
				}	
				
			}
		},
		
		{
			mData: "status", //database coloum name
			sTitle: "Active Status", //HTML column name(Title)
			searchable 	: false,
			orderable  	: false,
			targets    	: 0,
			mRender: function ( data , type, row ){
				//debugger;
				if(row.status==1)
				{
					return '<button class="btn btn-success btn-xs" type="button">Active</button>';
				}
				return "";
			}
		},
		
		{
			mData: "id", //database coloum name
			sTitle: "Action", //HTML column name(Title)
			searchable 	: false,
			orderable  	: false,
			targets    	: 0,
			mRender: function ( data , type, row )
			{
				return  '<a href="javascript:editd_img('+row.image_id+');" id="edit_click_img" class="btn btn-success btn-xs" style="margin-right: 0px;">Edit</a> <br> <a href="javascript:remove_img('+row.image_id+');" class="btn btn-danger btn-xs" style="margin-right: 0px;">Remove</a>';
			}

		}
		],
		fnDrawCallback: function ( oSettings ) {
			for ( var i=0, iLen=oSettings.aiDisplay.length ; i<iLen ; i++ )
			{
				$('td:eq(0)', oSettings.aoData[ oSettings.aiDisplay[i] ].nTr ).html( oSettings._iDisplayStart+i+1 );
			}
		},
		order: [[ 2, 'asc' ]],
		
		autoWidth :true,
	})
},
TableManageButtons1 = function() {
	"use strict";
	return {
		init: function() {
			handleDataTableButtons1()
		}
	}
}();
$("#datatable-images thead th input[type=text]").on( 'keyup change', function () {
	$("#datatable-images").DataTable()
	.search( this.value )
	.draw();
	
});


// $('#image_name').on('change',function(e){
	// $('#image_form').ajaxForm({
	// 	//display the uploaded images
	// 	target:'#images_preview',
	// 	beforeSubmit:function(e){
	// 		$('.uploading').show();
	// 	},
	// 	success:function(e){
	// 		$('.uploading').hide()g    ./.;
	// 	},
	// 	error:function(e){
	// 	}
	// }).submit();
// });
function editd_img(id)
{
	$("#img_submit").text("Update Image");	
	var ur=base_url+'admin/hotel/image_edit';
	$.ajax({
		type: 'post',
		url: ur,
		data : {
			id : id	
		},
		dataType : 'json'
	})
	.done(function (response) {
	//$("#id").val(response.id);
	$("#image_id").val(response.image_id);
	$("#i_hotel_id").val(response.i_hotel_id);
	
	if(response.image_name)
	{
		$("#image_name_div").html("<img src='" + base_url + hotel_upload_url + "/" + response.image_name + "' id='iiimage'  height='100' width='100' style='padding-top: 6%;'><span class='remove_img_preview'></span>");
		$("#image_name_images").val(response.image_name);
	}
	if (response.img_status == '1')
	{	
		$('#image_form').find('input:radio[name=status][value="1"]').prop('checked', true);
		$('input:radio[name=status]').iCheck('update');
	}
	else
	{
		$('#image_form').find('input:radio[name=status][value="0"]').prop('checked', true);
		$('input:radio[name=status]').iCheck('update');
	}
	$("#image_form").fadeIn();
	$("#image_name").removeAttr("multiple");
	$("#image_name").attr("name","image_name");
	$("#add_new_img_btn").text("Hide Image Form");
	$("#add_new_img_btn").addClass("hide_form1");
	$(".add_btn").css("display", "none");
	$('html, body').animate({
		scrollTop: $("#image_form").offset().top
	},1000);


	});

	var validator = $("#image_form").validate();
		   validator.resetForm();
		   $("input").parent().removeClass("has-error");
}

function remove_img(id)
{
	var confirm_message = confirm("Are you sure you want to delete this record?");
	if(confirm_message==true)
	{
		var ur='hotel/image_delete';
		$.ajax({
			type: 'post',
			url: ur,
			data: {
				image_id : id
			},
			dataType : 'json'
		})
		.done(function (response) {
			document.getElementById("succmsgDiv").style.display="block";
			document.getElementById("succmsg").innerHTML=response.message;
			$("#succmsgDiv").fadeOut(4000);
			$("#datatable-images").DataTable().ajax.reload();
		});
   }
}


function change_status_img(id,status)
{
	var ur='hotel/change_status_img';
	$.ajax({
		type: 'post',
		url: ur,
		data : {
			'id'     : id,
			'status' : status
		},
		dataType : 'json'
	})
	.done(function (response) {
		document.getElementById("succmsgDiv").style.display="block";
		document.getElementById("succmsg").innerHTML=response.message;
		$("#succmsgDiv").fadeOut(4000);
		$("#datatable-images").DataTable().ajax.reload();
	});
}


$(document).ready(function(){
    var maxField = 10; //Input fields increment limitation
    var addButton = $('.add_button'); //Add button selector
    var wrapper = $('.field_wrapper'); //Input field wrapper
    var fieldHTML = '<div><input type="file" name="image_name[]" value=""/><a href="javascript:void(0);" class="btn btn-sm btn-danger remove_button" title="Remove field"><i class="fa fa-minus"></i></a></div>'; //New input field html 
    var x = 1; //Initial field counter is 1
    $(addButton).click(function(){ //Once add button is clicked
        if(x < maxField){ //Check maximum number of input fields
            x++; //Increment field counter
            $(wrapper).append(fieldHTML); // Add field html
        }
    });
    $(wrapper).on('click', '.remove_button', function(e){ //Once remove button is clicked
        e.preventDefault();
        $(this).parent('div').remove(); //Remove field html
        x--; //Decrement field counter
    });
});
