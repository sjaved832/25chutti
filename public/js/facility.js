var handleDataTableButtons = function() {
	"use strict";
	0 !== $("#datatable-buttons").length && $("#datatable-buttons").DataTable({
		bProcessing	 	: true,
		ajax: {
						url: base_url+'admin/facility/get_facility_list',
						type: "POST",

						dataType:'json'
					},
		//ajax			 	: "facility/get_facility_list",
		dom: "Bfrtip",
		serverSide     	: true,
		lengthMenu: [
		[ 10, 25, 50, -1 ],
		[ '10 rows', '25 rows', '50 rows', 'Show all' ]
		],
		buttons: [
		{
			extend: "pageLength",
			className: "btn-sm"
		},
		{
			extend: "copy",
			className: "btn-sm"
			}, {
			extend: "csv",
			className: "btn-sm"
			}, {
			extend: "excel",
			className: "btn-sm"
			}, {
			extend: "pdf",
			className: "btn-sm"
			}, {
			extend: "print",
			className: "btn-sm"
		}],
		responsive: !0,
		aoColumns  	 	: [{

			mData		: "facility_id",
			sName		: "No",
			sTitle     	: "Sr No." ,
			searchable 	: false,
			orderable  	: false,
			targets    	: 0
		},

		{
			mData		: "facility_name", // database coloum name
			sName		: "facility_name", // database coloum name
			sTitle		: "facility Name", //HTML column name(Title)
			searchable 	: true,
			orderable  	: true
		},
		
		{
			mData		: "created_date", // database coloum name
			sName		: "created_date", // database coloum name
			sTitle		: "Created Date", //HTML column name(Title)
			searchable 	: false,
			orderable  	: false,
			targets    	: 0
		},

		{
			mData		: "modified_date", // database coloum name
			sName		: "modified_date", // database coloum name
			sTitle		: "Modified Date", //HTML column name(Title)
			searchable 	: false,
			orderable  	: false,
			targets    	: 0
		},

		{
			mData: "active", //database coloum name
			sTitle: "Active Status", //HTML column name(Title)
			searchable 	: false,
			orderable  	: false,
			targets    	: 0,
			mRender: function ( data , type, row )  {
				if(row.status==0){
					return '<button class="btn btn-danger btn-xs" type="button" disabled>Inactive</button>';
				}else{
					return '<button class="btn btn-success btn-xs" type="button" >Active</button>';
				}
			}
		},

		{
			mData: "facility_id", //database coloum name
			sTitle: "Action", //HTML column name(Title)
			searchable 	: false,
			orderable  	: false,
			targets    	: 0,
			mRender: function ( data , type, row )
			{
				return  '<a href="javascript:editd('+row.facility_id+');" id="edit_click" class="btn btn-success btn-xs" style="margin-right: 0px;">Edit</a>  <a href="javascript:remove('+row.facility_id+');" class="btn btn-danger btn-xs" style="margin-right: 0px;">Remove</a>';
			}

		}
		],
		fnDrawCallback: function ( oSettings ) {
			for ( var i=0, iLen=oSettings.aiDisplay.length ; i<iLen ; i++ )
			{
				$('td:eq(0)', oSettings.aoData[ oSettings.aiDisplay[i] ].nTr ).html( oSettings._iDisplayStart+i+1 );
			}
		},
		order: [[ 1, 'asc' ]],

		autoWidth :true,
	})
},
TableManageButtons = function() {
	"use strict";
	return {
		init: function() {
			handleDataTableButtons()
		}
	}
}();
$("#datatable-buttons thead th input[type=text]").on( 'keyup change', function () {
	$("#datatable-buttons").DataTable()
	.search( this.value )
	.draw();

});

function remove(id)
{
	var confirm_message = confirm("Are you sure you want to delete this record?");
	if(confirm_message==true)
	{
		var ur=base_url+'admin/facility/facility_delete';
		$.ajax({
			type: 'post',
			url: ur,
			data: {
				facility_id : id
			},
			dataType : 'json'
		})
		.done(function (response) {
			document.getElementById("succmsgDiv").style.display="block";
			document.getElementById("succmsg").innerHTML=response.message;
			$("#succmsgDiv").fadeOut(4000);
			$('#facility_form').trigger("reset");
			$("#id").val(0);
			$("#datatable-buttons").DataTable().ajax.reload();
		});
   }
}

$(document).ready(function(){
	$(document).on('click', "#add_new_facility_btn", function(){
		if($("#add_new_facility_btn").hasClass("hide_form"))
		{
			$("#facility_div").fadeOut();
			$("#add_new_facility_btn").text("Add facility");
			$("#add_new_facility_btn").removeClass("hide_form");
			$('html, body').animate({
				scrollTop: $("#facility_div").offset().top
			 }, 1000);
		}
		else{
			$("#facility_form")[0].reset(); // Added
			$("#facility_div").fadeIn();
			$('.error').html('');
			$("#add_new_facility_btn").text("Hide facility Form");
			$("#add_new_facility_btn").addClass("hide_form");
			 $('html, body').animate({
				scrollTop: $("#facility_div").offset().top
			 },1000);


		}
	});
	$(document).on('click','.hide_form',function(){
		 // otable.ajax.reload();
		 window.location.reload();
	});

	$(document).on('click', '#edit_click', function(){  // Added
		$("#add_new_facility_btn").click();
	});
	
	$(document).on('click', '#cancel', function(){  // Added
		$("#facility_div").fadeOut();
		$("#datatable-buttons").DataTable().ajax.reload();
		$("#add_new_facility_btn").text("Add facility");
	});

});

$(document).ready(function(){
	// validate signup form on keyup and submit
	$('#facility_form').validate({
		submitHandler:function(form)
		{
			var formData = new FormData(form);
			$.ajax({
				type: $(form).attr('method'),
				url: base_url+'admin/facility/save_facility',
				data: formData,
				dataType : 'json',
				processData: false,
                contentType: false,
			})
			.done(function (response) {
				Form_response(response)
			});
		},
		errorElement: 'span',
		errorClass: 'help-block error',
		rules:
		{
			facility_name:
			{
				required:true
			},
			
			status:
			{
				required:true
			}
		},
		messages:
		{
			facility_name:
			{
				required:"Please Enter facility Name"
			},
			
			status:
			{
				required:"Please select status"
			}
		},
		ignore: []
	});
});


function Form_response(response)
{
	if(response.success==true)
	{
		document.getElementById("succmsgDiv").style.display="block";
		document.getElementById("succmsg").innerHTML=response.message;
		$("#succmsgDiv").fadeOut(4000);
		$("#facility_form").validate().resetForm();
		//$('#facility_form').trigger("reset");
		$("#id").val(0);
		$("#facility_div").fadeOut(); // Added
		$("#add_new_facility_btn").text("Add facility"); // Added
		$("#datatable-buttons").DataTable().ajax.reload();
	}
	else
	{
		document.getElementById("errormsgDiv").style.display="block";
		document.getElementById("errormsg").innerHTML=response.message;
		$("#errormsgDiv").fadeOut(4000);
	}
}


function editd(facility_id)
{
	var ur=base_url+'admin/facility/facility_edit';
	$.ajax({
		type: 'post',
		url: ur,
		data : {
			facility_id : facility_id
		},
		dataType : 'json'
	})
	.done(function (response) {
	$("#id").val(response.facility_id);
	$("#facility_name").val(response.facility_name);
	//$("#state_id").select2("val",response.state_id);
	$("#state_id").val(response.state_id);
	if (response.status == '1')
	{	
		$('#facility_div').find(':radio[name=status][value="1"]').iCheck('check');
	}
	else
	{
		$('#facility_div').find(':radio[name=status][value="0"]').iCheck('check');
	}

	$("#facility_div").fadeIn();
	$("#add_new_facility_btn").text("Hide facility Form");
	$("#add_new_facility_btn").addClass("hide_form");
	$('html, body').animate({
		scrollTop: $("#facility_div").offset().top
	},1000);


	});

	/*var validator = $("#facility_form").validate();
		   validator.resetForm();
		   $("input").parent().removeClass("has-error");*/
}


