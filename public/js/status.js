var handleDataTableButtons = function() {
	"use strict";
	0 !== $("#datatable-buttons").length && $("#datatable-buttons").DataTable({
		bProcessing	 	: true,
		ajax: {
						url: base_url+'status/get_status_list',
						type: "POST",
						
						dataType:'json'
					},
		//ajax			 	: "status/get_status_list",
		dom: "Bfrtip",
		serverSide     	: true,
		lengthMenu: [
		[ 10, 25, 50, -1 ],
		[ '10 rows', '25 rows', '50 rows', 'Show all' ]
		],
		buttons: [
		{
			extend: "pageLength",
			className: "btn-sm"
		},
		{
			extend: "copy",
			className: "btn-sm"
			}, {
			extend: "csv",
			className: "btn-sm"
			}, {
			extend: "excel",
			className: "btn-sm"
			}, {
			extend: "pdf",
			className: "btn-sm"
			}, {
			extend: "print",
			className: "btn-sm"
		}],
		responsive: !0,
		aoColumns  	 	: [{
			
			mData		: "id",
			sName		: "No", 
			sTitle     	: "Sr No." ,
			searchable 	: false,
			orderable  	: false,
			targets    	: 0
		},
		
		{
			mData		: "name", // database coloum name
			sName		: "name", // database coloum name
			sTitle		: "Status Name", //HTML column name(Title)
			searchable 	: true,
			orderable  	: true
		},
		
		{
			mData		: "created_date", // database coloum name
			sName		: "created_date", // database coloum name
			sTitle		: "Created Date", //HTML column name(Title)
			searchable 	: false,
			orderable  	: false,
			targets    	: 0
		},
		
		{
			mData		: "modified_date", // database coloum name
			sName		: "modified_date", // database coloum name
			sTitle		: "Modified Date", //HTML column name(Title)
			searchable 	: false,
			orderable  	: false,
			targets    	: 0
		},
		
		{
			mData: "active", //database coloum name
			sTitle: "Active Status", //HTML column name(Title)
			searchable 	: false,
			orderable  	: false,
			targets    	: 0,
			mRender: function ( data , type, row )  {
				if(row.active==0){
					return '<button class="btn btn-danger btn-xs" type="button" disabled>Inactive</button>';
				}else{
					return '<button class="btn btn-success btn-xs" type="button" disabled>Active</button>';
				}
			}
		},
		
		{
			mData: "id", //database coloum name
			sTitle: "Action", //HTML column name(Title)
			searchable 	: false,
			orderable  	: false,
			targets    	: 0,
			mRender: function ( data , type, row )
			{
				///console.log(write_access);
				if(write_access == 1 && delete_access == 1)
				{
					return  '<a href="javascript:editd('+row.id+');" id="edit_click" class="btn btn-success btn-xs" style="margin-right: 0px;">Edit</a>  <a href="javascript:remove('+row.id+');" class="btn btn-danger btn-xs" style="margin-right: 0px;">Remove</a>';
				}
				else if(write_access == 1)
				{
					return  '<a href="javascript:editd('+row.id+');" id="edit_click" class="btn btn-success btn-xs" style="margin-right: 0px;">Edit</a>';
				}
				else if(delete_access == 1)
				{
					return  '<a href="javascript:remove('+row.id+');" class="btn btn-danger btn-xs" style="margin-right: 0px;">Remove</a>';
				}
				else
				{
					return 'NA';
				}
				
			}
			
		}
		],
		fnDrawCallback: function ( oSettings ) {
			for ( var i=0, iLen=oSettings.aiDisplay.length ; i<iLen ; i++ )
			{
				$('td:eq(0)', oSettings.aoData[ oSettings.aiDisplay[i] ].nTr ).html( oSettings._iDisplayStart+i+1 );
			}
		},
		order: [[ 1, 'asc' ]],
		
		autoWidth :true,
	})
},
TableManageButtons = function() {
	"use strict";
	return {
		init: function() {
			handleDataTableButtons()
		}
	}
}();
$("#datatable-buttons thead th input[type=text]").on( 'keyup change', function () {
	$("#datatable-buttons").DataTable()
	.search( this.value )
	.draw();
	
});
	
function remove(id)
{
	var confirm_message = confirm("Are you sure you want to delete this record?");
	if(confirm_message==true)
	{
		var ur='status/status_delete';
		$.ajax({
			type: 'post',
			url: ur,
			data: {
				id : id
			},
			dataType : 'json'
		})
		.done(function (response) {
			document.getElementById("succmsgDiv").style.display="block";
			document.getElementById("succmsg").innerHTML=response.message;
			$("#succmsgDiv").fadeOut(4000);
			$('#status_form').trigger("reset");
			$("#id").val(0);
			$("#datatable-buttons").DataTable().ajax.reload();
		});
   }
}	

$(document).ready(function(){
	$(document).on('click', "#add_new_status_btn", function(){
		if($("#add_new_status_btn").hasClass("hide_form"))
		{
			$("#status_div").fadeOut();
			$("#add_new_status_btn").text("Add Status");
			$("#add_new_status_btn").removeClass("hide_form");
			$('html, body').animate({
				scrollTop: $("#status_div").offset().top
			 }, 1000);
		}
		else{
			$("#status_form")[0].reset(); // Added
			$("#status_div").fadeIn();
			$('.error').html('');
			$("#add_new_status_btn").text("Hide Status Form");
			$("#add_new_status_btn").addClass("hide_form");
			 $('html, body').animate({
				scrollTop: $("#status_div").offset().top
			 },1000);
			

		}
	});
	$(document).on('click','.hide_form',function(){
		 // otable.ajax.reload();
		 window.location.reload();
	});

	$(document).on('click', '#edit_click', function(){  // Added
		$("#add_new_status_btn").click();
	});

});

$(document).ready(function(){
	// validate signup form on keyup and submit
	$('#status_form').validate({
		submitHandler:function(form)
		{
			$.ajax({
				type: $(form).attr('method'),
				url: 'status/save_status',
				data: $(form).serialize(),
				dataType : 'json'
			})
			.done(function (response) {
				Form_response(response)
			});
		},
		errorElement: 'span',
		errorClass: 'help-block error',
		rules:
		{
			name:"required",
			status:"required"
		},
		messages:
		{	name:"Please Enter Status Name",
			status:"Please select status"
		},
		ignore: []
	});
});

function Form_response(response)
{
	if(response.success==true)
	{
		document.getElementById("succmsgDiv").style.display="block";
		document.getElementById("succmsg").innerHTML=response.message;
		$("#succmsgDiv").fadeOut(4000);
		$("#status_form").validate().resetForm();
		//$('#status_form').trigger("reset");
		$("#id").val(0);
		$("#status_div").fadeOut(); // Added
		$("#add_new_status_btn").text("Add Status"); // Added
		$("#datatable-buttons").DataTable().ajax.reload();
	}
	else
	{
		document.getElementById("errormsgDiv").style.display="block";
		document.getElementById("errormsg").innerHTML=response.message;
		$("#errormsgDiv").fadeOut(4000);
	}
}

function editd(id)
{
	var ur='status/status_edit';
	$.ajax({
		type: 'post',
		url: ur,
		data : {
			id : id	
		},
		dataType : 'json'
	})
	.done(function (response) {
	$("#id").val(response.id);
	$("#name").val(response.name);
	$("#status_div").fadeIn();
	$("#add_new_status_btn").text("Hide Status Form");
	$("#add_new_status_btn").addClass("hide_form");
	$('html, body').animate({
		scrollTop: $("#status_div").offset().top
	},1000);


	});

	var validator = $("#status_form").validate();
		   validator.resetForm();
		   $("input").parent().removeClass("has-error");
}