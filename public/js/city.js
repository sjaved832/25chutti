var handleDataTableButtons = function() {
	"use strict";
	0 !== $("#datatable-buttons").length && $("#datatable-buttons").DataTable({
		bProcessing	 	: true,
		ajax: {
						url: base_url+'admin/city/get_city_list',
						type: "POST",

						dataType:'json'
					},
		//ajax			 	: "city/get_city_list",
		dom: "Bfrtip",
		serverSide     	: true,
		lengthMenu: [
		[ 10, 25, 50, -1 ],
		[ '10 rows', '25 rows', '50 rows', 'Show all' ]
		],
		buttons: [
		{
			extend: "pageLength",
			className: "btn-sm"
		},
		{
			extend: "copy",
			className: "btn-sm"
			}, {
			extend: "csv",
			className: "btn-sm"
			}, {
			extend: "excel",
			className: "btn-sm"
			}, {
			extend: "pdf",
			className: "btn-sm"
			}, {
			extend: "print",
			className: "btn-sm"
		}],
		responsive: !0,
		aoColumns  	 	: [{

			mData		: "city_id",
			sName		: "No",
			sTitle     	: "Sr No." ,
			searchable 	: false,
			orderable  	: false,
			targets    	: 0
		},

		{
			mData		: "city_name", // database coloum name
			sName		: "city_name", // database coloum name
			sTitle		: "City Name", //HTML column name(Title)
			searchable 	: true,
			orderable  	: true
		},
		
		{
			mData		: "state_name", // database coloum name
			sName		: "state_name", // database coloum name
			sTitle		: "State Name", //HTML column name(Title)
			searchable 	: true,
			orderable  	: true
		},
		
		{
			mData		: "created_date", // database coloum name
			sName		: "created_date", // database coloum name
			sTitle		: "Created Date", //HTML column name(Title)
			searchable 	: false,
			orderable  	: false,
			targets    	: 0
		},

		{
			mData		: "modified_date", // database coloum name
			sName		: "modified_date", // database coloum name
			sTitle		: "Modified Date", //HTML column name(Title)
			searchable 	: false,
			orderable  	: false,
			targets    	: 0
		},

		{
			mData: "active", //database coloum name
			sTitle: "Active Status", //HTML column name(Title)
			searchable 	: false,
			orderable  	: false,
			targets    	: 0,
			mRender: function ( data , type, row )  {
				if(row.status==0){
					return '<button class="btn btn-danger btn-xs" type="button" disabled>Inactive</button>';
				}else{
					return '<button class="btn btn-success btn-xs" type="button" >Active</button>';
				}
			}
		},

		{
			mData: "city_id", //database coloum name
			sTitle: "Action", //HTML column name(Title)
			searchable 	: false,
			orderable  	: false,
			targets    	: 0,
			mRender: function ( data , type, row )
			{
				return  '<a href="javascript:editd('+row.city_id+');" id="edit_click" class="btn btn-success btn-xs" style="margin-right: 0px;">Edit</a>  <a href="javascript:remove('+row.city_id+');" class="btn btn-danger btn-xs" style="margin-right: 0px;">Remove</a>';
			}

		}
		],
		fnDrawCallback: function ( oSettings ) {
			for ( var i=0, iLen=oSettings.aiDisplay.length ; i<iLen ; i++ )
			{
				$('td:eq(0)', oSettings.aoData[ oSettings.aiDisplay[i] ].nTr ).html( oSettings._iDisplayStart+i+1 );
			}
		},
		order: [[ 1, 'asc' ]],

		autoWidth :true,
	})
},
TableManageButtons = function() {
	"use strict";
	return {
		init: function() {
			handleDataTableButtons()
		}
	}
}();
$("#datatable-buttons thead th input[type=text]").on( 'keyup change', function () {
	$("#datatable-buttons").DataTable()
	.search( this.value )
	.draw();

});

function remove(id)
{
	var confirm_message = confirm("Are you sure you want to delete this record?");
	if(confirm_message==true)
	{
		var ur=base_url+'admin/city/city_delete';
		$.ajax({
			type: 'post',
			url: ur,
			data: {
				city_id : id
			},
			dataType : 'json'
		})
		.done(function (response) {
			document.getElementById("succmsgDiv").style.display="block";
			document.getElementById("succmsg").innerHTML=response.message;
			$("#succmsgDiv").fadeOut(4000);
			$('#city_form').trigger("reset");
			$("#id").val(0);
			$("#datatable-buttons").DataTable().ajax.reload();
		});
   }
}

$(document).ready(function(){
	$(document).on('click', "#add_new_city_btn", function(){
		if($("#add_new_city_btn").hasClass("hide_form"))
		{
			$("#city_div").fadeOut();
			$("#add_new_city_btn").text("Add City");
			$("#add_new_city_btn").removeClass("hide_form");
			$('html, body').animate({
				scrollTop: $("#city_div").offset().top
			 }, 1000);
		}
		else{
			$("#city_form")[0].reset(); // Added
			$("#city_div").fadeIn();
			$('.error').html('');
			$("#add_new_city_btn").text("Hide City Form");
			$("#add_new_city_btn").addClass("hide_form");
			 $('html, body').animate({
				scrollTop: $("#city_div").offset().top
			 },1000);


		}
	});
	$(document).on('click','.hide_form',function(){
		 // otable.ajax.reload();
		 window.location.reload();
	});

	$(document).on('click', '#edit_click', function(){  // Added
		$("#add_new_city_btn").click();
	});
	
	$(document).on('click', '#cancel', function(){  // Added
		$("#city_div").fadeOut();
		$("#datatable-buttons").DataTable().ajax.reload();
		$("#add_new_city_btn").text("Add City");
	});

});

$(document).ready(function(){
	// validate signup form on keyup and submit
	$('#city_form').validate({
		submitHandler:function(form)
		{
			var formData = new FormData(form);
			$.ajax({
				type: $(form).attr('method'),
				url: base_url+'admin/city/save_city',
				data: formData,
				dataType : 'json',
				processData: false,
                contentType: false,
			})
			.done(function (response) {
				Form_response(response)
			});
		},
		errorElement: 'span',
		errorClass: 'help-block error',
		rules:
		{
			city_name:
			{
				required:true
			},
			
			state_id:
			{
				required:true
			},
			
			status:
			{
				required:true
			}
		},
		messages:
		{
			city_name:
			{
				required:"Please Enter City Name"
			},
			
			state_id:
			{
				required:"Please Select State"
			},
			
			status:
			{
				required:"Please select status"
			}
		},
		ignore: []
	});
});


function Form_response(response)
{
	if(response.success==true)
	{
		document.getElementById("succmsgDiv").style.display="block";
		document.getElementById("succmsg").innerHTML=response.message;
		$("#succmsgDiv").fadeOut(4000);
		$("#city_form").validate().resetForm();
		//$('#city_form').trigger("reset");
		$("#id").val(0);
		$("#city_div").fadeOut(); // Added
		$("#add_new_city_btn").text("Add City"); // Added
		$("#datatable-buttons").DataTable().ajax.reload();
	}
	else
	{
		document.getElementById("errormsgDiv").style.display="block";
		document.getElementById("errormsg").innerHTML=response.message;
		$("#errormsgDiv").fadeOut(4000);
	}
}

$(document).ready(function(){
	
	$.ajax({
	url:base_url+'admin/city/get_state',
	method:"POST",
	dataType:'json',
	success : function(response)
	{
		
		$.each(response, function(key,val)
		{
			$('#state_id').append($('<option>').text(val.state_name).attr('value', val.state_id));
		})
	}
});
});


function editd(city_id)
{
	var ur=base_url+'admin/city/city_edit';
	$.ajax({
		type: 'post',
		url: ur,
		data : {
			city_id : city_id
		},
		dataType : 'json'
	})
	.done(function (response) {
	$("#id").val(response.city_id);
	$("#city_name").val(response.city_name);
	//$("#state_id").select2("val",response.state_id);
	$("#state_id").val(response.state_id);
	if (response.status == '1')
	{	
		$('#city_div').find(':radio[name=status][value="1"]').iCheck('check');
	}
	else
	{
		$('#city_div').find(':radio[name=status][value="0"]').iCheck('check');
	}

	$("#city_div").fadeIn();
	$("#add_new_city_btn").text("Hide City Form");
	$("#add_new_city_btn").addClass("hide_form");
	$('html, body').animate({
		scrollTop: $("#city_div").offset().top
	},1000);


	});

	var validator = $("#city_form").validate();
		   validator.resetForm();
		   $("input").parent().removeClass("has-error");
}


