var handleDataTableButtons = function() {
	"use strict";
	0 !== $("#datatable-buttons").length && $("#datatable-buttons").DataTable({
		bProcessing	 	: true,
		ajax: {
						url: base_url+'admin/customer/get_customer_list',
						type: "POST",

						dataType:'json'
					},
		dom: "Bfrtip",
		serverSide     	: true,
		lengthMenu: [
		[ 10, 25, 50, -1 ],
		[ '10 rows', '25 rows', '50 rows', 'Show all' ]
		],
		buttons: [
		{
			extend: "pageLength",
			className: "btn-sm"
		},
		{
			extend: "copy",
			className: "btn-sm"
			}, {
			extend: "csv",
			className: "btn-sm"
			}, {
			extend: "excel",
			className: "btn-sm"
			}, {
			extend: "pdf",
			className: "btn-sm"
			}, {
			extend: "print",
			className: "btn-sm"
		}],
		responsive: !0,
		aoColumns  	 	: [{

			mData		: "cust_id",
			sName		: "No",
			sTitle     	: "Sr No." ,
			searchable 	: false,
			orderable  	: false,
			targets    	: 0
		},

		{
			mData		: "name", // database coloum name
			sName		: "name", // database coloum name
			sTitle		: "Customer Name", //HTML column name(Title)
			searchable 	: true,
			orderable  	: true
		},
		//Hamid Issue
		{
			mData		: "phone", // database coloum name
			sName		: "phone", // database coloum name
			sTitle		: "Customer phone Number", //HTML column name(Title)
			searchable 	: true,
			orderable  	: true
        },
        

		{
			mData		: "email", // database coloum name
			sName		: "email", // database coloum name
			sTitle		: "Customer email", //HTML column name(Title)
			searchable 	: true,
			orderable  	: true
		},
		{
			mData		: "created_date", // database coloum name
			sName		: "created_date", // database coloum name
			sTitle		: "Created Date", //HTML column name(Title)
			searchable 	: false,
			orderable  	: false,
			targets    	: 0
		},

		{
			mData		: "modified_date", // database coloum name
			sName		: "modified_date", // database coloum name
			sTitle		: "Modified Date", //HTML column name(Title)
			searchable 	: false,
			orderable  	: false,
			targets    	: 0
		},

		{
			mData: "active", //database coloum name
			sTitle: "Active Status", //HTML column name(Title)
			searchable 	: false,
			orderable  	: false,
			targets    	: 0,
			mRender: function ( data , type, row )  {
				if(row.active==0){
					return '<button class="btn btn-danger btn-xs" type="button" disabled>Inactive</button>';
				}else{
					return '<button class="btn btn-success btn-xs" type="button" disabled>Active</button>';
				}
			}
		},

		{
			mData: "id", //database coloum name
			sTitle: "Action", //HTML column name(Title)
			searchable 	: false,
			orderable  	: false,
			targets    	: 0,
			mRender: function ( data , type, row )
			{
				return  '<a href="javascript:editd('+row.cust_id+');" id="edit_click" class="btn btn-success btn-xs" style="margin-right: 0px;">Edit</a>  <a href="javascript:remove('+row.cust_id+');" class="btn btn-danger btn-xs" style="margin-right: 0px;">Remove</a>';
			}

		}
		],
		fnDrawCallback: function ( oSettings ) {
			for ( var i=0, iLen=oSettings.aiDisplay.length ; i<iLen ; i++ )
			{
				$('td:eq(0)', oSettings.aoData[ oSettings.aiDisplay[i] ].nTr ).html( oSettings._iDisplayStart+i+1 );
			}
		},
		order: [[ 1, 'asc' ]],

		autoWidth :true,
	})
},
TableManageButtons = function() {
	"use strict";
	return {
		init: function() {
			handleDataTableButtons()
		}
	}
}();
$("#datatable-buttons thead th input[type=text]").on( 'keyup change', function () {
	$("#datatable-buttons").DataTable()
	.search( this.value )
	.draw();

});

function remove(id)
{
	var confirm_message = confirm("Are you sure you want to delete this record?");
	if(confirm_message==true)
	{
		var ur=base_url+'admin/customer/customer_delete';
		$.ajax({
			type: 'post',
			url: ur,
			data: {
				id : id
			},
			dataType : 'json'
		})
		.done(function (response) {
			document.getElementById("succmsgDiv").style.display="block";
			document.getElementById("succmsg").innerHTML=response.message;
			$("#succmsgDiv").fadeOut(4000);
			$('#customer_form').trigger("reset");
			$("#id").val(0);
			$("#datatable-buttons").DataTable().ajax.reload();
		});
   }
}

$(document).ready(function(){
	$(document).on('click', "#add_new_customer_btn", function(){
		if($("#add_new_customer_btn").hasClass("hide_form"))
		{
			$("#customer_div").fadeOut();
			$("#add_new_customer_btn").text("Add customer");
			$("#add_new_customer_btn").removeClass("hide_form");
			$('html, body').animate({
				scrollTop: $("#customer_div").offset().top
			 }, 1000);
		}
		else{
			$("#customer_form")[0].reset(); // Added
			$("#customer_div").fadeIn();
			$('.error').html('');
			$("#add_new_customer_btn").text("Hide customer Form");
			$("#add_new_customer_btn").addClass("hide_form");
			 $('html, body').animate({
				scrollTop: $("#customer_div").offset().top
			 },1000);


		}
	});
	$(document).on('click','.hide_form',function(){
		 // otable.ajax.reload();
		 window.location.reload();
	});

	$(document).on('click', '#edit_click', function(){  // Added
		$("#add_new_customer_btn").click();
	});
	
	$(document).on('click','#cancel',function(){
		$("#").fadeOut();
		$("#add_new_customer_btn").text("Add customer");
		$("#datatable-buttons").DataTable().ajax.reload();
	});

});customer_div

$(document).ready(function(){
	// validate signup form on keyup and submit
	$('#customer_form').validate({
		submitHandler:function(form)
		{
			var formData = new FormData(form);
			$.ajax({
				type: $(form).attr('method'),
				url: base_url+'admin/customer/save_customer',
				data: formData,
				dataType : 'json',
				processData: false,
                contentType: false,
			})
			.done(function (response) {
				Form_response(response)
			});
		},
		errorElement: 'span',
		errorClass: 'help-block error',
		rules:
		{
			name:
			{
				required:true
            },
            
			phone:
			{
				required:true,
				digits:true,
				minlength:10,
				maxlength:10
            },
            
			email:
			{
				required:true,
				email:true
			},
			status:
			{
				required:true
			}
		},
		messages:
		{
			name:
			{
				required : "Please Enter Customer Name"
            },
            phone:
			{
				required : "Please Enter Customer Phone Number",
				digits:"Please Enter Valid Phone Number",
				minlength:"minimum 10 digits allowed",
				maxlength:"maximum 10 digits allowed"
            },
			email:
			{
				required : "Please Enter Customer email",
				email:"Please Enter Valid Email"
			},
			status:
			{
				required:"Please select status"
			}
		},
		ignore: []
	});
});

function Form_response(response)
{
	if(response.success==true)
	{
		document.getElementById("succmsgDiv").style.display="block";
		document.getElementById("succmsg").innerHTML=response.message;
		$("#succmsgDiv").fadeOut(4000);
		$("#customer_form").validate().resetForm();
		//$('#customer_form').trigger("reset");
		$("#id").val(0);
		$("#customer_div").fadeOut(); // Added
		$("#add_new_customer_btn").text("Add customer"); // Added
		$("#datatable-buttons").DataTable().ajax.reload();
	}
	else
	{
		document.getElementById("errormsgDiv").style.display="block";
		document.getElementById("errormsg").innerHTML=response.message;
		$("#errormsgDiv").fadeOut(4000);
	}
}

function editd(id)
{
	//alert(id);
	var ur=base_url+'admin/customer/customer_edit';
	$.ajax({
		type: 'post',
		url: ur,
		data : {
			id : id
		},
		dataType : 'json'
	})
	.done(function (response) {
	$("#id").val(response.cust_id);
	$("#name").val(response.name);
	$("#phone").val(response.phone);
	$("#email").val(response.email);
	
	if (response.status == '1')
	{	
		$('#customer_div').find(':radio[name=status][value="1"]').iCheck('check');
	}
	else
	{
		$('#customer_div').find(':radio[name=status][value="0"]').iCheck('check');
	}
	$("#customer_div").fadeIn();
	$("#add_new_customer_btn").text("Hide customer Form");
	$("#add_new_customer_btn").addClass("hide_form");
	$('html, body').animate({
		scrollTop: $("#customer_div").offset().top
	},1000);


	});

	var validator = $("#customer_form").validate();
		   validator.resetForm();
		   $("input").parent().removeClass("has-error");
}