var handleDataTableButtons = function() {
	"use strict";
	0 !== $("#datatable-buttons").length && $("#datatable-buttons").DataTable({
		bProcessing	 	: true,
		ajax: {
						url: base_url+'admin/state/get_state_list',
						type: "POST",

						dataType:'json'
					},
		//ajax			 	: "state/get_state_list",
		dom: "Bfrtip",
		serverSide     	: true,
		lengthMenu: [
		[ 10, 25, 50, -1 ],
		[ '10 rows', '25 rows', '50 rows', 'Show all' ]
		],
		buttons: [
		{
			extend: "pageLength",
			className: "btn-sm"
		},
		{
			extend: "copy",
			className: "btn-sm"
			}, {
			extend: "csv",
			className: "btn-sm"
			}, {
			extend: "excel",
			className: "btn-sm"
			}, {
			extend: "pdf",
			className: "btn-sm"
			}, {
			extend: "print",
			className: "btn-sm"
		}],
		responsive: !0,
		aoColumns  	 	: [{

			mData		: "state_id",
			sName		: "No",
			sTitle     	: "Sr No." ,
			searchable 	: false,
			orderable  	: false,
			targets    	: 0
		},

		{
			mData		: "state_name", // database coloum name
			sName		: "state_name", // database coloum name
			sTitle		: "State Name", //HTML column name(Title)
			searchable 	: true,
			orderable  	: true
		},
		
		{
			mData		: "state_logo", // database coloum name
			sName		: "state_logo", // database coloum name
			sTitle		: "Card", //HTML column name(Title)
			searchable 	: true,
			orderable  	: true,
			mRender: function ( data , type, row )  
			{
				if(row.state_logo)
				{
					return ' <img src="'+base_url+state_url+'/'+row.state_logo+'" height="42" width="42"> ';
				}
				else
				{
					return "Not Set";
				}
				
			}
		},
		
		{
			mData		: "created_date", // database coloum name
			sName		: "created_date", // database coloum name
			sTitle		: "Created Date", //HTML column name(Title)
			searchable 	: false,
			orderable  	: false,
			targets    	: 0
		},

		{
			mData		: "modified_date", // database coloum name
			sName		: "modified_date", // database coloum name
			sTitle		: "Modified Date", //HTML column name(Title)
			searchable 	: false,
			orderable  	: false,
			targets    	: 0
		},

		{
			mData: "active", //database coloum name
			sTitle: "Active Status", //HTML column name(Title)
			searchable 	: false,
			orderable  	: false,
			targets    	: 0,
			mRender: function ( data , type, row )  {
				if(row.status==0){
					return '<button class="btn btn-danger btn-xs" type="button" disabled>Inactive</button>';
				}else{
					return '<button class="btn btn-success btn-xs" type="button" >Active</button>';
				}
			}
		},

		{
			mData: "id", //database coloum name
			sTitle: "Action", //HTML column name(Title)
			searchable 	: false,
			orderable  	: false,
			targets    	: 0,
			mRender: function ( data , type, row )
			{
				return  '<a href="javascript:editd('+row.state_id+');" id="edit_click" class="btn btn-success btn-xs" style="margin-right: 0px;">Edit</a>  <a href="javascript:remove('+row.state_id+');" class="btn btn-danger btn-xs" style="margin-right: 0px;">Remove</a>';
			}

		}
		],
		fnDrawCallback: function ( oSettings ) {
			for ( var i=0, iLen=oSettings.aiDisplay.length ; i<iLen ; i++ )
			{
				$('td:eq(0)', oSettings.aoData[ oSettings.aiDisplay[i] ].nTr ).html( oSettings._iDisplayStart+i+1 );
			}
		},
		order: [[ 1, 'asc' ]],

		autoWidth :true,
	})
},
TableManageButtons = function() {
	"use strict";
	return {
		init: function() {
			handleDataTableButtons()
		}
	}
}();
$("#datatable-buttons thead th input[type=text]").on( 'keyup change', function () {
	$("#datatable-buttons").DataTable()
	.search( this.value )
	.draw();

});

function remove(id)
{
	var confirm_message = confirm("Are you sure you want to delete this record?");
	if(confirm_message==true)
	{
		var ur=base_url+'admin/state/state_delete';
		$.ajax({
			type: 'post',
			url: ur,
			data: {
				id : id
			},
			dataType : 'json'
		})
		.done(function (response) {
			document.getElementById("succmsgDiv").style.display="block";
			document.getElementById("succmsg").innerHTML=response.message;
			$("#succmsgDiv").fadeOut(4000);
			$('#state_form').trigger("reset");
			$("#id").val(0);
			$("#datatable-buttons").DataTable().ajax.reload();
		});
   }
}

$(document).ready(function(){
	$(document).on('click', "#add_new_state_btn", function(){
		if($("#add_new_state_btn").hasClass("hide_form"))
		{
			$("#state_div").fadeOut();
			$("#add_new_state_btn").text("Add State");
			$("#add_new_state_btn").removeClass("hide_form");
			$('html, body').animate({
				scrollTop: $("#state_div").offset().top
			 }, 1000);
		}
		else{
			$("#state_form")[0].reset(); // Added
			$("#state_div").fadeIn();
			$('.error').html('');
			$("#add_new_state_btn").text("Hide State Form");
			$("#add_new_state_btn").addClass("hide_form");
			 $('html, body').animate({
				scrollTop: $("#state_div").offset().top
			 },1000);


		}
	});
	$(document).on('click','.hide_form',function(){
		 // otable.ajax.reload();
		 window.location.reload();
	});

	$(document).on('click', '#edit_click', function(){  // Added
		$("#add_new_state_btn").click();
	});

});

$(document).ready(function(){
	// validate signup form on keyup and submit
	$('#state_form').validate({
		submitHandler:function(form)
		{
			var formData = new FormData(form);
			$.ajax({
				type: $(form).attr('method'),
				url: base_url+'admin/state/save_state',
				data: formData,
				dataType : 'json',
				processData: false,
                contentType: false,
			})
			.done(function (response) {
				Form_response(response)
			});
		},
		errorElement: 'span',
		errorClass: 'help-block error',
		rules:
		{
			state_name:
			{
				required:true
			},
			status:
			{
				required:true
			}
		},
		messages:
		{
			state_name:
			{
				required:"Please Enter State Name"
			},
			status:
			{
				required:"Please select status"
			}
		},
		ignore: []
	});
});

function Form_response(response)
{
	if(response.success==true)
	{
		document.getElementById("succmsgDiv").style.display="block";
		document.getElementById("succmsg").innerHTML=response.message;
		$("#succmsgDiv").fadeOut(4000);
		$("#state_form").validate().resetForm();
		//$('#state_form').trigger("reset");
		$("#id").val(0);
		$("#state_div").fadeOut(); // Added
		$("#add_new_state_btn").text("Add State"); // Added
		$("#datatable-buttons").DataTable().ajax.reload();
	}
	else
	{
		document.getElementById("errormsgDiv").style.display="block";
		document.getElementById("errormsg").innerHTML=response.message;
		$("#errormsgDiv").fadeOut(4000);
	}
}

function editd(id)
{
	//alert(id);
	var ur=base_url+'admin/state/state_edit';
	$.ajax({
		type: 'post',
		url: ur,
		data : {
			id : id
		},
		dataType : 'json'
	})
	.done(function (response) {
	$("#id").val(response.state_id);
	$("#state_name").val(response.state_name);
	if(response.state_logo){
		$("#bannern").html("<img src='"+base_url+state_url+'/'+response.state_logo+"' id='bimage'  height='100' width='100'>");
		$("#banners").val(response.state_logo);
	}
	if (response.status == '1')
	{	
		$('#state_div').find(':radio[name=status][value="1"]').iCheck('check');
	}
	else
	{
		$('#state_div').find(':radio[name=status][value="0"]').iCheck('check');
	}
	$("#state_div").fadeIn();
	$("#add_new_state_btn").text("Hide State Form");
	$("#add_new_state_btn").addClass("hide_form");
	$('html, body').animate({
		scrollTop: $("#state_div").offset().top
	},1000);


	});

	var validator = $("#state_form").validate();
		   validator.resetForm();
		   $("input").parent().removeClass("has-error");
}

$(function() {
    $(document.body).on('change', '.state_logo' ,function(){
		var productId = $(this).attr('id');
		var files = !!this.files ? this.files : [];
        if (!files.length || !window.FileReader) return; // no file selected, or no FileReader support

        if (/^image/.test( files[0].type)){ // only image file
            var reader = new FileReader(); // instance of the FileReader
            reader.readAsDataURL(files[0]); // read the local file
			reader.onloadend = function(){ // set image data as background of div
				$("#bannern").html("<img src='"+this.result+"' id='bimage'  height='100' width='100'>");	
				$("#banners").val(files[0]['name']);
			}
        }
    });
});


