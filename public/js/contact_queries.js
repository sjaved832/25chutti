var handleDataTableButtons = function() {
	"use strict";
	0 !== $("#datatable-buttons").length && $("#datatable-buttons").DataTable({
		bProcessing	 	: true,
		ajax			: 
		{
            url		: base_url+"contact_queries/get_contact_query_list",
            cache	:false,
			type: "POST",
            data	: 
				function ( d ) 
				{
					d.startDate = $("#startDate").val();
                    d.endDate   = $("#endDate").val();
                }
        },
		//ajax			 	: base_url+"contact_queries/get_contact_query_list",
		dom: "Bfrtip",
		serverSide     	: true,
		lengthMenu: [
		[ 10, 25, 50, -1 ],
		[ '10 rows', '25 rows', '50 rows', 'Show all' ]
		],
		buttons: [
		{
			extend: "pageLength",
			className: "btn-sm"
		},
		{
			extend: "copy",
			className: "btn-sm"
			}, {
			extend: "csv",
			className: "btn-sm"
			}, {
			extend: "excel",
			className: "btn-sm"
			}, {
			extend: "pdf",
			className: "btn-sm"
			}, {
			extend: "print",
			className: "btn-sm"
		}],
		responsive: !0,
		aoColumns  	 	: [{
			
			mData		: "id",
			sName		: "No", 
			sTitle     	: "Sr No." ,
			searchable 	: false,
			orderable  	: false,
			targets    	: 0
		},
		
		{
			mData		: "name", // database coloum name
			sName		: "name", // database coloum name
			sTitle		: "Name", //HTML column name(Title)
			searchable 	: true,
			orderable  	: true
		},
		
		{
			mData		: "mobile", // database coloum name
			sName		: "mobile", // database coloum name
			sTitle		: "Contact Number", //HTML column name(Title)
			searchable 	: true,
			orderable  	: true
		},
		
		{
			mData		: "email", // database coloum name
			sName		: "email", // database coloum name
			sTitle		: "Email Id", //HTML column name(Title)
			searchable 	: true,
			orderable  	: true
		},
		
		{
			mData		: "message", // database coloum name
			sName		: "message", // database coloum name
			sTitle		: "Message", //HTML column name(Title)
			searchable 	: false,
			orderable  	: false
		},
		
		{
			mData: "id", //database coloum name
			sTitle: "Action", //HTML column name(Title)
			searchable 	: false,
			orderable  	: false,
			targets    	: 0,
			mRender: function ( data , type, row )
			{
				///console.log(write_access);
				if(write_access == 1 && delete_access == 1)
				{
					return  '<a href="javascript:viewd('+row.id+');" id="edit_click" class="btn btn-success btn-xs" style="margin-right: 0px;">View</a>  <a href="javascript:remove('+row.id+');" class="btn btn-danger btn-xs" style="margin-right: 0px;">Remove</a>';
				}
				else if(write_access == 1)
				{
					return  '<a href="javascript:editd('+row.id+');" id="edit_click" class="btn btn-success btn-xs" style="margin-right: 0px;">Edit</a>';
				}
				else if(delete_access == 1)
				{
					return  '<a href="javascript:remove('+row.id+');" class="btn btn-danger btn-xs" style="margin-right: 0px;">Remove</a>';
				}
				else
				{
					return 'NA';
				}
				
			}
			
		},
		
		{
			mData		: "ip_address", // database coloum name
			sName		: "ip_address", // database coloum name
			sTitle		: "IP Address", //HTML column name(Title)
			searchable 	: true,
			orderable  	: true,
			targets    	: 0
		},
		
		{
			mData		: "created_date", // database coloum name
			sName		: "created_date", // database coloum name
			sTitle		: "Query Date", //HTML column name(Title)
			searchable 	: true,
			orderable  	: true,
			targets    	: 0
		}
		
		],
		fnDrawCallback: function ( oSettings ) {
			for ( var i=0, iLen=oSettings.aiDisplay.length ; i<iLen ; i++ )
			{
				$('td:eq(0)', oSettings.aoData[ oSettings.aiDisplay[i] ].nTr ).html( oSettings._iDisplayStart+i+1 );
			}
		},
		order: [[ 1, 'asc' ]],
		
		autoWidth :true,
	})
},
TableManageButtons = function() {
	"use strict";
	return {
		init: function() {
			handleDataTableButtons()
		}
	}
}();
$("#datatable-buttons thead th input[type=text]").on( 'keyup change', function () {
	$("#datatable-buttons").DataTable()
	.search( this.value )
	.draw();
	
});

$('#re span').html('Select Date');
$('#re').daterangepicker({
		format: 'DD/MM/YYYY',
		showDropdowns: true,
		ranges: {
			   'Today': [moment(), moment()],
			   'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
			   'Last 7 Days': [moment().subtract(6, 'days'), moment()],
			   'Last 30 Days': [moment().subtract(29, 'days'), moment()],
			   'This Month': [moment().startOf('month'), moment().endOf('month')],
			   'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
			}
	}, 
	function(start, end, label) {
	/*console.log(start.format('YYYY-MM-DD'));
	console.log(end.format('YYYY-MM-DD'));*/
	$("#startDate").val(start.format('YYYY-MM-DD'));
	$("#endDate").val(end.format('YYYY-MM-DD'));
	$('#re span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
	$("#datatable-buttons").DataTable().draw(); 
});

function callDatatable(purpose){
	$("#datatable-buttons").DataTable().draw(); 
}

function remove(id)
{
	var confirm_message = confirm("Are you sure you want to delete this Contact Query ?");
	if(confirm_message==true)
	{
		var ur=base_url+'contact_queries/contact_query_delete';
		$.ajax({
			type: 'post',
			url: ur,
			data:{
				"id":id,
			},
			dataType : 'json'
		})
		.done(function (response) {
			document.getElementById("succmsgDiv").style.display="block";
			document.getElementById("succmsg").innerHTML=response.message;
			$("#succmsgDiv").fadeOut(4000);
			$("#datatable-buttons").DataTable().ajax.reload();
		});
	}	
}

function viewd(id)
{
	var ur=base_url+'contact_queries/contact_query_view';
	$.ajax({
		type: 'post',
		url: ur,
		data:{
			"id":id,
		},
		dataType : 'json'
	})
	.done(function (response) {
	$("#name").html(response.name);
	$("#email").html(response.email);
	$("#mobile").html(response.mobile);
	$("#message").html(response.message);
	$("#contact_query_div").fadeIn();
	$('html, body').animate({
		scrollTop: $("#contact_query_div").offset().top
	 }, 1000);

	});
}

$(document).on('click', "#cancel", function(){
	$("#contact_query_div").fadeOut();
	$("#name").html("");
	$("#email").html("");
	$("#mobile").html("");
	$("#message").html("");
	$('html, body').animate({
		scrollTop: $("#contact_query_div").offset().top
	}, 1000);
});