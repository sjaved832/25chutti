function search_init(){
    //For category init
    dynamic_select_list('api/category_list',"categories","category_id","category_name");

    // For state init
    dynamic_select_list('api/state_list',"state","state_id","state_name");

    // For city init
    dynamic_select_list('api/city_list',"city","city_id","city_name");
    
    // location
    dynamic_select_list('api/area_list',"area","area_id","area_name");    

}

function dynamic_select_list(url,id,select_value,select_label){

    var url = base_url+url;
	$.ajax({
		type: 'get',
		url: url,
		dataType : 'json'
	})
	.done(function (response) {
    var categories = $("#"+id);
    var category_list = response.data;
    category_list.forEach(cat_obj => {
        var option = $('<option/>');
        option.attr({ 'value': cat_obj[select_value] }).text(cat_obj[select_label]);
        categories.append(option); 
    });
    });
}

function get_city_by_state_id(state_id) 
{
var url = base_url+'api/city_list_by_state_id?id=' + state_id;

$.ajax({
    type : 'get',
    url : url,
    dataType : 'json'
}).done(function (res) {
    var city = $("#city");
    city.empty();
    var option = $('<option/>');
    option.attr({ 'value':'' }).text("select city");
    city.append(option);

    res.data.forEach(function(data) {
        var option = $('<option/>');
        option.attr({ 'value': data.city_id }).text(data.city_name);
        city.append(option);
    });
});
}
function get_area_by_city_id(city_id) 
{
var url = base_url+'api/localarea_list_by_city_id?id=' + city_id;

$.ajax({
    type : 'get',
    url : url,
    dataType : 'json'
}).done(function (res) {
    var area = $("#area");
    area.empty();
    var option = $('<option/>');
    option.attr({ 'value':'' }).text("select location");
    area.append(option);

    res.data.forEach(function(data) {
        var option = $('<option/>');
        option.attr({ 'value': data.area_id }).text(data.area_name);
        area.append(option);
    });
});
}