var handleDataTableButtons = function() {
			mData: "active", //database coloum name
	"use strict";
	0 !== $("#datatable-buttons").length && $("#datatable-buttons").DataTable({
		bProcessing	 	: true,
		ajax: {
						url: base_url+'admin/localarea/get_localarea_list',
						type: "POST",

						dataType:'json'
					},
		//ajax			 	: "localarea/get_localarea_list",
		dom: "Bfrtip",
		serverSide     	: true,
		lengthMenu: [
		[ 10, 25, 50, -1 ],
		[ '10 rows', '25 rows', '50 rows', 'Show all' ]
		],
		buttons: [
		{
			extend: "pageLength",
			className: "btn-sm"
		},
		{
			extend: "copy",
			className: "btn-sm"
			}, {
			extend: "csv",
			className: "btn-sm"
			}, {
			extend: "excel",
			className: "btn-sm"
			}, {
			extend: "pdf",
			className: "btn-sm"
			}, {
			extend: "print",
			className: "btn-sm"
		}],
		responsive: !0,
		aoColumns  	 	: [{

			mData		: "area_id",
			sName		: "No",
			sTitle     	: "Sr No." ,
			searchable 	: false,
			orderable  	: false,
			targets    	: 0
		},

		{
			mData		: "area_name", // database coloum name
			sName		: "area_name", // database coloum name
			sTitle		: "Area", //HTML column name(Title)
			searchable 	: true,
			orderable  	: true
		},

		{
			mData		: "pincode", // database coloum name
			sName		: "pincode", // database coloum name
			sTitle		: "pincode", //HTML column name(Title)
			searchable 	: true,
			orderable  	: true
		},
		{
			mData		: "created_date", // database coloum name
			sName		: "created_date", // database coloum name
			sTitle		: "Created Date", //HTML column name(Title)
			searchable 	: false,
			orderable  	: false,
			targets    	: 0
		},

		{
			mData		: "modified_date", // database coloum name
			sName		: "modified_date", // database coloum name
			sTitle		: "Modified Date", //HTML column name(Title)
			searchable 	: false,
			orderable  	: false,
			targets    	: 0
		},

		{
			sTitle: "Active Status", //HTML column name(Title)
			searchable 	: false,
			orderable  	: false,
			targets    	: 0,
			mRender: function ( data , type, row )  {
				if(row.status==0){
					return '<button class="btn btn-danger btn-xs" type="button" disabled>Inactive</button>';
				}else{
					return '<button class="btn btn-success btn-xs" type="button" disabled>Active</button>';
				}
			}
		},

		{
			mData: "id", //database coloum name
			sTitle: "Action", //HTML column name(Title)
			searchable 	: false,
			orderable  	: false,
			targets    	: 0,
			mRender: function ( data , type, row )
			{
				return  '<a href="javascript:editd('+row.area_id+');" id="edit_click" class="btn btn-success btn-xs" style="margin-right: 0px;">Edit</a>  <a href="javascript:remove('+row.area_id+');" class="btn btn-danger btn-xs" style="margin-right: 0px;">Remove</a>';
			}

		}
		],
		fnDrawCallback: function ( oSettings ) {
			for ( var i=0, iLen=oSettings.aiDisplay.length ; i<iLen ; i++ )
			{
				$('td:eq(0)', oSettings.aoData[ oSettings.aiDisplay[i] ].nTr ).html( oSettings._iDisplayStart+i+1 );
			}
		},
		order: [[ 1, 'asc' ]],

		autoWidth :true,
	})
},
TableManageButtons = function() {
	"use strict";
	return {
		init: function() {
			handleDataTableButtons()
		}
	}
}();
$("#datatable-buttons thead th input[type=text]").on( 'keyup change', function () {
	$("#datatable-buttons").DataTable()
	.search( this.value )
	.draw();

});

function remove(id)
{
	var confirm_message = confirm("Are you sure you want to delete this record?");
	if(confirm_message==true)
	{
		var ur=base_url+'admin/localarea/localarea_delete';
		$.ajax({
			type: 'post',
			url: ur,
			data: {
				id : id
			},
			dataType : 'json'
		})
		.done(function (response) {
			document.getElementById("succmsgDiv").style.display="block";
			document.getElementById("succmsg").innerHTML=response.message;
			$("#succmsgDiv").fadeOut(4000);
			$('#localarea_form').trigger("reset");
			$("#id").val(0);
			$("#datatable-buttons").DataTable().ajax.reload();
		});
   }
}

$(document).ready(function(){
	$(document).on('click', "#add_new_localarea_btn", function(){
		if($("#add_new_localarea_btn").hasClass("hide_form"))
		{
			$("#localarea_div").fadeOut();
			$("#add_new_localarea_btn").text("Add localarea");
			$("#add_new_localarea_btn").removeClass("hide_form");
			$('html, body').animate({
				scrollTop: $("#localarea_div").offset().top
			 }, 1000);
		}
		else{
			$("#localarea_form")[0].reset(); // Added
			$("#localarea_div").fadeIn();
			$('.error').html('');
			$("#add_new_localarea_btn").text("Hide localarea Form");
			$("#add_new_localarea_btn").addClass("hide_form");
			 $('html, body').animate({
				scrollTop: $("#localarea_div").offset().top
			 },1000);


		}
	});
	$(document).on('click','.hide_form',function(){
		 // otable.ajax.reload();
		 window.location.reload();
	});

	$(document).on('click', '#edit_click', function(){  // Added
		$("#add_new_localarea_btn").click();
	});

});

$(document).ready(function(){
	// validate signup form on keyup and submit
	$('#localarea_form').validate({
		submitHandler:function(form)
		{
			var formData = new FormData(form);
			$.ajax({
				type: $(form).attr('method'),
				url: base_url+'admin/localarea/save_localarea',
				data: formData,
				dataType : 'json',
				processData: false,
                contentType: false,
			})
			.done(function (response) {
				debugger;
				Form_response(response)
			});
		},
		errorElement: 'span',
		errorClass: 'help-block error',
		rules:
		{
			state_id:
			{
				required:true
			},
			city_id:
			{
				required:true
			},
			area_name:
			{
				required:true
			},
			pincode:
			{
				required:true,
				digits:true,
				maxlength:6,
				minlength:6
			},
			status:
			{
				required:true
			}
		},
		messages:
		{
			state_id:
			{
				required:"Please Select State"
			},
			city_id:
			{
				required:"Please Select City"
			},
			area_name:
			{
				required:"Please Enter Area"
			},
			pincode:
			{
				required:"Please Enter pincode",
				digits:"Please Enter valid pincode",
				maxlength:"Maximum 6 digits required",
				minlength:"Minimum 6 digits required"
			},
			status:
			{
				required:"Please select status"
			}
		},
		ignore: []
	});
});

function Form_response(response)
{
	if(response.success==true)
	{
		document.getElementById("succmsgDiv").style.display="block";
		document.getElementById("succmsg").innerHTML=response.message;
		$("#succmsgDiv").fadeOut(4000);
		$("#localarea_form").validate().resetForm();
		$('#localarea_form').trigger("reset");
		$("#localarea_div").fadeOut(); // Added
		$("#add_new_localarea_btn").text("Add localarea"); // Added
		$("#datatable-buttons").DataTable().ajax.reload();
	}
	else
	{
		document.getElementById("errormsgDiv").style.display="block";
		document.getElementById("errormsg").innerHTML=response.message;
		$("#errormsgDiv").fadeOut(4000);
	}
}

function editd(id)
{
	//alert(id);
	var ur=base_url+'admin/localarea/localarea_edit';
	$.ajax({
		type: 'post',
		url: ur,
		data : {
			id : id
		},
		dataType : 'json'
	})
	.done(function (response) {
	get_city_by_state(response.state_id,response.city_id);
	$("#id").val(response.area_id);
	$("#area_name").val(response.area_name);
	$("#pincode").val(response.pincode);
	$("#state_id").val(response.state_id);
	$("#city_id").val(response.city_id);
	debugger;
	if (response.status == '1')
	{
		$('#localarea_div').find(':radio[name=status][value="1"]').iCheck('check');
	}
	else
	{
		$('#localarea_div').find(':radio[name=status][value="0"]').iCheck('check');
	}
	$("#localarea_div").fadeIn();
	$("#add_new_localarea_btn").text("Hide localarea Form");
	$("#add_new_localarea_btn").addClass("hide_form");
	$('html, body').animate({
		scrollTop: $("#localarea_div").offset().top
	},1000);


	});

	var validator = $("#localarea_form").validate();
		   validator.resetForm();
		   $("input").parent().removeClass("has-error");
}

$(document).ready(function(){
	
	$.ajax({
	         url : base_url+'admin/localarea/get_state',
	         method:"POST",
	         dataType:'json',
	         success : function(response)
	         {
		       //var state_id = $('#state_id').val();
		       $.each(response, function(key,val)
		       {
			      $('#state_id').append($('<option>').text(val.state_name).attr('value', val.state_id));
		       })
	         }
        });
});

     
function get_city_by_state(state_id,city_id=null)
{
	    $.ajax({
	         url : base_url+'admin/localarea/get_city_by_state/'+state_id,
	         method:"POST",
	         dataType:'json',
	         success : function(response)
	         {
		       $('#city_id').html('<option value="">----Please Select Select-----</option>');
		       $.each(response, function(key,val)
		       {
			      $('#city_id').append($('<option>').text(val.city_name).attr('value', val.city_id));
		       });
				if(city_id)
				{
					$("#city_id").val(city_id).change();
				} 
	         }
        });
}


