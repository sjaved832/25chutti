
<?php
defined('BASEPATH') OR exit('No direct script access allowed');
// This can be removed if you use __autoload() in config.php OR use Modular Extensions
/** @noinspection PhpIncludeInspection */
require APPPATH . 'libraries/REST_Controller.php';
class customMapper {
    private $baseUrl = NULL;
    public function __construct($baseUrl){
        $this->baseUrl = $baseUrl;
    }
    public function hotelMapper($data){
        $data["link"] = $this->baseUrl."hotel-details/".$data['hotel_id'];
        $data['text'] = $data["hotel_name"];
        return $data; 
    }

    public function stateMapper($data){
        $data["link"] = $this->baseUrl."hotels?state=".$data['state_id'];
        $data['text'] = $data["state_name"];
        return $data; 
    }
}

class Api extends REST_Controller {
    function __construct()
    {
        // Construct the parent class
        parent::__construct();
        // Configure limits on our controller methods
        // Ensure you have created the 'limits' table and enabled 'limits' within application/config/rest.php
        //Result format
        $this->result = array("status"=>true,"message"=>"success","data"=>array());

        // Load Models
        $this->load->model("admin/hotels_model");
		$this->load->model('admin/state_model');
        $this->load->model('admin/packages_model');
        $this->load->model('admin/city_model');
        $this->load->model('admin/facility_model');
    }
	
    function package_list_get()
    {
        $packages = $this->packages_model->get_packages_list();
        if(isset($packages['data']))
            $this->result['data'] = $packages['data'];
        $this->response($this->result);
    }
    function hotel_list_get(){
        $package = null;
		if($this->input->get("package"))
		{
			$package = $this->input->get("package");
		}
		$state = null;
		if($this->input->get("state"))
		{
			$state = $this->input->get("state"); 
		}
        
        $page = 0;
        if($this->input->get("page"))
		{
			$page = $this->input->get("page");
		}
        $limit = 2;
        if($this->input->get("limit"))
		{
			$limit = $this->input->get("limit");
        }
        $limit = $page + $limit;
        $result = $this->hotels_model->get_hotels_by_package($package , $state,$page,$limit);
        
        $arrarFilter = array();
        foreach($result as $key => $value){
            if($page<= $key && $limit >$key ){
                array_push($arrarFilter,$value);
            }
        }
        // array_splice($result,$page,$limit);
        $this->response($arrarFilter);
    }

    function search_get(){
        $search_query = $this->input->get('query');
        $hotels = $this->hotels_model->search_hotel($search_query);
        $states = $this->state_model->search_hotel_states_wise($search_query);
        // $hotelLinkMapper = function($hotel_obj){
        //     $hotel_obj['link'] = "www.facebook.com";
        //     return $hotel_obj;
        // };
        // $stateLinkMapper = function($hotel_obj){
        //     $hotel_obj['link'] ="www.facebook.com";
        //     return $hotel_obj;
        // };
        $baseUrl = $this->config->item("base_url");
        $hotels = array_map(array(new customMapper($baseUrl), 'hotelMapper'), $hotels);
        $states = array_map(array(new customMapper($baseUrl), 'stateMapper'), $states);
        // array_map($hotelLinkMapper,$hotels);
        // array_map($stateLinkMapper,$states);
        $result = array(
            "hotels"=>array("hamid","dssd"),
            "states"=>array("Maharashtra","Goa"),
            "search_query"=>$search_query
        );
        $result['hotelList'] = $hotels;
        $result['stateList'] = $states;
        $this->response($result);
    }

    function state_list_get()
    {
		$state = $this->state_model->get_active_state_list();
        if(isset($state['data']))
            $this->result['data'] = $state['data'];
        $this->response($this->result);
    }

    function city_list_get()
    {
		$city = $this->city_model->get_active_city_list();
        if(isset($city['data']))
            $this->result['data'] = $city['data'];
        $this->response($this->result);
    }
    
    function city_list_by_state_id_get()
    {
		$city = $this->city_model->city_by_state_id($this->input->get("id"));
        if(isset($city['data']))
            $this->result['data'] = $city['data'];
        $this->response($this->result);
    }

    function facility_list_post()
    {
        $facility = $this->facility_model->get_facility_list();
        if(isset($facility['data']))
            $this->result['data'] = $facility['data'];
        $this->response($this->result);
    }
    
}
?>
