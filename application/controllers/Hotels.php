<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


class Hotels extends CI_Controller 
{

	function __construct()
    {
		parent::__construct();
		$this->load->model('admin/hotels_model');
		//$this->load->library('pagination');
    }

	function index()
	{
		$isHotel = true;
		$queryString = "";
		$package = null;
		if($this->input->get("package"))
		{
			$isHotel = false;
			$queryString .= "&package=".$this->input->get("package");
			$package = $this->input->get("package");
		}
		$state = null;
		if($this->input->get("state"))
		{
			$isHotel = false;
			$queryString .= "&state=".$this->input->get("state");
			$state = $this->input->get("state"); 
		}
		
		$result= $this->hotels_model->get_hotels_by_package($package , $state,0,10);
		
		$count_by_type = array(
			"hotel" => 0,
			"resort" => 0
		);
		
		foreach($result as $row){
			if($row['type'] == 1){
				$count_by_type["hotel"]++;
			}else{
				$count_by_type["resort"]++;
			}
		}
		
		
		$view_item = array(
			"upload_url" => $this->config->item('upload_url').'/'.$this->config->item('hotel_upload_url'),
			"result" => $result,
			"queryString" => $queryString,
			"count" => $this->hotels_model->get_hotels_count($package , $state),
			"facilities" => $this->hotels_model->get_all_facilities($state),
			"count_by_type" => $count_by_type,
			"isHotel" => $isHotel
		);
		// header('content-type:application/json');
		// echo json_encode($count_by_type);
		// exit;
		$this->load->view('common/header');
		$this->load->view('hotels/view_all_hotels',$view_item);
		$this->load->view('common/footer');
	}

	function hotels_list_by_state()
	{
		echo $state_id = $this->input->get('state');exit;
		$result= $this->hotels_model->get_hotels_list_by_id($id);
		$view_item = array(
			"upload_url" => $this->config->item('upload_url').'/'.$this->config->item('hotels_upload_url'),
			"result" => $result

		);
		$this->load->view('common/header');
		$this->load->view('hotels/view_all_hotels',$view_item);
		$this->load->view('common/footer');
	}
	
	function hotels_list_by_package()
	{
		echo $package_id = $this->uri->segment(2);exit;
		$result= $this->hotels_model->get_hotels_list_by_id($id);
		$view_item = array(
			"upload_url" => $this->config->item('upload_url').'/'.$this->config->item('hotels_upload_url'),
			"result" => $result

		);
		$this->load->view('common/header');
		$this->load->view('hotels/view_all_hotels',$view_item);
		$this->load->view('common/footer');
	}


	function details()
	{
		$hotels_id = $this->uri->segment(2);
		$hotels_details= $this->hotels_model->get_hotels_details_by_id( $hotels_id );
		$hotels_images_list= $this->hotels_model->get_hotels_images_by_id( $hotels_id );
		$view_item = array(
			"upload_url" => $this->config->item('upload_url').'/'.$this->config->item('hotels_upload_url'),
			"hotels_details" => $hotels_details,
			"hotels_images_list" => $hotels_images_list
		);
		$this->load->view('common/header');
		$this->load->view('hotels_details/hotels_details_view',$view_item);
		$this->load->view('common/footer');
	}
}


?>
