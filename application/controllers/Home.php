<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller
{
	public function __construct()
	{
		parent ::__construct();
		$this->load->model('home_model');
	}

	public function index()
	{
		$view_info = array(
			"state_url" => $this->config->item('upload_url').'/'.$this->config->item('state_upload_url'),
			"package_url" => $this->config->item('upload_url').'/'.$this->config->item('packages_upload_url'),
			'packages' => $this->home_model->get_packages(),
			'states' => $this->home_model->get_states()
		);
		
		$this->load->view('common/header');
		$this->load->view('home/home_view',$view_info);
		$this->load->view('common/footer');
	}
}
?>
