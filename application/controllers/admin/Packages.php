<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


class Packages extends CI_Controller 
{

	function __construct()
    {
		parent::__construct();
		$this->load->library('ion_auth');
		if (!$this->ion_auth->logged_in())
		{
		   //redirect them to the login page
		   redirect('auth/login', 'refresh');
		}
		$config = array();
		$config['upload_path']          = $this->config->item('upload_url').'/'.$this->config->item('packages_upload_url');
        $config['allowed_types']    	= $this->config->item('allowed_types');
		$config['max_size']             = 0; //2048000;
		$config['max_width']            = 0; //1024;
		$config['max_height']           = 0; //768;
		$config['remove_spaces'] 		= true;
		if (!is_dir($config['upload_path'])) {
			mkdir($config['upload_path'], 0777, true);
		}
		$this->load->library('upload', $config);
		$this->load->library('ion_auth');
		$this->load->model('admin/packages_model');
		$this->view_config = $config;
    }

	function index()
	{
		$this->load->view('admin/common/header');
		$this->load->view('admin/packages/packages_view',$this->view_config);
		$this->load->view('admin/common/footer');
	}

	function save_package()
	{
		$this->packages_model->save_package();
	}

	function get_packages_list()
	{
		$packages= $this->packages_model->get_packages_list();
		echo json_encode($packages);
	}

	function package_edit()
	{
		$response = $this->packages_model->package_edit();
		echo json_encode($response);
	}

	function package_delete()
	{
		$this->packages_model->package_delete();
	}
}


?>
