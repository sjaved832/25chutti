<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


class Hotel extends CI_Controller {

	function __construct()
    {
		parent::__construct();
		$this->load->library('ion_auth');
		if (!$this->ion_auth->logged_in())
		{
		   //redirect them to the login page
		   redirect('auth/login', 'refresh');
		}
		$config = array();
		$config['upload_path']          = $this->config->item('upload_url').'/'.$this->config->item('hotel_upload_url');
        $config['allowed_types']    	= $this->config->item('allowed_types');
		$config['max_size']             = 0; //2048000;
		$config['max_width']            = 0; //1024;
		$config['max_height']           = 0; //768;
		$config['remove_spaces'] 		= true;
		if (!is_dir($config['upload_path'])) {
			mkdir($config['upload_path'], 0777, true);
		}
		$this->load->library('upload', $config);
		$this->load->library('ion_auth');
		$this->load->model('admin/hotels_model');
		$this->view_config = $config;
    }

	function index()
	{
		$this->load->view('admin/common/header');
		$this->load->view('admin/hotel/hotel_view',$this->view_config);
		$this->load->view('admin/common/footer');
	}

	function save_basic_form(){
		$this->hotels_model->save_basic_form();
	}

	function get_hotels_list(){
		echo json_encode($this->hotels_model->get_hotels_list());
	}

	function hotel_edit(){
		echo json_encode($this->hotels_model->hotel_edit());
	}

	function hotel_delete(){
		$this->hotels_model->hotel_delete();
	}

	function save_details_form(){
		$this->hotels_model->save_details_form();
	}
	function save_image_form(){
		$this->hotels_model->save_image_form();
	}

	function get_image_list(){
		echo json_encode($this->hotels_model->get_image_list());
	}

	function image_edit(){
		echo json_encode($this->hotels_model->image_edit());
	}

	function image_delete(){
		$this->hotels_model->image_delete();
	}
	// function save_category()
	// {
	// 	$this->category_model->save_category();
	// }

	// function get_category_list()
	// {
	// 	$category= $this->category_model->get_category_list();
	// 	echo json_encode($category);
	// }

	// function category_edit()
	// {
	// 	$response = $this->category_model->category_edit();
	// 	echo json_encode($response);
	// }

	// function category_delete()
	// {
	// 	$this->category_model->category_delete();
	// }
}


?>
