<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


class State extends CI_Controller 
{
	
	function __construct()
    {
		parent::__construct();
		$this->load->library('ion_auth');
		if (!$this->ion_auth->logged_in())
		{
		   //redirect them to the login page
		   redirect('auth/login', 'refresh');
		}
		$config = array();
		$config['upload_path']          = $this->config->item('upload_url').'/'.$this->config->item('state_upload_url');
        $config['allowed_types']    	= $this->config->item('allowed_types');
		$config['max_size']             = 0; //2048000;
		$config['max_width']            = 0; //1024;
		$config['max_height']           = 0; //768;
		$config['remove_spaces'] 		= true;
		if (!is_dir($config['upload_path'])) {
			mkdir($config['upload_path'], 0777, true);
		}
		$this->load->library('upload', $config);
		$this->load->library('ion_auth');
		$this->load->model('admin/state_model');
		$this->view_config = $config;
    }
	
	function index()
	{
		$this->load->view('admin/common/header');
		$this->load->view('admin/state/state_view',$this->view_config);
		$this->load->view('admin/common/footer');
	}

	function save_state()
	{
		$this->state_model->save_state();
	}

	function get_state_list()
	{
		$state= $this->state_model->get_state_list();
		echo json_encode($state);
	}

	function state_edit()
	{
		$response = $this->state_model->state_edit();
		echo json_encode($response);
	}

	function state_delete()
	{
		$this->state_model->state_delete();
	}
}
?>
