<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


class Cabinet_type extends CI_Controller {

	function __construct()
    {
		// parent::__construct();
		// if( !$this->session->userdata('user_id') )
		// {
		// 	redirect(base_url());
		// }

        parent::__construct();
        $this->load->library('ion_auth');
        if (!$this->ion_auth->logged_in())
        {
          //redirect them to the login page
          redirect('admin/login', 'refresh');
        }
        $this->data['page_title'] = 'CI App - Dashboard';

		$this->load->model('cabinet_type_model');
		
    }
	
	function index()
	{
		$this->load->view('common/header');
		$this->load->view('cabinet_type/cabinet_type_view');
		$this->load->view('common/footer');
	}
	
	function save_cabinet_type()
	{
		$this->cabinet_type_model->save_cabinet_type();
	}

	function get_cabinet_type_list()
	{
		$cabinet_type = $this->cabinet_type_model->get_cabinet_type_list();
		echo json_encode($cabinet_type);		
	}

	function cabinet_type_edit()
	{	
		$response = $this->cabinet_type_model->cabinet_type_edit();
		echo json_encode($response);
	}

	function cabinet_type_delete()
	{
		$this->cabinet_type_model->cabinet_type_delete();
	}
	
	function get_parents()
	{
		$cabinet_type = $this->cabinet_type_model->get_parents();
		echo json_encode($cabinet_type);		
	}
}


?>