<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


class City extends CI_Controller {

	function __construct()
    {
		parent::__construct();
		$this->load->library('ion_auth');
		if (!$this->ion_auth->logged_in())
		{
		   //redirect them to the login page
		   redirect('auth/login', 'refresh');
		}
		$this->load->library('ion_auth');
		$this->load->model('admin/city_model');

    }

	function index()
	{
		$this->load->view('admin/common/header');
		$this->load->view('admin/city/city_view');
		$this->load->view('admin/common/footer');
	}

	function save_city()
	{
		$this->city_model->save_city();
	}

	function get_city_list()
	{
		$city= $this->city_model->get_city_list();
		echo json_encode($city);
	}

	function city_edit()
	{
		$response = $this->city_model->city_edit();
		echo json_encode($response);
	}

	function city_delete()
	{
		$this->city_model->city_delete();
	}
	function get_city_by_state_id(){
		$state_id = $this->uri->segment(4);
		$this->city_model->city_by_state_id($state_id);
	}

	function get_state()
	{
		$state = $this->city_model->get_state();
		echo json_encode($state);
	}
}


?>
