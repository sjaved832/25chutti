<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


class Facility extends CI_Controller {

	function __construct()
    {
		parent::__construct();
		$this->load->library('ion_auth');
		if (!$this->ion_auth->logged_in())
		{
		   //redirect them to the login page
		   redirect('auth/login', 'refresh');
		}
		$this->load->library('ion_auth');
		$this->load->model('admin/facility_model');

    }

	function index()
	{
		$this->load->view('admin/common/header');
		$this->load->view('admin/facility/facility_view');
		$this->load->view('admin/common/footer');
	}

	function save_facility()
	{
		$this->facility_model->save_facility();
	}

	function get_facility_list()
	{
		$facility= $this->facility_model->get_facility_list();
		
		echo json_encode($facility);
	}

	function facility_edit()
	{
		$response = $this->facility_model->facility_edit();
		echo json_encode($response);
	}

	function facility_delete()
	{
		$this->facility_model->facility_delete();
	}
	
}


?>
