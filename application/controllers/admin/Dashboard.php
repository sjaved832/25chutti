<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


class Dashboard extends CI_Controller { 

	public function __construct()
    {
		 parent::__construct();
		 $this->load->library('ion_auth');
		 if (!$this->ion_auth->logged_in())
		 {
		   //redirect them to the login page
		   redirect('auth/login', 'refresh');
		 }
		 $this->data['page_title'] = 'CI App - Dashboard';
 
    }
	
	public function index()
	{
		$this->load->view('admin/common/header');
		$this->load->view('admin/common/dashboard');
		$this->load->view('admin/common/footer');
	}
}

?>
