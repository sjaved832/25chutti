<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Home extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	public function __construct()
    {
		parent::__construct();
		$this->load->library('ion_auth');
		if (!$this->ion_auth->logged_in())
		{
		  //redirect them to the login page
		  redirect('auth/login', 'refresh');
		  exit;
		}
		
        $this->load->model("admin/home_model");		
    }

	public function index()
	{

		$this->load->view('admin/common/header');
		$this->load->view('admin/home');
		$this->load->view('admin/common/footer');
		
	}

	function ajaxCombo($res){
		$str ="<option value=''>-- Please Select --</option>";
		foreach($res as $row){
			$str.="<option value='".$row->f1."'>".$row->f2."</option>";
		}
		print $str;
	}

	function get_dropdown_search_data(){
		
		$input_search_text = $this->input->post('input_search_text');
		$db_table_name = $this->input->post('db_table_name');
		$search_box_id = $this->input->post('search_box_id');
		$result_table_id = $this->input->post('result_table_id');
		$value_box_id = $this->input->post('value_box_id');
		$depend_box_id = $this->input->post('depend_box_id');
		echo $this->home_model->get_dropdown_search_data($input_search_text,$db_table_name,$search_box_id,$result_table_id,$value_box_id,$depend_box_id);
	}
	
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */