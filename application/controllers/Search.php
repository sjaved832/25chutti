<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Search extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->model('admin/store_model');
		$this->load->model('admin/category_model');
	}

	function index()
	{
        $search_params = array();
        $search_params['category_id'] = $this->input->get("category_id");
        $search_params['area_id'] = $this->input->get("area_id");
        $search_params['city_id'] = $this->input->get("city_id");
        $search_params['state_id'] = $this->input->get("state_id");

        $category_list = array();
		$search_data = $this->store_model->get_search_data_by_query($search_params);
		if($this->input->get("area_id") != ""){
			$category_list = $this->category_model->get_categorylist_for_area($this->input->get("area_id"));
		}
				$view_item = array(
					"upload_url" => $this->config->item('upload_url').'/'.$this->config->item('store_upload_url'),
					"category_upload_url" => $this->config->item('upload_url').'/'.$this->config->item('category_upload_url'),
					"store_list" => $search_data,
					"category_list" => $category_list
				);

				// print_r($category_list);
				// exit;
			$this->load->view('common/header');
			$this->load->view('search/search_section');
			$this->load->view('store/view_all_store' ,$view_item);
			$this->load->view('common/footer');

	}
}

?>
