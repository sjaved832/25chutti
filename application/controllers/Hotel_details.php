<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Hotel_details extends CI_Controller 
{
	public function __construct()
	{
		parent::__construct();
		$this->load->model('admin/hotels_model');
	}
	
	public function index()
	{
		$hotel_id = $this->uri->segment(2);
		$hotel_details= $this->hotels_model->get_hotel_details_by_id( $hotel_id );
		$hotel_images_list= $this->hotels_model->get_hotel_images_by_id($hotel_id);
		$view_item = array(
			"upload_url" => $this->config->item('upload_url').'/'.$this->config->item('hotel_upload_url'),
			"hotel_details" => $hotel_details,
			"hotel_images_list" => $hotel_images_list
		);
		//print_r($view_item);exit;
		$this->load->view('common/header');
		$this->load->view('hotel_details/hotel_details_view',$view_item);
		$this->load->view('common/footer');
	}
}
?>
