<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Contact extends CI_Controller 
{
	public function index()
	{
		$this->load->view('common/header');
		$this->load->view('contact/contact_view');
		$this->load->view('common/footer');
	}
}
?>

