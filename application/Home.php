<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller
{
	public function __construct()
	{
		parent ::__construct();
		$this->load->model('home_model');
	}

	public function index()
	{
		$this->load->view('common/header');
		$this->load->view('home/home_view');
		$this->load->view('common/footer');
	}
	
	public function get_category_list()
	{
		$category = $this->category_model->get_category_list();
		echo json_encode($category);
	}
}
?>
