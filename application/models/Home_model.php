<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Home_model extends CI_Model 
{
	function __construct()
    {
        parent::__construct();
    }
	
	function get_states()
	{
		$query = $this->db->select("*");
		$this->db->from('state as a');
		$this->db->where('a.status','1');
		$this->db->where('a.flag','0');
		$this->db->where('a.state_logo !=', "");
		return $query->get()->result_array();
	}
	
	function get_packages()
	{
		$query = $this->db->select("*");
		$this->db->from('packages as a');
		$this->db->where('a.status','1');
		$this->db->where('a.flag','0');
		return $query->get()->result_array();
	}
}
?>