<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

// $this->db->where_in('id', array('20','15','22','42','86'));

class Hotels_model extends CI_Model
{
	function __construct()
    {
		parent::__construct();
		$this->load->model("admin/city_model");
		$this->load->model("admin/state_model");
    }

	function get_hotels_list()
	{

		$search_word 		= 	$this->input->post('search')['value'];
		$coloum_index 		= 	$this->input->post('order')[0]['column'];
		$order_by_column 	= 	$this->input->post('columns')[$coloum_index]['name'];
		$order_by 			= 	$this->input->post('order')[0]['dir'];
		$query = $this->db->select('SQL_CALC_FOUND_ROWS a.hotel_id, a.price,a.hotel_name, a.status, d.city_name as city_name',false);
		$this->db->from('hotels as a');
		$this->db->join('city as d','d.city_id = a.city_id', 'left');
		$this->db->where('a.flag','0');
		if($search_word != '')
		{
			// $query->like('a.description',$search_word);
			$query->or_like('a.hotel_name',$search_word);
			// $query->or_like('a.specification',$search_word);
			$query->or_like('d.city_name',$search_word);
		}

		if($this->input->post('start')!="" && $this->input->post('length')!="-1")
		{
			$query->limit($this->input->post('length'),$this->input->post('start'));
		}

		if($order_by!="")
		{
			$query->order_by($order_by_column,$order_by);
		}

		$result 					= 	$query->get()->result_array();
		//echo $this->db->last_query();
		$query 						= 	$this->db->query('SELECT FOUND_ROWS() AS `Count`');
		$total_rows 				= 	$query->row()->Count;

		$data['draw'] 				= 	$this->input->get('draw');
		$data['recordsTotal'] 		= 	$total_rows;
		$data['recordsFiltered'] 	= 	$total_rows;
		$data['data'] 				= 	$result;

		return $data;
	}

	function hotel_delete()
	{
		$hotel_id = $this->input->post('hotel_id');
		$updatetArray = array(
				'flag' 	=> 1,
				'modified_date'	=> date('Y-m-d H:i:s')
		);
		$this->db->where('hotel_id',$hotel_id);
		$this->db->update('hotels',$updatetArray);
		echo json_encode(array('success'=>true,'message'=>'Hotels Deleted Successfully'));
	}

	/**
	 * ${param} $query to search
	 * ${return} returns Array of data
	 */
	function search_hotel($search_query){
		$query = $this->db->select("hotel_name,hotel_id");
		$this->db->from('hotels');
		$this->db->where('flag','0');
		$this->db->like("hotel_name",$search_query);
		return $query->get()->result_array();;
	}
	
	function save_basic_form()
	{
		//print_r($this->input->post());exit;
		$error=0;
		$errorTxt='';
		if(addslashes($this->input->post("hotel_name"))=='')
		{ $error++; $errorTxt.='Please Enter Hotels Name';}

		if($error==0)
		{
			$facilities_ids = $this->input->post('facilities_ids');
			array_unshift($facilities_ids,"");

			if($this->input->post("b_hotel_id")	== 0 || $this->input->post("b_hotel_id") == '')
			{

				$query = $this->db->where("hotel_name",$this->input->post("hotel_name"))
									->where("flag","0")
										->get("hotels")
											->num_rows();
				if($query == 0)
				{
					
					$insertArray = array(
						'hotel_name' 			=> $this->input->post("hotel_name"),
						'price'					=> $this->input->post("price"),
						'style'					=> $this->input->post("style"),
						'setting'				=> $this->input->post("setting"),
						'extras'				=> $this->input->post("extras"),
						'city_id' 				=> $this->input->post("city_id"),
						'package_id' 			=> $this->input->post("package_id"),
						'facilities_ids'		=> implode(',',$facilities_ids),
						'type'					=> $this->input->post("type"),
						'status'				=> $this->input->post("status"),
						'added_by'				=> 'administrator',
						'created_date'			=> date('Y-m-d H:i:s'),
						'modified_date'			=> date('Y-m-d H:i:s')
					);
					$this->db->insert('hotels',$insertArray);
					

					$id = $this->db->insert_id();
					echo json_encode(array('success'=>true,'message'=>'Hotel Added Successfully ', 'hotel_id'=>$id));
				}
				else
				{
					echo json_encode(array('success'=>false,'message'=>'Hotel Already Exist'));
				}
			}
			else
			{
				$updatetArray = array(
						'hotel_name' 			=> $this->input->post("hotel_name"),
						'price'					=> $this->input->post("price"),
						'style'					=> $this->input->post("style"),
						'extras'				=> $this->input->post("extras"),
						'setting'				=> $this->input->post("setting"),
						'city_id' 				=> $this->input->post("city_id"),
						'package_id' 			=> $this->input->post("package_id"),
						'facilities_ids'		=> implode(',',$facilities_ids),
						'type'					=> $this->input->post("type"),
						'status'				=> $this->input->post("status"),
						'added_by'				=> 'administrator',
						'created_date'			=> date('Y-m-d H:i:s'),
						'modified_date'			=> date('Y-m-d H:i:s')
						);
				$this->db->where('hotel_id',$this->input->post("b_hotel_id"));
				$this->db->update('hotels',$updatetArray);
				
				echo json_encode(array('success'=>true,'message'=>'Basic Details Updated Successfully', 'hotel_id'=> $this->input->post("b_hotel_id")));
			}
		}
		else
		{
			echo json_encode(array('success'=>false,'message'=>$errorTxt));
		}
	}

	public function save_details_form()
	{
		$error=0;
		$errorTxt='';
		if(addslashes($this->input->post("highlights"))=='')
		{ $error++; $errorTxt.='Please Enter Highlights';}

		if($error==0)
		{
			if($this->input->post("d_hotel_id")	== 0 || $this->input->post("d_hotel_id") == '')
			{
				echo json_encode(array('success'=>false,'message'=>'Unauthorized Access'));
			}
			else
			{
				$updatetArray = array(
					'highlights' 		=> $this->input->post("highlights"),
					'offers' 			=> $this->input->post("offers"),
					'favourite_rooms' 	=> $this->input->post("favourite_rooms"),
					'added_by'		    => 'administrator',
					'modified_date'		=> date('Y-m-d H:i:s')
				);

				$this->db->where('hotel_id',$this->input->post("d_hotel_id"));
				$this->db->update('hotels',$updatetArray);
				echo json_encode(array('success'=>true,'message'=>'Details Info Added / Updated Successfully', 'hotel_id'=> $this->input->post("d_hotel_id")));
			}
		}
		else
		{
			echo json_encode(array('success'=>false,'message'=>$errorTxt));
		}
	}


	public function save_image_form()
	{
		$error=0;
		$errorTxt='';
		
		if($this->input->post("i_hotel_id")	== 0 || $this->input->post("i_hotel_id") == '')
		{ $error++; $errorTxt.='Some thing is wrong. Please Refresh Page.';}

		if($error==0)
		{
			if($this->input->post("image_id")	== 0 || $this->input->post("image_id") == '')
			{
				// print_r($_FILES);echo "<br>";print_r($this->input->post());exit;
				
				/*$query = 0;
				if($query == 0)
				{*/
					$insertArray = array(
						'hotel_id' 	    => $this->input->post("i_hotel_id"),
						'status'	    => $this->input->post("img_status"),
						'added_by'		=> 'administrator',
						'created_date'	=> date('Y-m-d H:i:s'),
						'modified_date'	=> date('Y-m-d H:i:s')
					);

					//$this->multiple_image_save($_FILES,$insertArray);
					$cpt = count($_FILES['image_name']['name']);
					for($i=0;$i<$cpt;$i++)
					{
						$_FILES['media']['name']= $_FILES['image_name']['name'][$i];
						$_FILES['media']['type']= $_FILES['image_name']['type'][$i];
						$_FILES['media']['tmp_name']= $_FILES['image_name']['tmp_name'][$i];
						$_FILES['media']['error']= $_FILES['image_name']['error'][$i];
						$_FILES['media']['size']= $_FILES['image_name']['size'][$i]; 
						$this->upload->do_upload('media');
						$insertArray['image_name'] = $this->upload->data('file_name');
						$this->db->insert('images',$insertArray);
					}

					
					echo json_encode(array('success'=>true,'message'=>'Hotels Image Added Successfully'));
				/*}
				else
				{
					echo json_encode(array('success'=>false,'message'=>'Hotels Image Already Exist'));
				}*/
			}
			else
			{

				$updateArray = array(
					'hotel_id' 		=> $this->input->post("i_hotel_id"),
					'status'			=> $this->input->post("img_status"),
					'added_by'			=> 'administrator',
					'modified_date'		=> date('Y-m-d H:i:s')
				);
				if($this->upload->do_upload('image_name'))
				{
					$upload_data = $this->upload->data();
					$updateArray['image_name'] = $upload_data["file_name"];
				}
				// else
				// {
				// 	$updateArray['image_name'] = $this->input->post('old_image');
				// }
				
				$this->db->where('image_id',$this->input->post("image_id"));
				$this->db->update('images',$updateArray);
	
				// $this->multiple_image_save($_FILES,$updateArray,$this->input->post("image_id"));
				// if($this->upload->do_upload('image_name'))
				// {
				// 	$upload_data = $this->upload->data();
				// 	$updateArray['image_name'] = $upload_data["file_name"];
				// }
				// $this->db->where('image_id',$this->input->post("image_id"));
				// $this->db->update('images',$updateArray);
				echo json_encode(array('success'=>true,'message'=>'Hotels Image Updated Successfully'));
			}
		}
		else
		{
			echo json_encode(array('success'=>false,'message'=>$errorTxt));
		}
	}

	
	function hotel_edit()
	{
		$hotel_id = $this->input->post('hotel_id');
		$query = $this->db->select("*");
		$this->db->from('hotels as a');
		$this->db->join('city as c','c.city_id = a.city_id','left');
		$this->db->join('state as b','c.state_id = b.state_id','left');
		$this->db->join('packages as p','p.package_id = a.package_id','left');
		$this->db->where("hotel_id",$hotel_id);
		$result = $query->get()->result_array();
		
		$response 	= array(
			'hotel_id' 	        => $result[0]["hotel_id"],
			'hotel_name' 		=> $result[0]["hotel_name"],
			'highlights'		=> $result[0]["highlights"],
			'price'				=> $result[0]["price"],
			'style'				=> $result[0]["style"],
			'setting'			=> $result[0]["setting"],
			'extras'			=> $result[0]["extras"],
			'offers'			=> $result[0]["offers"],
			'favourite_rooms'	=> $result[0]["favourite_rooms"],
			'city_id' 			=> $result[0]["city_id"],
			'state_id' 			=> $result[0]["state_id"],
			'package_id' 		=> $result[0]["package_id"],
			'facilities_ids'	=> $result[0]["facilities_ids"],
			'type'				=> $result[0]["type"],
			'status'			=> $result[0]["status"]
		);

		return $response;
	}

	function change_status()
	{
		$id = $this->input->post('id');
		$status 	= $this->input->post('status');
		if($status==1){
			$updatetArray['status'] = '0';
		}else{
			$updatetArray['status'] = '1';
		}
		$updatetArray['user_id'] = $this->session->userdata('user_id');
		$updatetArray['modified_date'] = date('Y-m-d H:i:s');
		$this->db->where('id',$this->input->post('id'));
		$this->db->update('hotels',$updatetArray);
		echo json_encode(
						array(
							'success'=>true,
							'message'=>"Hotels's Status Updated Successfully"
							)
						);
	}

	function get_hotels_count($package,$state)
	{
		$query = $this->db->select("*");
		$this->db->from('hotels as a');
		$this->db->join('city as c','c.city_id = a.city_id','left');
		$this->db->join('state as b','c.state_id = b.state_id','left');
		if(!is_null($package))
			$this->db->where("a.package_id",$package);
		if(!is_null($state))
			$this->db->where("b.state_id",$state);
		return $query->count_all_results(); 
	}

	//Last two parameter is for pagination
	function get_hotels_by_package($package,$state,$page,$limit)
	{
		$query = $this->db->select("*");
		$this->db->from('hotels as a');
		$this->db->join('city as c','c.city_id = a.city_id','left');
		$this->db->join('state as b','c.state_id = b.state_id','left');
		$this->db->join('images as i','i.hotel_id = a.hotel_id','left');
		$this->db->join('packages as p','p.package_id = a.package_id','left');
		if(!is_null($package))
			$this->db->where("a.package_id",$package);
		if(!is_null($state))
			$this->db->where("b.state_id",$state);
		
		$this->db->where("a.flag",0);
		$this->db->where("i.flag",0);
		$result = $query->get()->result_array();
		
		//To create
		$structure = array();
		foreach( $result as $row ) { //add rows to array by id
			$structure[ $row["hotel_id"] ] = $row + array( "images" => array() );
		}

		foreach( $result as $row ) { //link children to parents
			if( ! is_null($row["image_id"] ) && !is_null($structure[ $row["hotel_id"] ])) {
				$structure[ $row["hotel_id"] ]["images"][] = $row;    
			}
		}
		$formatedArray = array();
		foreach( $structure as $row ) { 
			$formatedArray[] = $row;
		}
		return $formatedArray;
	}
	
	function get_all_facilities($state)
	{
		$this->db->select('a.facilities_ids');
		$this->db->from('hotels as a');
		$this->db->join('city as c','c.city_id = a.city_id','left');
		$this->db->join('state as b','c.state_id = b.state_id','left');
		
		$this->db->where("b.state_id",$state);
		$this->db->where('a.facilities_ids !=', "0");
		$this->db->distinct();
		$query = $this->db->get();
		$result = $query->result_array();
		$text = "";
		foreach($result as $key => $value)
		{
			$text.= implode(',',$value);
		}
		
		$arr = array_unique(explode(',',$text));
		$facilities = array();
		foreach($arr as $key => $value)
		{
			$this->db->select('a.facility_id, a.facility_name');
			$this->db->from('facilities as a');
			$this->db->where('a.facility_id',$value);
			$query_facilities = $this->db->get();
			$facilities[] = $query_facilities->result_array();
		}
		
		return($facilities);
	}

	function get_state()
	{
		$query = $this->db->select("a.state_id, a.state_name");
		$this->db->from('state as a');
		$this->db->where("a.flag",'0');
		$this->db->where("a.status",'1');
		$response = $query->get()->result_array();
		return $response;
	}

	function get_city_by_state()
	{
		$state_id = $this->input->post('state_id');
		$query = $this->db->select("a.city_id, a.city_name");
		$this->db->from('city as a');
		$this->db->where("a.state_id",$state_id);
		$this->db->where("a.flag",'0');
		$this->db->where("a.status",'1');
		$response = $query->get()->result_array();
		return $response;
	}

	function get_area_by_city()
	{
		$city_id = $this->input->post('city_id');
		$query = $this->db->select("a.area_id, a.area_name");
		$this->db->from('area as a');
		$this->db->where("a.city_id",$city_id);
		$this->db->where("a.flag",'0');
		$this->db->where("a.status",'1');
		$response = $query->get()->result_array();
		return $response;
	}

	function get_area()
	{
		$this->db->select('area_id, area_name');
		$this->db->where('flag','0');
        $result = $this->db->get('area')->result_array();
		return $result;
	}

	function remove_image()
	{
		if($this->input->post('id'))
		{
			if($this->input->post('banner'))
			{
				if(file_exists('upload/hotels/'.$this->input->post('banner'))) {
					unlink('upload/hotels/'.$this->input->post('banner'));
				}
				$id = $this->input->post('id');
				$updatetArray = array(
						'banner' 	=> "",
						'modified_date'	=> date('Y-m-d H:i:s')
				);
				$this->db->where('id',$id);
				$this->db->update('hotels',$updatetArray);
				echo json_encode(array('success'=>true,'message'=>'Featured Image Removed Successfully'));
			}
			else if($this->input->post('small_img'))
			{
				if(file_exists('upload/hotels/'.$this->input->post('small_img'))) {
					unlink('upload/hotels/'.$this->input->post('small_img'));
				}
				$id = $this->input->post('id');
				$updatetArray = array(
						'small_img' 	=> "",
						'modified_date'	=> date('Y-m-d H:i:s')
				);
				$this->db->where('id',$id);
				$this->db->update('images',$updatetArray);
				echo json_encode(array('success'=>true,'message'=>'Thumbnail Image Removed Successfully'));
			}
			else if($this->input->post('image_name'))
			{
				if(file_exists('upload/hotels/'.$this->input->post('image_name'))) {
					unlink('upload/hotels/'.$this->input->post('image_name'));
				}
				$id = $this->input->post('id');
				$updatetArray = array(
						'image_name' 	=> "",
						'modified_date'	=> date('Y-m-d H:i:s')
				);
				$this->db->where('id',$id);
				$this->db->update('images',$updatetArray);
				echo json_encode(array('success'=>true,'message'=>'Hotels Image Removed Successfully'));
			}
		}
	}

	function get_image_list()
	{
		$search_word 		= 	$this->input->post('search')['value'];
		$coloum_index 		= 	$this->input->post('order')[0]['column'];
		$order_by_column 	= 	$this->input->post('columns')[$coloum_index]['name'];

		$order_by 	= 	$this->input->post('order')[0]['dir'];
		$query = $this->db->select('SQL_CALC_FOUND_ROWS a.*, b.hotel_name',false);
		$this->db->from('images as a');
		$this->db->join('hotels as b','a.hotel_id = b.hotel_id','inner');
		$this->db->where('a.flag','0');
		$this->db->where('a.hotel_id',$this->input->post('hotel_id'));
		if($search_word != '')
		{
			$query->like('a.image_name',$search_word);
		}

		if($this->input->post('start')!="" && $this->input->post('length')!="-1")
		{
			$query->limit($this->input->post('length'),$this->input->post('start'));
		}

		/*if($order_by!="")
		{
			$query->order_by('a.'.$order_by_column,$order_by);
		}*/

		$result = 	$query->get()->result_array();
        //echo $this->db->last_query();exit;
		$query 						= 	$this->db->query('SELECT FOUND_ROWS() AS `Count`');
		$total_rows 				= 	$query->row()->Count;

		$data['draw'] 				= 	$this->input->get('draw');
		$data['recordsTotal'] 		= 	$total_rows;
		$data['recordsFiltered'] 	= 	$total_rows;
		$data['data'] 				= 	$result;

		return $data;
	}

	function image_edit()
	{
		$id = $this->input->post('id');
		$query = $this->db->select("*");
		$this->db->from('images');
		$this->db->where("image_id",$id);
		$result = $query->get()->result_array();
		$response 	= array(
			'image_id' 		=> $result[0]["image_id"],
			'i_hotel_id' 	=> $result[0]["hotel_id"],
			'image_name' 	=> $result[0]["image_name"],
			'img_status'	=> $result[0]["status"],
		);

		return $response;
	}

	function image_delete()
	{
		$image_id = $this->input->post('image_id');
		$updatetArray = array(
				'flag' 		=> 1,
				'modified_date'	=> date('Y-m-d H:i:s')
		);
		$this->db->where('image_id',$image_id);
		$this->db->update('images',$updatetArray);
		echo json_encode(array('success'=>true, 'message'=>'Image Deleted Successfully'));
	}

	function change_status_img()
	{
		$id = $this->input->post('id');
		$status 	= $this->input->post('status');
		if($status==1){
			$updatetArray['status'] = '0';
		}else{
			$updatetArray['status'] = '1';
		}
		$updatetArray['user_id'] = $this->session->userdata('user_id');
		$updatetArray['modified_date'] = date('Y-m-d H:i:s');
		$this->db->where('id',$this->input->post('id'));
		$this->db->update('hotels_images',$updatetArray);
		echo json_encode(array('success'=>true, 'message'=>"Hotels Image Status Updated Successfully"));
	}

	function get_hotels_list_by_id($package_id)
	{
		$this->db->get_where('hotels','s');
		$this->db->select('a.*, d.city_name, e.state_name');
		$this->db->from('hotels as a');
		//$this->db->join('area as c','a.area_id = c.area_id');
		$this->db->join('city as d','a.city_id = d.city_id');
		$this->db->join('state as e','d.state_id = e.state_id');
		$this->db->join('images as f', 'f.hotel_id = a.hotel_id','left outer');
		$this->db->where( 'package_id' ,$package_id);
		$this->db->where('a.flag','0');
		$this->db->group_by('a.hotel_id');
		$this->db->order_by("a.hotel_name", "asc");
        $result = $this->db->get()->result_array();
		
		//echo $this->db->last_query();exit;
		//return $result;
		print_r($result);exit;
	}
	
	function get_hotel_images_by_id($hotel_id)
    {
		$this->db->select('*');
		$this->db->where('flag','0');
		$this->db->where( 'hotel_id' , $hotel_id );
		$this->db->order_by("created_date", "asc");
        $result = $this->db->get('images')->result_array();
		return $result;
	}

	function get_hotel_details_by_id($hotel_id)
	{
		$this->db->select('a.*, e.city_name, f.state_name');
		$this->db->from('hotels as a');
		$this->db->join('city as e','a.city_id = e.city_id');
		$this->db->join('state as f','e.state_id = f.state_id');
		$this->db->where('a.hotel_id' , $hotel_id );
		$this->db->where('a.flag','0');
        $result = $this->db->get()->result_array();
		//print_r($result);exit;
        $res = array();
        if (count($result)>0 ) {
        	$res = $result[0];
        }
		return $res;
	}
	
	
    
	public function get_search_data_by_query($query)
    {

		$area_ids = array();
		$this->db->select('a.*,b.city_name, d.state_name');
		$this->db->from('hotels as a');
		$this->db->join("city as b","b.city_id =c.city_id");
		$this->db->join("state as d","d.state_id = b.state_id");
    
		if((isset($query['city_id']) && $query['city_id'] != '') || (isset($query['state_id']) && $query['state_id'] != ''))
		{
			if(isset($query['city_id']) && $query['city_id']){
				$this->db->where("b.city_id =".$query['city_id']);
			}else{
				$this->db->where("d.state_id =".$query['state_id']);
			}
		}

		$this->db->where('a.flag','0');
		
        $query = $this->db->get();
		//echo $this->db->last_query();exit;
		$result = $query->result_array();
		//print_r($result);exit;
		return $result;
	}
}
