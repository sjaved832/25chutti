<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Facility_model extends CI_Model
{
	function __construct()
    {
        parent::__construct();
		$this->tableName = "facilities";
	}
	
	
	function get_facility_list()
	{

		$search_word 	= 	$this->input->post('search')['value'];
		$coloum_index = 	$this->input->post('order')[0]['column'];
		//$order_by_column 	= 	$this->input->post('columns')[$coloum_index]['facility_name'];
		$order_by 		= 	$this->input->post('order')[0]['dir'];
		$query = $this->db->select('SQL_CALC_FOUND_ROWS a.facility_id, a.facility_name, a.status, a.created_date, a.modified_date',false);
		$this->db->from($this->tableName.' as a');
		$this->db->where('a.flag','0');
		$this->db->where('a.status','1');
		
		if($search_word != '')
		{
			$query->like('a.facility_name',$search_word);
		}

		if($this->input->post('start')!="" && $this->input->post('length')!="-1")
		{
			$query->limit($this->input->post('length'),$this->input->post('start'));
		}

		/*if($order_by!="")
		{
			$query->order_by($order_by_column,$order_by);
		}*/

		$result = $query->get()->result_array();
	    //$this->db->last_query();exit;
		$query 						= 	$this->db->query('SELECT FOUND_ROWS() AS `Count`');
		$total_rows 				= 	$query->row()->Count;

		$data['draw'] 				= 	$this->input->get('draw');
		$data['recordsTotal'] 		= 	$total_rows;
		$data['recordsFiltered'] 	= 	$total_rows;
		$data['data'] 				= 	$result;
		
		return $data;
	}


	function facility_delete()
	{
		$facility_id = $this->input->post('facility_id');
		$updatetArray = array(
				'flag' 	=> 1,
				'modified_date'	=> date('Y-m-d H:i:s')
		);
		$this->db->where('facility_id',$facility_id);
		$this->db->update($this->tableName,$updatetArray);
		echo json_encode(
						array(
							'success'=>true,
							'message'=>'facility Deleted Successfully'
							)
						);
	}
	function facility_by_state_id($state_id)
	{
		// $state_id = $this->uri->segment(4);
		//$state_id = $this->input->get('id');
		$query = $this->db->select('SQL_CALC_FOUND_ROWS a.facility_id, a.facility_name, a.status, a.created_date, a.modified_date',false);
		$this->db->from($this->tableName.' as a');
		$this->db->where('a.state_id',$state_id);
		$this->db->where('a.flag','0');
		$this->db->where('a.status','1');
		$result = $query->get()->result_array();
		$data = array();
		$data['data'] = $result;
		return $data;
		// echo json_encode($data);
	}

	public function save_facility()
	{
		$facility_name = $this->input->post("facility_name");
		$error=0;
		$errorTxt='';
		if(addslashes($facility_name)=='')
		{ $error++; $errorTxt.='Please Enter facility Name';}

		if($error==0)
		{
			if($this->input->post("id")	== 0 || $this->input->post("id") == '')
			{

				$query = $this->db->where("facility_name",$facility_name)
									->where("status","1")
									    ->where("flag","0")
											->get($this->tableName)
												->num_rows();



				if($query == 0)
				{
					$insertArray = array(
						'facility_name' 	=> $facility_name,
						'status'		=> $this->input->post("status"),
						'added_by'	    => 'administrator',
						'created_date'	=> date('Y-m-d H:i:s'),
						'modified_date'	=> date('Y-m-d H:i:s')
					);

					$this->db->insert($this->tableName,$insertArray);
					$id = $this->db->insert_id();
					echo json_encode(array('success'=>true,'message'=>'facility Added Successfully '));
				}
				else
				{
					echo json_encode(array('success'=>false,'message'=>'facility Already Exist'));
				}
			}
			else
			{
				$updatetArray = array(
						'facility_name' => $facility_name,
						'status'		=> $this->input->post("status"),
						'added_by'	    => 'administrator',
						'modified_date'	=> date('Y-m-d H:i:s')
					);
				$this->db->where('facility_id',$this->input->post('id'));
				$this->db->update($this->tableName,$updatetArray);
				echo json_encode(array('success'=>true,'message'=>'facility Updated Successfully '));
			}
		}
		else
		{
			echo json_encode(array('success'=>false,'message'=>$errorTxt));
		}
	}

	function facility_edit()
	{
		$facility_id = $this->input->post('facility_id');
		$query = $this->db->select("*");
		$this->db->from($this->tableName);
		$this->db->where("facility_id",$facility_id);
		$result = $query->get()->result_array();
		//echo $this->db->last_query();exit;
		$response 	= array(
			'facility_id' 	=> $result[0]["facility_id"],
			'facility_name' => $result[0]["facility_name"],
			'status'	=> $result[0]["status"]
		);
		return $response;
	}

	
}
