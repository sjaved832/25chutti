<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Packages_model extends CI_Model
{
	function __construct()
    {
        parent::__construct();
        date_default_timezone_set('UTC');
	}

	function get_packages_list($order_by_col = "package_name" )
	{
		if($order_by_col == null ){
			$order_by_col = "package_name";
		}

		$search_word 		= 	$this->input->post('search')['value'];
		$coloum_index 		= 	$this->input->post('order')[0]['column'];
		//$order_by_column 	= 	$this->input->post('columns')[$coloum_index]['packages_name'];
		$order_by 			= 	$this->input->post('order')[0]['dir'];
		$query = $this->db->select('SQL_CALC_FOUND_ROWS a.*',false);
		$this->db->from('packages as a');
		$this->db->order_by("a.".$order_by_col, "asc");
		$this->db->where('a.flag','0');
		if($search_word != '')
		{
			$query->like('a.package_name',$search_word);
		}

		if($this->input->post('start')!="" && $this->input->post('length')!="-1")
		{
			$query->limit($this->input->post('length'),$this->input->post('start'));
		}

		/*if($order_by!="")
		{
			$query->order_by($order_by_column,$order_by);
		}*/

		$result = $query->get()->result_array();
		//echo $this->db->last_query();exit;
		$query 						= 	$this->db->query('SELECT FOUND_ROWS() AS `Count`');
		$total_rows 				= 	$query->row()->Count;

		$data['draw'] 				= 	$this->input->get('draw');
		$data['recordsTotal'] 		= 	$total_rows;
		$data['recordsFiltered'] 	= 	$total_rows;
		$data['data'] 				= 	$result;
		
		return $data;
	}

	function get_packageslist_for_area($area_id){
		$query = $this->db->select('a.*');
		$this->db->from('packages as a');
		// $this->db->order_by("packages_name", "asc");
		$this->db->join('store as b','b.packages_id = a.packages_id', 'inner');
		$this->db->where('b.area_id',$area_id);
		// $this->db->where('a.flag',0);
		return $query->get()->result_array();
	}

	function package_delete()
	{
		$package_id = $this->input->post('package_id');
		$updatetArray = array(
				'flag' 	=> 1,
				'modified_date'	=> date('Y-m-d H:i:s')
		);
		$this->db->where('package_id',$package_id);
		$this->db->update('packages',$updatetArray);
		echo json_encode(
						array(
							'success'=>true,
							'message'=>'package Deleted Successfully'
							)
						);
	}

	public function save_package()
	{
		$package_name = $this->input->post("package_name");

		$error=0;
		$errorTxt='';
		if(addslashes($package_name)=='')
		{ $error++; $errorTxt.='Please Enter package Name';}

		if($error==0)
		{
			if($this->input->post("id")	== 0 || $this->input->post("id") == '')
			{
				$query = $this->db->where("package_name",$package_name)
									->where("status","1")
									   ->where("flag","0")
											->get("packages")
												->num_rows();
				if($query == 0)
				{
					$insertArray = array(
						'package_name' 	=> $package_name,
						'status'		=> $this->input->post("status"),
						'added_by'	    => 'administrator',
						'created_date'	=> date('Y-m-d H:i:s'),
						'modified_date'	=> date('Y-m-d H:i:s')
					);
					if(!$this->upload->do_upload('package_logo'))
					{
						$error = array('success'=>false,'message' => $this->upload->display_errors());
						echo json_encode($error);
					}
					else
					{
						$upload_data = $this->upload->data();
						$insertArray['package_logo'] = $upload_data["file_name"];
							
					}
					$this->db->insert('packages',$insertArray);
					$id = $this->db->insert_id();
					echo json_encode(array('success'=>true,'message'=>'packages Added Successfully '));
				}
				else
				{
					echo json_encode(array('success'=>false,'message'=>'packages Already Exist'));
				}
			}
			else
			{
				$updatetArray = array(
						'package_name' => $package_name,
						'status'		=> $this->input->post("status"),
						'added_by'	    => 'administrator',
						'modified_date'	=> date('Y-m-d H:i:s')
					);
				if($this->upload->do_upload('package_logo'))
				{
              		$upload_data = $this->upload->data();
		    		$updatetArray['package_logo'] = $upload_data["file_name"];
							
				}
				else
				{
					$updatetArray['package_logo'] = $this->input->post("banners");
				}
                $this->db->where('package_id',$this->input->post('id'));
				$this->db->update('packages',$updatetArray);
				echo json_encode(array('success'=>true,'message'=>'package Updated Successfully '));
          // }
			}
		}
		else
		{
			echo json_encode(array('success'=>false,'message'=>$errorTxt));
		}
	}

	function package_edit()
	{
		$package_id = $this->input->post('package_id');
		$query = $this->db->select("*");
		$this->db->from('packages');
		$this->db->where("package_id",$package_id);
		$result = $query->get()->result_array();
		$response 	= array(
			'package_id' 					=> $result[0]["package_id"],
			'package_name' 					=> $result[0]["package_name"],
			'package_logo' 					=> $result[0]["package_logo"],
			'status'						=> $result[0]["status"]
		);
		return $response;
	}
}
