<div class="container footer">
	<div class="row social-row">
		<div class="col-md-4 col-md-offset-4 social">
		    <a href="https://instagram.com/" target="_blank"><i class="fa fa-instagram"></i></a>
            <a href="https://twitter.com/login" target="_blank"><i class="fa fa-twitter"></i></a>
            <a href="https://www.facebook.com/" target="_blank"><i class="fa fa-facebook"></i></a>
            <a href="https://in.pinterest.com/" target="_blank"><i class="fa fa-pinterest"></i></a>
            <a href="https://www.youtube.com/" target="_blank"><i class="fa fa-youtube"></i></a>
            <a href="https://plus.google.com/discover" target="_blank"><i class="fa fa-google"></i></a>
		</div>
	</div>
	<div class="row foot-first">
		<div class="col-xs-12 mobile-list" style="display:none;">
			<p class="dropdown-toggle" data-toggle="dropdown" id="menu1">About us <i class="glyphicon glyphicon-menu-down down-arrow"></i></p>
			<ul class="dropdown-menu detail-menu" aria-labelledby="menu1">
				<li><a href="#">Who we are</a></li>
				<li><a href="#">Why book with us?</a></li>
				<li><a href="#">Best-price guarantee</a></li>
				<li><a href="#">Our mdith24 travel experts</a></li>
				<li><a href="#">Our reviewers</a></li>
				<li><a href="#">Contact us</a></li>
			</ul>
		</div>
		<div class="col-xs-12 mobile-list" style="display:none;">
			<p class="dropdown-toggle" data-toggle="dropdown" id="menu2">Boutique hotel collections <i class="glyphicon glyphicon-menu-down down-arrow"></i></p>
			<ul class="dropdown-menu detail-menu" aria-labelledby="menu2">
				<li><a href="#">Easter break hotels</a></li>
				<li><a href="#">Beach hotels</a></li>
				<li><a href="#">Hotels with a spa</a></li>
				<li><a href="#">City break hotels</a></li>
				<li><a href="#">Honeymoon hotels</a></li>
				<li><a href="#">Child-friendly boutique hotels</a></li>
				<li><a href="#">Hotels with swimming pools</a></li>
			</ul>
		</div>
		<div class="col-xs-12 mobile-list" style="display:none;">
			<p class="dropdown-toggle" data-toggle="dropdown" id="menu3">Our top countries <i class="glyphicon glyphicon-menu-down down-arrow"></i></p>
			<ul class="dropdown-menu detail-menu" aria-labelledby="menu3">
				<li><a class="first" href="#">United Kingdom</a></li>
				<li><a href="#">Morocco</a></li>
				<li><a href="#">Spain</a></li>
				<li><a href="#">Italy</a></li>
				<li><a href="#">France</a></li>
				<li><a href="#">Portugal</a></li>
				<li><a href="#">Greece</a></li>
			</ul>
		</div>
		<div class="col-xs-12 mobile-list" style="display:none;">
			<p class="dropdown-toggle" data-toggle="dropdown" id="menu4">Top destinations<i class="glyphicon glyphicon-menu-down down-arrow"></i></p>
			<ul class="dropdown-menu detail-menu" aria-labelledby="menu4">
				<li><a href="#">Barcelona</a></li>
				<li><a href="#">London</a></li>
				<li><a href="#">Paris</a></li>
				<li><a href="#">Rome</a></li>
				<li><a href="#">New York</a></li>
				<li><a href="#">The Cotswolds</a></li>
				<li><a href="#">Marrakech</a></li>
			</ul>
		</div>
		
		<!--desktop menu-->
		<div class="col-md-2 col-md-offset-2 desktop-list">
			<p>About us</p>
			<ul>
				<li><a href="#">Who we are</a></li>
				<li><a href="#">Why book with us?</a></li>
				<li><a href="#">Best-price guarantee</a></li>
				<li><a href="#">Our mdith24 travel experts</a></li>
				<li><a href="#">Our reviewers</a></li>
				<li><a href="#">Contact us</a></li>
			</ul>
		</div>
		<div class="col-md-2 desktop-list">
			<p>Boutique hotel collections</p>
			<ul>
				<li><a href="#">Easter break hotels</a></li>
				<li><a href="#">Beach hotels</a></li>
				<li><a href="#">Hotels with a spa</a></li>
				<li><a href="#">City break hotels</a></li>
				<li><a href="#">Honeymoon hotels</a></li>
				<li><a href="#">Child-friendly boutique hotels</a></li>
				<li><a href="#">Hotels with swimming pools</a></li>
			</ul>
		</div>
		<div class="col-md-2 desktop-list">
			<p>Our top countries</p>
			<ul>
				<li><a class="first" href="#">United Kingdom</a></li>
				<li><a href="#">Morocco</a></li>
				<li><a href="#">Spain</a></li>
				<li><a href="#">Italy</a></li>
				<li><a href="#">France</a></li>
				<li><a href="#">Portugal</a></li>
				<li><a href="#">Greece</a></li>
			</ul>
		</div>
		<div class="col-md-2 desktop-list">
			<p>Top destinations</p>
			<ul>
				<li><a href="#">Barcelona</a></li>
				<li><a href="#">London</a></li>
				<li><a href="#">Paris</a></li>
				<li><a href="#">Rome</a></li>
				<li><a href="#">New York</a></li>
				<li><a href="#">The Cotswolds</a></li>
				<li><a href="#">Marrakech</a></li>
			</ul>
		</div>
	</div>
	<div class="row footer-menu-row">
		<ul class="footer-menu">
			<li><a href="#">About</a></li>
			<li><a href="#">Legal</a></li>
			<li><a href="#">mdith Travel Blog</a></li>
			<li><a href="#">Luxury villas</a></li>
			<li><a href="#">Partners</a></li>
			<li><a href="#">All our hotels</a></li>
		</ul>
	</div>
	<div class="row copyright">
		<div class="col-md-12">
			<p>© <script> document.write(new Date().getFullYear()) </script> 25 Chutti Global Ltd luxury hotels and villas</p>
		</div>
	</div>
</div>

</body>
</html>