<!DOCTYPE html>
<html lang="en">
<head>
  <title>25 Chutti</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="icon" href="<?php echo base_url();?>images/favicon.png" type="image/png">
  <link rel="stylesheet" href="<?php echo base_url();?>css/bootstrap.min.css">
  <!--link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css"-->
  <link rel="stylesheet" href="<?php echo base_url();?>css/style.css">
  <script src="<?php echo base_url();?>js/jquery-3.3.1.min.js"></script>
  <script src="<?php echo base_url();?>js/bootstrap.min.js"></script>

	<script src="<?=base_url()?>EasyAutocomplete/jquery.easy-autocomplete.js"></script>
	<link rel="stylesheet" href="<?=base_url()?>EasyAutocomplete/easy-autocomplete.css">

  <script type="text/javascript">
	var base_url = "<?php echo base_url();?>";
	var pathname = "<?php echo $this->uri->segment(1);?>"
  </script>
  <style>
	/* -----------------------------------------
Proxima Nova
----------------------------------------- */
/* regular */
@font-face {
	font-family: 'proxima-nova';
	src: url(fonts/proximanova-regular-webfont.eot);
	src: url(fonts/proximanova-regular-webfont.eot?iefix) format("eot"), url(fonts/proximanova-regular-webfont.woff) format("woff"), url(fonts/proximanova-regular-webfont.ttf) format("truetype"), url(fonts/proximanova-regular-webfont.svg#webfontZam02nTh) format("svg");
	font-weight: normal;
	font-style: normal;
}
/* light */
@font-face {
	font-family: 'proxima-nova';
	src: url(fonts/proximanova-light-webfont.eot);
	src: url(fonts/proximanova-light-webfont.eot?iefix) format("eot"), url(fonts/proximanova-light-webfont.woff) format("woff"), url(fonts/proximanova-light-webfont.ttf) format("truetype"), url(fonts/proximanova-light-webfont.svg#webfontBtAcCspH) format("svg");
	font-weight: 200;
	font-style: normal;
}
/* bold */
@font-face {
	font-family: 'proxima-nova';
	src: url(fonts/proximanova-bold-webfont.eot);
	src: url(fonts/proximanova-bold-webfont.eot?iefix) format("eot"), url(fonts/proximanova-bold-webfont.woff) format("woff"), url(fonts/proximanova-bold-webfont.ttf) format("truetype"), url(fonts/proximanova-bold-webfont.svg#webfontZ6etP9ZY) format("svg");
	font-weight: bold;
	font-style: normal;
}
/* Xtra bold */
@font-face {
	font-family: 'proxima-nova';
	src: url(fonts/proximanova-xbold-webfont.eot);
	src: url(fonts/proximanova-xbold-webfont.eot?iefix) format("eot"), url(fonts/proximanova-xbold-webfont.woff) format("woff"), url(fonts/proximanova-xbold-webfont.ttf) format("truetype"), url(fonts/proximanova-xbold-webfont.svg#webfont2rpW2ohz) format("svg");
	font-weight: 900;
	font-style: normal;
}
/* regular Italic */
@font-face {
	font-family: 'proxima-nova';
	src: url(fonts/proximanova-regitalic-webfont.eot);
	src: url(fonts/proximanova-regitalic-webfont.eot?#iefix) format("embedded-opentype"), url(fonts/proximanova-regitalic-webfont.woff) format("woff"), url(fonts/proximanova-regitalic-webfont.ttf) format("truetype"), url(fonts/proximanova-regitalic-webfont.svg#proxima_novaregular_italic) format("svg");
	font-weight: normal;
	font-style: italic;
}
/* Condensed regular */
@font-face {
	font-family: 'proxima-nova-condensed';
	src: url(fonts/proximanovacond-regular.eot);
	src: url(fonts/proximanovacond-regular.eot?#iefix) format("embedded-opentype"), url(fonts/proximanovacond-regular.woff) format("woff"), url(fonts/proximanovacond-regular.ttf) format("truetype"), url(fonts/proximanovacond-regular.svg#proximanovacond-regular) format("svg");
	font-weight: normal;
	font-style: normal;
}
/* Condensed bold */
@font-face {
	font-family: 'proxima-nova-condensed';
	src: url(<?php echo base_url();?>fonts/proximanovacond-bold-webfont.eot);
	src: url(<?php echo base_url();?>fonts/proximanovacond-bold-webfont.eot?#iefix) format("embedded-opentype"), url(<?php echo base_url();?>fonts/proximanovacond-bold-webfont.woff) format("woff"), url(<?php echo base_url();?>fonts/proximanovacond-bold-webfont.ttf) format("truetype"), url(<?php echo base_url();?>fonts/proximanovacond-bold-webfont.svg#proximanovaCondensedbold) format("svg");
	font-weight: bold;
	font-style: normal;
	text-transform: uppercase !important;
}
/* Condensed Semibold */
@font-face {
	-family: 'proxima-nova-condensed';
	src: url(<?php echo base_url();?>fonts/proximanovacond-semibold-webfont.eot);
	src: url(<?php echo base_url();?>fonts/proximanovacond-semibold-webfont.eot?#iefix) format("embedded-opentype"), url(<?php echo base_url();?>fonts/proximanovacond-semibold-webfont.woff) format("woff"), url(<?php echo base_url();?>fonts/proximanovacond-semibold-webfont.ttf) format("truetype"), url(<?php echo base_url();?>fonts/proximanovacond-semibold-webfont.svg#proxima_nova_condensedSBd) format("svg");
	font-weight: 600;
	font-style: normal;
}
  </style>
</head>
<body>
<nav class="navbar navbar-inverse top-menu" id="top-menu-id">
  <div class="container-fluid">
    <div class="row">
		<div class="col-md-4">	
		<p class="text-center"><span>Call us 24/7 on </span><a href="tel:+918765432456" class="phone ">+91 8765432456</a></p>
			
		</div>
		<div class="col-md-4 text-center">
			<div class="navbar-header">
			  <a class="navbar-brand logo" href="<?php echo base_url();?>">
					<img src="<?php echo base_url();?>images/logo.png" class="img-responsive">
			  </a>
			</div>
		</div>
		<div class="col-md-4">
			<ul class="nav navbar-nav navbar-left right-links">
				<li class="sign-in"><a href="#">Join the Club</a></li>
				<li class="sign-up"><a href="#">Sign In</a></li>
			</ul>
		</div>
    </div>
   </div>
</nav>
<nav class="navbar navbar-inverse main-menu">
  <div class="container">
	<div class="row">
		<div class="col-md-12" style="padding:0px 0px;">
			<div class="navbar-header">
			  <button type="button" class="navbar-toggle pull-left three-lines" data-toggle="collapse-side" data-target="#myNavbar">
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span> 
			  </button>
			  <button type="button" class="navbar-toggle pull-left" data-toggle="collapse" data-target="#myNavbar2" style="background:#19242f;border:none;">
				<span class="glyphicon glyphicon-search" style="display:none;" id="mobile-search-icon"></span>
			  </button>
			  <a href="<?php echo base_url();?>"><img src="<?php echo base_url();?>images/logo.png" class="img-responsive mobile-logo" style="display:none;"></a>
			</div>
			<div class="collapse navbar-collapse" id="myNavbar">
			  <ul class="nav navbar-nav navbar-left" style="display:none;" id="mobile-menu">
				<li class="sign-in"><a href="#">Join the Club</a></li>
				<li class="sign-up"><a href="#">Sign In</a></li>
			  </ul>
			  <ul class="nav navbar-nav menu-items">
				<li id="hidden-logo"></li>
				<li class="active menu-links"><a href="<?php echo base_url();?>hotels">Hotels</a></li>
				<li class="menu-links"><a href="#">Resorts</a></li>
				<li class="menu-links"><a href="#">Offers</a></li> 
				<li class="menu-links"><a href="#">About</a></li> 
				<li class="menu-links"><a href="#">Testimonials</a></li> 
				<li class="menu-links"><a href="#">Gallery</a></li> 
				<li class="menu-links"><a href="#">CSR</a></li>  
			  </ul>
			</div>
			
			<div class="collapse navbar-collapse" id="myNavbar2">
			  <form action="" style="display:none;" method="post" class="search-form">
				<div class="col-xs-12" style="padding:0px;">
					<div class="input-group" style="margin:10px 0px;">
						<span class="input-group-addon search-icon" style="border-radius:0px;"><i class="glyphicon glyphicon-search" id="inner-icon"></i></span>
						<input type="text" class="form-control" name="mobile-search" id="mobile-search" placeholder="Where do you want to go?" style="height:45px;border-radius:0px;">
					</div>
				</div>
				<div class="col-xs-12" style="padding:0px;">
					<button type="submit" class="btn btn-danger search-btn">Search</button>
				</div>
			 </form>
			</div>
	</div>
	</div>
	</div>
</nav>
<script src="<?php echo base_url();?>js/script.js"></script>