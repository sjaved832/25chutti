<div class="container-fluid">
  <div class="row slider">
	<div id="slideshow">
		<img class="active img-responsive" src="<?php echo base_url();?>images/slider/1.png" alt="Slideshow Image 1" />
		<img src="<?php echo base_url();?>images/slider/2.png"  alt="Slideshow Image 2" class=" img-responsive" />
		<img src="<?php echo base_url();?>images/slider/3.png"  alt="Slideshow Image 3" class=" img-responsive"/>
	</div>
  </div>
</div>
<div class="container">
	<div class="row form-row" id="form-col">
		<form action="" method="post">
			<div class="col-md-11 col-xs-12" style="padding-left:10px;">
				<div class="input-group">
					<span class="input-group-addon search-icon"><i class="glyphicon glyphicon-search"></i></span>
					<input type="text" class="form-control" name="search" id="search" placeholder="Where do you want to go?">
				</div>
			</div>
			<div class="col-md-1 col-xs-12" style="padding-left:0px;">
				<button type="submit" class="btn btn-danger search-btn">Search</button>
			</div>
		</form>
	</div>
</div>
<div class="container">
  <div class="row" style="margin-top:50px;">
    <div class="col-md-4 col-xs-12 mobile-padding">
		<div class="first-row-div">
			<div class="static-div">
				<a href="#">
					<img src="<?php echo base_url();?>images/b3.jpg" class="img-responsive">
				</a>
			</div>
			<div class="c-awardsBobs__container">
				<h3 class="text-center">The travel club for hotel lovers</h3>
				<p class="text-center">Our story</p>
			</div>
		</div>
	</div>
    <div class="col-md-4 col-xs-12 mobile-padding">
		<div class="first-row-div">
			<div class="static-div">
				<a href="#">
					<img src="<?php echo base_url();?>images/c3.jpg" class="img-responsive">
				</a>
			</div>
			<div class="c-awardsBobs__container">
				<h3 class="text-center">Member Benefits</h3>
				<p class="text-center">Why book with 25 Chutti?</p>
			</div>
		</div>
	</div>
    <div class="col-md-4 col-xs-12 mobile-padding">
		<div class="first-row-div">
			<div class="static-div">
				<a href="#">
					<img src="<?php echo base_url();?>images/a1.jpg" class="img-responsive">
				</a>
			</div>
			<div class="c-awardsBobs__container">
				<h3 class="text-center">Travel tales from around the globe</h3>
				<p class="text-center">Word 25chutti</p>
			</div>
		</div>
	</div>
  </div>
</div>

<div class="container"> 
	<h2 class="text-center">25 Chutti Popular Destinations</h2>
    <div class="row offers" id="states-div">
		
		<?php
			//echo count($states);echo "<br>";
			foreach($states as $key=>$value)
			{
				$class = "";
				if($key > 8)
				{
					$class = "hide_state";
				}
		?>
				<div class="col-md-4 col-xs-12 offer-cols <?php echo $class;?>">
					<div class="thumbnail1">
						<a href="<?php echo base_url().'hotels?state='.$value['state_id'];?>">
							<img src="<?php echo base_url().$state_url.'/'.$value['state_logo'];?>" class="img-responsive" width="352" height="230">
							<div class="centered"><h4><?php echo $value['state_name'];?></h4></div>
						</a>
					</div>
				</div>
		<?php
			}
		?>
    </div>
	<div class="row button-row">
    <div class="col-md-4 col-md-offset-4 text-center">
		<button class="btn btn-md btn-danger show_all" id="view_more_state">
			View All
		</button>
		<button class="btn btn-md btn-danger show_all" id="view_less_state" style="display:none;">
			View Less
		</button>
	</div>
  </div>
</div>
<div class="container collections" id="hotels">
	<h2 class="text-center">25 Chutti Packages</h2>
	<p class="generic-content">Torn between an island retreat and a seductive city break? Take inspiration from our collections…</p>
  <div class="row" id="packages-div">
	<?php
		foreach($packages as $key => $value)
		{
			$class = "";
			if($key > 8)
			{
				$class = "hide_state";
			}
	?>
			<div class="col-md-4 col-xs-12 offer-cols <?php echo $class;?>">
				<div class="img-div">
					<a href="<?php echo base_url().'hotels/?package='.$value['package_id'];?>">
						<img src="<?php echo base_url().$package_url.'/'.$value['package_logo'];?>" class="img-responsive">
						<div class="centered"><h4><?php echo $value['package_name'];?></h4></div>
					</a>
				</div>
			</div>
	<?php
		}
	?>
  </div>
  
  <div class="row button-row">
    <div class="col-md-4 col-md-offset-4 text-center">
		<button class="btn btn-md btn-danger show_all" id="view_more_package">
			View All
		</button>
		<button class="btn btn-md btn-danger show_all" id="view_less_package" style="display:none;">
			View Less
		</button>
	</div>
  </div>
</div>

<script type="text/javascript">
	var package_url = "<?php echo $package_url;?>";
	var state_url = "<?php echo $state_url;?>";
	
	/*script for hide and show states*/

	$('#view_more_state').on('click',function()
	{
		$('#states-div').find('.hide_state').css('display','block');
		$('#view_more_state').hide();
		$('#view_less_state').show();
	});
	
	
	$('#view_less_state').on('click',function()
	{
	   $('#states-div').find('.hide_state').css('display','none');
		$('#view_more_state').show();
		$('#view_less_state').hide();
	});

	/*script for hide and show packages*/

	$('#view_more_package').on('click',function()
	{
		$('#packages-div').find('.hide_state').css('display','block');
		$('#view_more_package').hide();
		$('#view_less_package').show();
	});
	
	
	$('#view_less_package').on('click',function()
	{
	   $('#packages-div').find('.hide_state').css('display','none');
		$('#view_more_package').show();
		$('#view_less_package').hide();
	});
</script>
<script src="<?=base_url()?>js/search-autocomplete.js"></script>
