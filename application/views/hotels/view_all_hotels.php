<div class="container-fluid" style="padding:0px;">
	<div class="row form-row" id="form-col2">
		<form action="" method="post">
			<div class="col-md-9 col-md-offset-1 col-xs-12" style="padding-left:60px;">
				<div class="input-group">
					<span class="input-group-addon search-icon"><i class="glyphicon glyphicon-search"></i></span>
					<input type="text" class="form-control" name="search" id="search" placeholder="Where do you want to go?" style="width:98%;">
				</div>
			</div>
			<div class="col-md-1 col-xs-12" style="padding-left:0px;">
				<button type="submit" class="btn btn-danger search-btn" id="listing-search-btn">Search</button>
			</div>
		</form>
	</div>
</div>
<div class="container">
<?php if(!$isHotel){ ?>
	<div class="row">
			<h1 class="page-title">Luxury &amp; boutique hotels</h1>
			<div class="col-md-12 col-xs-12 filters-div">
				<ul id="list-menu" class="filters">
					<li class="selected" onclick="loadHotel(1)" id="hotels-filter"><?=$count_by_type["hotel"]?> Hotels</li>
					<li id="explore-filter" onclick="loadHotel(0)"><?=$count_by_type["resort"]?> Resorts</li>
					<!-- <li id="villas-filter">Explore</li> -->
				</ul>
			</div>
	</div>	 
	 <?php
	}
		if(count($facilities) > 1)
		{
	?>
			<div class="row facilities-row">
				<p class="text-center">Try adding one of these filters:</p>
				<div class="col-md-12 col-xs-12 filters-div">
					<ul>
						<?php
							foreach($facilities as $key => $value)
							{
								if(!empty($value))
								{
						?>
									<li><a href="javascript:goFor('<?php echo $value[0]['facility_id'];?>');"><?php echo $value[0]['facility_name'];?></a></li>
						<?php
								}
							}
						?>
					</ul>
				</div>
			</div>
	<?php
		}
	?>	 
  <?php 
	if(!empty($result))
	{
  ?>
		 <div class="row filters-row">
			<div class="col-md-12 filters-col">
				<select id="sort_type_price" name="sort_type_price">
					<option value="" selected="selected">Sort By</option>
					<option value="price_asc">Price Low</option>
					<option value="price_desc">Price High</option>
				</select>
				<select onchange="update_no_of_records(this.value)" id="sort_type_num" name="sort_type_num">
					<option value="5" selected="selected">Show</option>
					<option value="5">Show 5</option>
					<option value="10">Show 10</option>
					<option value="15">Show 15</option>
					<option value="20">Show 20</option>
				</select>
			</div>
		 </div>
		<div id="hotelList">
  <?php
		$index = 0;
		foreach($result as $row)
		{
   ?>
			<div class="row single">
				<div class="col-md-6 col-sm-6 col-xs-12 imgs-div">
					<div id="myCarousel<?php echo $index?>" class="carousel slide" data-ride="carousel">
						
						<!-- Wrapper for slides -->
						<div class="carousel-inner">
							<?php 
							$i = 0;
							foreach($row['images'] as $inner_row){ ?>
							<div class="item <?php echo $i==0?'active':''?>">
							  <img src="<?php echo base_url().$upload_url."/".$inner_row['image_name'];?>" class="img-responsive" style="width:100%;height:300px;">
							</div>
							<?php $i++; } ?>
						</div>

						<!-- Left and right controls -->
						<a class="left carousel-control" href="#myCarousel<?php echo $index?>" data-slide="prev">
							<span class="glyphicon glyphicon-chevron-left"></span>
							<span class="sr-only">Previous</span>
						</a>
						<a class="right carousel-control" href="#myCarousel<?php echo $index?>" data-slide="next">
							<span class="glyphicon glyphicon-chevron-right"></span>
							<span class="sr-only">Next</span>
						</a>
					</div>
					
				</div>
				<div class="col-md-6 col-sm-6 col-xs-12 text-div">
					<div class="row basic-info">
						<div class="col-md-8">
							<p class="hotel-title-destination">
								<span class="city"> <?php echo $row['city_name']?> </span>, 
								<span class="country"><?php echo $row['state_name']?></span>                    
							</p>
							<a href="#">
								<h2 class="hotel-title-hotelname"><?php echo $row['hotel_name']?></h2>                
							</a>
						</div>
						<div class="col-md-4 text-right">
							<p class="hotel-pricing-label text-right">
								Total price                   
							</p>
							<a href="#">
								<h2 class="hotel-pricing-rate"><i class="fa fa-inr"></i>&nbsp;<?php echo $row['price'];?></h2>                
							</a>
						</div>
					</div>
					<div class="row special-row">
						<div class="col-md-2">
							<p class="style">Style&nbsp;</p>
						</div>
						<div class="col-md-10">
							<p class="hotel-maintext-setting"><?php echo $row['style'];?></p>
						</div>
						<div class="col-md-2">
							<p class="setting">Setting&nbsp;</p>
						</div>
						<div class="col-md-10">
							<p class="hotel-maintext-style"><?php echo $row['setting'];?></p>
						</div>
					</div>
					<div class="row extra-row">
						<div class="col-md-12">
							<p class="hotel-maintext-extralabel"><span>25 Chutti Extra&nbsp;</span>-- 
								<?php 
									if(strlen($row['extras']) > 30)
									{
										echo substr($row['extras'],0,30);
									}
								?>
							</p>  
						</div>       
					</div>
					<div class="row">
						<div class="col-md-6 col-xs-12 quick" data-visible="0">
							<p>Quickview</p>
						</div>
						<div class="col-md-6 col-xs-12 view" onclick="goTo('<?php echo base_url().'hotel-details/'.$row['hotel_id'];?>')">
							View Details
						</div>
					</div>
				</div>
				<div class="row quick-row">
					<div class="col-md-4 col-sm-4 col-xs-12" style="background:#fff;">
						<div class="offers-div" style="display: table-cell;">
							<h3 class="offers-heading">Offers</h3>
							<?php echo $row['offers']?>
						</div>
					</div>
					<div class="col-md-8 col-sm-4 col-xs-12" style="background:#fff;">
						<div class="hotelcard-highlights-secondcol">
							<h3 class="highlights-heading">Highlights</h3>
							<?php echo $row['highlights']?>

							<h3 id="favrooms">Our favourite rooms</h3>
							<p class="favrooms"><?php echo $row['favourite_rooms']?></p>
							<h3 id="location">Location</h3>
							<a href="#" class="location"><?php echo $row['city_name']?>,<?php echo $row['state_name']?></a>
						</div>
					</div>
				</div>
			</div>
  <?php
			$index++;
		}
	?>
	</div>
  <?php
	}
	else
	{
   ?>
			<div class="row">
				<h1 class="page-title">No Hotels</h1>
			</div>
   <?php
	}
   ?>
   <input type="hidden" id="page_no" value="0"/>
	<ul id="pagination_no">
	</ul>
<!--script for view and close more details of hotel-->
</div>
<script type="text/javascript">

	function goTo(url)
	{
		location.href = url;
		//alert(url);
	}

	$('.quick').click(function(){
	     var attribute = $(this).attr("data-visible");
		 if(attribute == 0)
		 {
			$(this).attr("data-visible","1"); 
			$('.quick-row').fadeIn({queue: false, duration: 'slow'});
			$('.quick-row').animate({ top: "-10px" }, 'slow');
			$('.quick-row').fadeIn();
		 }
		 else
		 {
			$('.quick-row').fadeOut();
			$(this).attr("data-visible","0");
		 }
	});
	var upload_url = '<?=$upload_url?>';
	var totalRecords = <?=$count?>;
	var query = '<?=$queryString?>';
	var recordsToDisplay = 2; //
</script>
<script type="text/javascript">
	function goFor(facility_id)
	{
		alert(facility_id);
	}
</script>
<script src="<?php echo base_url()?>js/search-autocomplete.js"></script>
<script src="<?php echo base_url()?>js/pagination.js"></script>