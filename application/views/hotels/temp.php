<div class="row single">
	<div class="col-md-6 col-sm-6 col-xs-12 imgs-div">
		<div id="myCarousel<?php echo $index?>" class="carousel slide" data-ride="carousel">
			<!-- Indicators -->
			<!-- <ol class="carousel-indicators">

							<?php 
							/*$i = 0;
							 foreach($row['images'] as $inner_row){ ?>
							<li data-target="#myCarousel<?php echo $index?>" data-slide-to="<?php echo $i?>" class="<?php echo $i==0?'active':''?> "></li>
							 <?php $i++; }*/ ?>
						</ol> -->

			<!-- Wrapper for slides -->
			<div class="carousel-inner">
				<?php 
							$i = 0;
							foreach($row['images'] as $inner_row){ ?>
				<div class="item <?php echo $i==0?'active':''?>">
					<img src="<?php echo base_url().$upload_url." / ".$inner_row['image_name'];?>" class="img-responsive" style="width:100%;height:300px;">
				</div>
				<?php $i++; } ?>
			</div>

			<!-- Left and right controls -->
			<a class="left carousel-control" href="#myCarousel<?php echo $index?>" data-slide="prev">
				<span class="glyphicon glyphicon-chevron-left"></span>
				<span class="sr-only">Previous</span>
			</a>
			<a class="right carousel-control" href="#myCarousel<?php echo $index?>" data-slide="next">
				<span class="glyphicon glyphicon-chevron-right"></span>
				<span class="sr-only">Next</span>
			</a>
		</div>
		<a href="#">

			<!--div class="patch">
						25 Chutti Runnership 2018
					 </div-->
		</a>
	</div>
	<div class="col-md-6 col-sm-6 col-xs-12 text-div">
		<div class="row basic-info">
			<div class="col-md-8">
				<p class="hotel-title-destination">
					<span class="city">
						<?php echo $row['city_name']?> </span>,
					<span class="country">
						<?php echo $row['state_name']?>
					</span>
				</p>
				<a href="#">
					<h2 class="hotel-title-hotelname">
						<?php echo $row['hotel_name']?>
					</h2>
				</a>
			</div>
			<div class="col-md-4 text-right">
				<p class="hotel-pricing-label text-right">
					Total price
				</p>
				<a href="#">
					<h2 class="hotel-pricing-rate">
						<i class="fa fa-inr"></i>&nbsp;
						<?php echo $row['price'];?>
					</h2>
				</a>
			</div>
		</div>
		<div class="row special-row">
			<div class="col-md-2">
				<p class="style">Style&nbsp;</p>
			</div>
			<div class="col-md-10">
				<p class="hotel-maintext-setting">
					<?php echo $row['style'];?>
				</p>
			</div>
			<div class="col-md-2">
				<p class="setting">Setting&nbsp;</p>
			</div>
			<div class="col-md-10">
				<p class="hotel-maintext-style">
					<?php echo $row['setting'];?>
				</p>
			</div>
		</div>
		<div class="row extra-row">
			<div class="col-md-12">
				<p class="hotel-maintext-extralabel">
					<span>25 Chutti Extra&nbsp;</span>--
					<?php 
									if(strlen($row['extras']) > 30)
									{
										echo substr($row['extras'],0,30);
									}
								?>
				</p>
			</div>
		</div>
		<div class="row">
			<div class="col-md-6 col-xs-12 quick" data-visible="0">
				<p>Quickview</p>
			</div>
			<div class="col-md-6 col-xs-12 view" onclick="goTo('<?php echo base_url().'hotel-details/'.$row['hotel_id'];?>')">
				View Details
			</div>
		</div>
	</div>
	<div class="row quick-row">
		<div class="col-md-4 col-sm-4 col-xs-12" style="background:#fff;">
			<div class="offers-div" style="display: table-cell;">
				<h3 class="offers-heading">Offers</h3>
				<?php echo $row['offers']?>
				<!-- <ul class="offers">
								<li>Half board - save 25% offer</li>
								<li>Advance Purchase with Breakfast - 10% Off</li>
								<li>Family Package</li>
								<li>Advance Purchase with Breakfast - 20% Off</li>
								<li>Advance Purchase with Breakfast - 15% Off</li>
								<li>Smith Exclusive: Family Villas offer: massage on arrival, in-villa treats, reserved private beach cabana, and more</li>
							</ul> -->
			</div>
		</div>
		<div class="col-md-8 col-sm-4 col-xs-12" style="background:#fff;">
			<div class="hotelcard-highlights-secondcol">
				<h3 class="highlights-heading">Highlights</h3>
				<?php echo $row['highlights']?>

				<!-- <p class="overview">-A private corner of Sicily's southern coast, Verdura Resort is a luxury hotel lovingly created by Sir Rocco Forte for the most discerning travellers.</p> -->
				<h3 id="favrooms">Our favourite rooms</h3>
				<p class="favrooms">
					<?php echo $row['favourite_rooms']?>
				</p>
				<h3 id="location">Location</h3>
				<a href="#" class="location">
					<?php echo $row['city_name']?>,
					<?php echo $row['state_name']?>
				</a>
			</div>
		</div>
	</div>
</div>
