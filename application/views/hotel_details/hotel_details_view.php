<div class="container-fluid " style="padding:0px;">
	<div class="row form-row" id="form-col2">
		<form action="" method="post">
			<div class="col-md-9 col-md-offset-1 col-xs-12" style="padding-left:60px;">
				<div class="input-group">
					<span class="input-group-addon search-icon"><i class="glyphicon glyphicon-search"></i></span>
					<input type="text" class="form-control" name="search" id="search" placeholder="Where do you want to go?">
				</div>
			</div>
			<div class="col-md-1 col-xs-12" style="padding-left:0px;">
				<button type="submit" class="btn btn-danger search-btn">Search</button>
			</div>
		</form>
	</div>
</div>
<div class="container-fluid" style="padding:0px;">
	<div class="row first-row">
		<img src="<?php echo base_url().$upload_url.'/'.$hotel_images_list[0]['image_name'];?>" class="img-responsive" style="width:100%;">
	
		  <span class="name_banner"><?php echo $hotel_details['hotel_name'];?></span>
		<span class="price-banner">
			<i class="fa fa-inr"></i> <?php echo $hotel_details['price'];?>
		</span>
	</div>
</div>
<nav class="navbar navbar-inverse one-pager">
  <div class="container">
	<div class="row" style="margin:0px;">
		<div class="col-md-8 col-md-offset-2 col-xs-12">
			<ul class="nav navbar-nav" id="myNavbar3">
			  <li class="active"><a href="#overview" id="overview-link">Overview</a></li>
			  <li class=""><a href="#photos" id="photos-link">Photos</a></li>
			  <li class=""><a href="#location" id="location-link">Location</a></li> 
			  <li class=""><a href="#facilities" id="facilities-link">Facilities</a></li> 
			</ul>
		</div>
	</div>
  </div>
</nav>

<!--nav class="navbar navbar-inverse one-pager">
  <div class="container">
	<div class="row" style="margin:0px;">
		<div class="col-md-8 col-md-offset-2 col-xs-12">
			<div class="navbar-header">
			  <!--<button type="button" class="navbar-toggle pull-left three-lines" data-toggle="collapse-side" data-target="#myNavbar3">
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span> 
			  </button>-->
			<!--/div>
			<div class="collapse navbar-collapse" id="myNavbar3">
			  <ul class="nav navbar-nav">
				<li class="active"><a href="#overview" id="overview-link">Overview</a></li>
				<li class=""><a href="#photos" id="photos-link">Photos</a></li>
				<li class=""><a href="#location" id="location-link">Location</a></li> 
				<li class=""><a href="#facilities" id="facilities-link">Facilities</a></li> 
			  </ul>
			</div>
		</div>
	</div>
   </div>
</nav-->
<div class="container-fluid" id="overview">
	<div class="row" style="margin:0px;">
		<div class="col-md-2 col-md-offset-2">
			<p class="style">Style</p>
			<p class="setting">Setting</p>
		</div>
		<div class="col-md-6">
			<p class="hotel-maintext-setting"><?php echo $hotel_details['style'];?></p>
			<p><?php echo $hotel_details['setting'];?></p>
		</div>
		<div class="col-md-8 col-md-offset-2 highlights-div">
				<?php echo $hotel_details['highlights'];?>
		</div>
		<div class="col-md-8 col-md-offset-2">
			<div id="extra-text">
						<h2 class="extra-heading text-center">25 Chutti EXTRA</h2>
						<p class="text-center"><?php echo $hotel_details['extras'];?></p>
			</div>
		</div>
	</div>
</div>
<div class="container-fluid" id="photos" style="padding:0px;">
	<div class="row" style="margin:0px;">
		<div class="col-md-12 col-xs-12" style="padding:0px;">
			<img src="<?php echo base_url().$upload_url.'/'.$hotel_images_list[1]['image_name'];?>" class="img-responsive" style="width:100%;">
		</div>
	</div>
</div>
<div class="container-fluid" id="facilities" style="padding:0px;">
	<div class="row" style="margin:0px;">
		<div class="col-md-8 col-md-offset-2 col-xs-12">
			<div>
				<h3>Facilities</h3>
				<p><?php echo $hotel_details['offers'];?>
				</p>
			</div>
		</div>
	</div>
</div>

<div class="container-fluid" id="location" style="padding:0px;">
	<div class="row" style="margin:0px;">
		<div class="col-md-12 col-xs-12" style="padding:0px;">
				<img src="<?php echo base_url().$upload_url.'/'.$hotel_images_list[2]['image_name'];?>" class="img-responsive" style="width:100%;">
		</div>
		<div class="col-md-8 col-md-offset-2 col-xs-12">
			<div>
					<h3>Location</h3>
					<p>Address: <?php echo $hotel_details['city_name'].', '.$hotel_details['state_name'];?></p>
			</div>
		</div>
	</div>
</div>


<script src="<?=base_url()?>js/search-autocomplete.js"></script>
<script type="text/javascript">
	$("#overview-link").on('click', function(event) 
	{
		$('#myNavbar3 li').removeClass('active');
		$(this).parent().addClass('active');
		
		
		if (this.hash !== "") {
		  event.preventDefault();

		  // Store hash
		  var hash = this.hash;
		  $('html, body').animate({
			scrollTop: $(hash).offset().top
		  }, 1000, function(){
	   
			window.location.hash = hash;
		  });
		} 
  });
  
  $("#gallery-link").on('click', function(event) 
	{
		$('#myNavbar3 li').removeClass('active');
		$(this).parent().addClass('active');
		
		if (this.hash !== "") {
		  event.preventDefault();

		  // Store hash
		  var hash = this.hash;
		  $('html, body').animate({
			scrollTop: $(hash).offset().top
		  }, 1000, function(){
	   
			window.location.hash = hash;
		  });
		} 
  });
  
  $("#location-link").on('click', function(event) 
	{
		$('#myNavbar3 li').removeClass('active');
		$(this).parent().addClass('active');
		
		if (this.hash !== "") {
		  event.preventDefault();

		  // Store hash
		  var hash = this.hash;
		  $('html, body').animate({
			scrollTop: $(hash).offset().top
		  }, 1000, function(){
	   
			window.location.hash = hash;
		  });
		} 
  });
  
  
  
  $("#facilities-link").on('click', function(event) 
	{
		$('#myNavbar3 li').removeClass('active');
		$(this).parent().addClass('active');
		
		if (this.hash !== "") {
		  event.preventDefault();
		  
		  var hash = this.hash;
		  $('html, body').animate({
			scrollTop: $(hash).offset().top
		  }, 1000, function(){
	   
			window.location.hash = hash;
		  });
		} 
  });
  
  $("#photos-link").on('click', function(event) 
	{
		$('#myNavbar3 li').removeClass('active');
		$(this).parent().addClass('active');
		
		if (this.hash !== "") {
		  event.preventDefault();
		  
		  var hash = this.hash;
		  $('html, body').animate({
			scrollTop: $(hash).offset().top
		  }, 1000, function(){
	   
			window.location.hash = hash;
		  });
		} 
  });
	
</script>



