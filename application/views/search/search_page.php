
        <!-- Service Area Start Here -->
        <style>
        .card-gap{
            padding-top: 3px;
        }
        </style>
		<!-- Search Area Start Here -->
        <section class="search-layout1 bg-body full-width-border-bottom fixed-menu-mt">
            <div class="container">
                <form id="cp-search-form" action="<?php echo base_url(); ?>search" method="get">
                    <div class="row">
						<div class="col-lg-2 col-md-3 col-sm-6 col-xs-12">
                            <div class="form-group search-input-area input-icon-category">
                                <select id="categories" name="category_id" class="select2">
                                    <option class="first" value="">Select Categories</option>
                                </select>
                            </div>
                        </div>
						<div class="col-lg-2 col-md-3 col-sm-6 col-xs-12">
                            <div class="form-group search-input-area input-icon-category">
                                <select id="state" name="state_id" class="select2" onchange="get_city_by_state_id(this.value)">
                                    <option class="first" value="">Select State</option>
                                </select>
                            </div>
                        </div>

						<div class="col-lg-2 col-md-3 col-sm-6 col-xs-12">
                            <div class="form-group search-input-area input-icon-category">
                                <select id="city" class="select2" name="city_id" onchange="get_area_by_city_id(this.value)">
                                    <option class="first" value="">Select City</option>
                                </select>
                            </div>
                        </div>

                        <div class="col-lg-2 col-md-3 col-sm-6 col-xs-12">
                            <div class="form-group search-input-area input-icon-location">
                                <select id="area" class="select2" name="area_id">
                                    <option class="first" value="">Select Location</option>
                                </select>
                            </div>
                        </div>

                        <div class="col-lg-2 col-md-3 col-sm-4 col-xs-4 col-mb-12 text-right text-left-mb">
                            <button type="submit" class="cp-search-btn">
                                <i class="fa fa-search" aria-hidden="true"></i>Search
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </section>
        <!-- Search Area End Here -->
        
        <section class="service-layout1 bg-accent s-space-custom2 search-result">
            <div class="container">
                <div class="section-title-dark">
                    <h1>Welcome To Store Listing page</h1>
                </div>
                <!--<dir class="row">
                    <div class="col-lg-12">-->
                        <div class="row" id="categoryList">
                        	<?php
                        	foreach ($store_list as $store_obj) {
                        	?>
                            <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 col-mb-12 item-mb wow fadeIn stores" data-wow-duration="1.5s" style="cursor: pointer;" onclick="goTo(<?php echo $store_obj['store_id'];?>)" data-wow-delay="0.2s">
                                <div class="row service-box1 bg-body text-center align-items-center" style="margin-right:0px;margin-left:  0px;">
                                    <div class="col-lg-6">
                                    <img src="<?php echo base_url().$upload_url.'/'.$store_obj['store_logo']; ?>" alt="service" class="img-responsive" width="100%" ></div>
                                    <div class="col-lg-6" >
                                        <h3 class="title-medium-dark">
                                            <a href="<?php base_url(); ?>../store-details/<?php echo $store_obj['store_id']; ?>"><?php echo $store_obj['store_name']; ?></a>
                                        </h3>
                                        <section class="card-gap"><?php echo $store_obj['area_name'].', '.$store_obj['city_name'];?></section>

                                        <section class="'card-gap"><?php echo $store_obj['tag_line']; ?></section>
                                        <a class="btn btn-default my-3" href="#" onclick="goTo(<?php echo $store_obj['store_id'];?>)" style="background-color:#119d7f;color:#fff;">Read more</a>
                                    </div>
                                    <section style="color:#5c6935;position: absolute;top: 0px;right: 14px;background:#68dec2; padding: 3px 5px;border-bottom-left-radius: 26%;" class="reviews">
                                        <div><?php echo $store_obj['num_of_reviews'];?></div>
                                        <div>Reviews</div>
                                    </section>
                                </div>
                            </div>
                            <?php
                        	}
                            ?>
                        </div>
            </div>

        </section>
        <!-- Service Area End Here -->
        <section class="overlay-default s-space-equal overflow-hidden" style="background-image: url('<?php echo base_url();?>img/banner/counter-back1.jpg');">
            <div class="container">
                <div class="row">
                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 col-mb-12 wow bounceInUp" data-wow-duration="1.5s" data-wow-delay=".3s">
                        <div class="d-md-flex justify-md-content-center counter-box text-center--md">
                            <div>
                                <img src="<?php echo base_url();?>img/banner/counter1.png" alt="counter" class="img-responsive mb20-auto--md">
                            </div>
                            <div>
                                <div class="counter counter-title" data-num="100000">1,00,000</div>
                                <h3 class="title-regular-light">Our Products</h3>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 col-mb-12 wow bounceInUp" data-wow-duration="1.5s" data-wow-delay=".5s">
                        <div class="d-md-flex justify-md-content-center counter-box text-center--md">
                            <div>
                                <img src="<?php echo base_url();?>img/banner/counter2.png" alt="counter" class="img-responsive mb20-auto--md">
                            </div>
                            <div>
                                <div class="counter counter-title" data-num="500000">5,00,000</div>
                                <h3 class="title-regular-light">Our Happy Buyers</h3>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 col-mb-12 wow bounceInUp" data-wow-duration="1.5s" data-wow-delay=".7s">
                        <div class="d-md-flex justify-md-content-center counter-box text-center--md">
                            <div>
                                <img src="<?php echo base_url();?>img/banner/counter3.png" alt="counter" class="img-responsive mb20-auto--md">
                            </div>
                            <div>
                                <div class="counter counter-title" data-num="200000">2,00,000</div>
                                <h3 class="title-regular-light">Verified Users</h3>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- Counter Area End Here -->
        <!-- Pricing Plan Area Start Here -->


		<!-- jquery-->
        <script src="<?php echo base_url();?>js/jquery-2.2.4.min.js" type="text/javascript"></script>
        <!-- Plugins js -->
        <script src="<?php echo base_url();?>js/plugins.js" type="text/javascript"></script>
        <!-- Bootstrap js -->
        <script src="<?php echo base_url();?>js/bootstrap.min.js" type="text/javascript"></script>
        <!-- WOW JS -->
        <script src="<?php echo base_url();?>js/wow.min.js"></script>
        <!-- Owl Cauosel JS -->
        <script src="<?php echo base_url();?>OwlCarousel/owl.carousel.min.js" type="text/javascript"></script>
        <!-- Meanmenu Js -->
        <script src="<?php echo base_url();?>js/jquery.meanmenu.min.js" type="text/javascript"></script>
        <!-- Srollup js -->
        <script src="<?php echo base_url();?>js/jquery.scrollUp.min.js" type="text/javascript"></script>
        <!-- jquery.counterup js -->
        <script src="<?php echo base_url();?>js/jquery.counterup.min.js"></script>
        <script src="<?php echo base_url();?>js/waypoints.min.js"></script>
        <!-- Select2 Js -->
        <script src="<?php echo base_url();?>js/select2.min.js" type="text/javascript"></script>
        <!-- Isotope js -->
        <script src="<?php echo base_url();?>js/isotope.pkgd.min.js" type="text/javascript"></script>
        <!-- Magnific Popup -->
        <script src="<?php echo base_url();?>js/jquery.magnific-popup.min.js"></script>
        <!-- Custom Js -->
        <script src="<?php echo base_url();?>js/main.js" type="text/javascript"></script>
		<script src="<?php echo base_url();?>js/modernizr-2.8.3.min.js"></script>
        <script>
        var upload_url = '<?php echo $upload_url; ?>';
      function goTo(id)
      	  {
              if (id) {
            		 url = base_url+'store-details/'+id;
            		 location.href = url;
               }else {
                 location.href = base_url;
               }
      	  }

        </script>
