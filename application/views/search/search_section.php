
        <!-- Service Area Start Here -->
        <style>
        .card-gap{
            padding-top: 3px;
        }
        </style>
		<!-- Search Area Start Here -->
        <section class="search-layout1 bg-body full-width-border-bottom fixed-menu-mt">
            <div class="container">
                <form id="cp-search-form" action="<?php echo base_url(); ?>search" method="get">
                    <div class="row">
						<div class="col-lg-2 col-md-3 col-sm-6 col-xs-12">
                            <div class="form-group search-input-area input-icon-category">
                                <select id="categories" name="category_id" class="select2">
                                    <option class="first" value="">Select Categories</option>
                                </select>
                            </div>
                        </div>
						<div class="col-lg-2 col-md-3 col-sm-6 col-xs-12">
                            <div class="form-group search-input-area input-icon-category">
                                <select id="state" name="state_id" class="select2" onchange="get_city_by_state_id(this.value)">
                                    <option class="first" value="">Select State</option>
                                </select>
                            </div>
                        </div>

						<div class="col-lg-2 col-md-3 col-sm-6 col-xs-12">
                            <div class="form-group search-input-area input-icon-category">
                                <select id="city" class="select2" name="city_id" onchange="get_area_by_city_id(this.value)">
                                    <option class="first" value="">Select City</option>
                                </select>
                            </div>
                        </div>

                        <div class="col-lg-2 col-md-3 col-sm-6 col-xs-12">
                            <div class="form-group search-input-area input-icon-location">
                                <select id="area" class="select2" name="area_id">
                                    <option class="first" value="">Select Location</option>
                                </select>
                            </div>
                        </div>

                        <div class="col-lg-2 col-md-3 col-sm-4 col-xs-4 col-mb-12 text-right text-left-mb">
                            <button type="submit" class="cp-search-btn">
                                <i class="fa fa-search" aria-hidden="true"></i>Search
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </section>