<!DOCTYPE html>
<html lang="en">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<!-- Meta, title, CSS, favicons, etc. -->
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=devNataraj-width, initial-scale=1">

		<title>25 Chutti! </title>

		<!-- Bootstrap core CSS -->

		  <link href="<?php echo base_url();?>css/bootstrap.min.css" rel="stylesheet">

		  <link href="<?php echo base_url();?>css/font-awesome.min.css" rel="stylesheet">
		  <link href="<?php echo base_url();?>css/animate.min.css" rel="stylesheet">

		  <!-- Custom styling plus plugins -->
		  <link href="<?php echo base_url();?>css/admin/custom.css" rel="stylesheet">
		  <link href="<?php echo base_url();?>css/admin/icheck/flat/green.css" rel="stylesheet">
          <link href="<?php echo base_url();?>css/style.css" rel="stylesheet">


		<script src="<?php echo base_url();?>js/jquery-3.3.1.min.js"></script>
		<script src="<?php echo base_url();?>js/bootstrap.min.js"></script>
		
		<style type="text/css">
			.login-page{background:#d2d6de;}
			.login-box
			{
				background: #fff;
				padding: 20px;
				border-top: 0;
				color: #666;
				box-shadow: 5px 5px 20px 0px;
			}
			.pwd_forgot{
			display:none;
			}
			.hidden-class{
			display: none;
			}
			.forgot_pwd{
			color:#F79F00;
			}
			.forgot_pwd:hover,
			.forgot_pwd:focus,
			.forgot_pwd:active {
			color:#F79F00;
			}
			em.invalid{
			color:red;
			}
			.scc{font-family: initial;
			font-size: 18px;
			color: green;}
			.state-error{
				text-align: left !important;
			}
			.error{color:#ff0000;}
			.login-form .sub
			{
				width: 160px;
                height: 49px;
				color:#ffffff;
				padding: 15px 0;
                background:#730315;
                text-transform: uppercase;
                font-size: 14px;
                font-weight: 700;
                display: inline-block;
                border: none;
                text-align: center;
                -webkit-transition: all 0.5s ease-out;
                -moz-transition: all 0.5s ease-out;
                -ms-transition: all 0.5s ease-out;
                -o-transition: all 0.5s ease-out;
                transition: all 0.5s ease-out;
			}
			.login-form .sub:hover
			{
				background: #730315;
                color: #ffffff;
			}
			.mb-none{font-size: 30px;color: #730315;}
		</style>
		<!--<script type="text/javascript">
			$(window).on('load',function(){
				$('#myModal').modal('show');
			});
		</script>-->
	</head>

	<body class="login-page">
	
		
	</body>
	<!--<script>

		$(document).ready(function(){
			$('.pwd_forgot').hide();

			$('.forgot_pwd').click(function(){
				$('.login-box').hide();
				$('.pwd_forgot').show();
			});

			$('#back').click(function(){
				$("#useremail").rules("remove", "required");
				/*$("#useremail").rules("remove", "email");
				$("#useremail").rules("remove", "remote");*/
				$("#useremail").val('');
				$('.login-box').show();
				$('.pwd_forgot').hide();
			});

		});
	</script>-->

    <script type ="text/javascript">
		window.history.forward();
		function noBack() { window.history.forward(); }
	</script>
    	<!-- Modal Start-->
    <div class="modal fade" id="myModal" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <div class="title-default-bold mb-none">Login</div>
                </div>
                <div class="modal-body">
                    <div class="login-form">
					    <div id="infoMessage"><?php echo $message;?></div>
						<!--<div>
							<h1><?php// echo lang('login_heading');?></h1>
							<p><?php //echo lang('login_subheading');?></p>
						</div>-->
					    <?php echo form_open("auth/login");?>
						    <?php echo lang('login_identity_label', 'identity');?>
								
                            
							<?php echo form_input($identity, '','class="form-control"');?>
                           
							<?php echo lang('login_password_label', 'password');?>
                            <?php echo form_input($password,'', 'class="form-control"');?>
                            
                            <div class="checkbox checkbox-primary">
							    <?php echo form_checkbox('remember', '1', FALSE, 'id="remember"');?>
                                <?php echo lang('login_remember_label', 'remember');?>

                            </div>
							<?php echo form_submit('submit', lang('login_submit_btn'), 'class="default-big-btn sub"');?>
								
                            <!--<button class="default-big-btn form-cancel" type="submit" value="">Cancel</button>-->
                            
							<label class="lost-password">
							<a href="forgot_password"><?php echo lang('login_forgot_password');?></a>
							</label>
							
                        <?php echo form_close();?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Modal End-->
    <script type="text/javascript">
	     $(document).ready(function(){
			 $('#myModal').modal('toggle');
		 });
	</script>
</html>
