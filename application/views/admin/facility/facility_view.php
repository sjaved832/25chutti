<style>
    .error {color: #FF0000;}
    .no_padding{
		padding-left: 0px;
    }
    .custom_padding{
		padding-right: 0px;
		padding-left: 10px;
    }

    fieldset.scheduler-border {
		border: 1px groove #ddd !important;
		padding: 0 1.4em 1.4em 1.4em !important;
		margin: 0 0 1.5em 0 !important;
		-webkit-box-shadow:  0px 0px 0px 0px #000;
		box-shadow:  0px 0px 0px 0px #000;
	}

	.control-label{
		text-align: left !important;
	}
	.modal{
		height: 500px;
		overflow-y: auto;
	}
	.animate {
		-webkit-animation: animate_bg 5s;
		animation: animate_bg 5s;
		-webkit-animation-iteration-count: infinite;
		animation-iteration-count: infinite;
	}

	@keyframes animate_bg {
		0%   {color:red;}
		100% {color:blue;}
	}

	@keyframes animate_bg {
		0%   {color:red;}
		100% {color:blue;}
	}

	@-webkit-keyframes animate_bg {
		0%   {color:red;}
		100% {color:blue;}
	}
</style>

<!-- page content -->
<div class="right_col" role="main">
	<div class="">
		<div class="page-title">
            <div class="title_left">
				<h3>
                    facility Master
                </h3>
			</div>
			<div class="title_right" style="float: right !important;">
				<div class="col-md-2 col-sm-2 col-xs-12 form-group pull-right top_search" style="float: right !important;">
					<div class="input-group">
						<span class="buttons">
							<button class="btn btn-info btn-sm" type="button" id="add_new_facility_btn" style="padding: none !important;">Add facility</button>
						</span>
					</div>
				</div>
			</div>
		</div>
		<div class="clearfix"></div>

		<div class="row">

			<div class="col-md-12 col-sm-12 col-xs-12">
				<div class="x_panel">
					<div class="x_title">
						<h2>facility</h2>

						<div class="clearfix"></div>
						<div class="alert alert-danger alert-dismissable" id="errormsgDiv" style="display:none">
							<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
							<h4><i class="icon fa fa-ban"></i> Error!</h4>
							<h2 id="errormsg"></h2>
						</div>

						<div class="alert alert-success alert-dismissable" id="succmsgDiv" style="display:none">
							<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
							<h4>  <i class="icon fa fa-check"></i> Success!</h4>
							<h6 id="succmsg"></h6>
						</div>
					</div>
					<div class="x_content">
						<table id="datatable-buttons" class="table table-striped table-bordered" style="width:100%;">
							<thead>
								<tr>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
									<th></th>
                                </tr>
							</thead>
						</table>
					</div>
				</div>
			</div>

		</div>
	</div>
	<div class="row" id="facility_div" style="display:none;">
		<div class="col-md-12 col-sm-12 col-xs-12">
			<div class="x_panel">
                <div class="x_title">
					<h2>facility</h2>
					<div class="clearfix"></div>
				</div>
                <div class="x_content">
					<br />
					<form name="facility_form" id="facility_form" action="<?php echo base_url(); ?>admin/facility/save_facility" method="post" enctype="multipart/form-data" class="form-horizontal form-label-left">
						<input type="hidden" name="id" id="id" value="0" />

						<div class="form-group">
							<label class="control-label col-md-2 col-sm-2 col-xs-12" for="facility_name">Facility Name<span class="required">*</span>
							</label>
							<div class="col-md-5 col-sm-9 col-xs-12">
								<input type="text" id="facility_name" name="facility_name" class="form-control col-md-7 col-xs-12" placeholder="Enter Facility Name">
							</div>
						</div>

						<div class="form-group">
							<label class="control-label col-md-2 col-sm-2 col-xs-12">Active Status<span class="required">*</span></label>
							<div class="col-md-9 col-sm-9 col-xs-12">
								<input type="radio" class="flat" name="status" value="1" checked="" required />&nbsp; Active&nbsp;&nbsp;
								<input type="radio" class="flat" name="status" value="0"/>&nbsp;&nbsp;Inactive
							</div>
						</div>
						<div class="ln_solid"></div>
						<div class="form-group">
							<div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
								<button type="reset" class="btn btn-primary" id="cancel">Cancel</button>
								<button type="submit" id="facilityubmit" class="btn btn-success">Submit</button>
							</div>
						</div>

					</form>
				</div>
			</div>
		</div>
	</div>

<script src="<?php echo base_url();?>js/bootstrap.min.js"></script>
<!-- bootstrap progress js -->
<script src="<?php echo base_url();?>js/admin/progressbar/bootstrap-progressbar.min.js"></script>
<script src="<?php echo base_url();?>js/admin/nicescroll/jquery.nicescroll.min.js"></script>
<!-- icheck -->
<script src="<?php echo base_url();?>js/admin/icheck/icheck.min.js"></script>

<!-- tags -->
<script src="<?php echo base_url();?>js/admin/tags/jquery.tagsinput.min.js"></script>
<script src="<?php echo base_url();?>js/admin/custom.js"></script>


<!-- Datatables -->
<!-- <script src="js/datatables/js/jquery.dataTables.js"></script>
<script src="js/datatables/tools/js/dataTables.tableTools.js"></script> -->

<!-- Datatables-->
<script src="<?php echo base_url();?>js/admin/datatables/jquery.dataTables.min.js"></script>
<script src="<?php echo base_url();?>js/admin/datatables/dataTables.bootstrap.js"></script>
<script src="<?php echo base_url();?>js/admin/datatables/dataTables.buttons.min.js"></script>
<script src="<?php echo base_url();?>js/admin/datatables/buttons.bootstrap.min.js"></script>
<script src="<?php echo base_url();?>js/admin/datatables/jszip.min.js"></script>
<script src="<?php echo base_url();?>js/admin/datatables/pdfmake.min.js"></script>
<script src="<?php echo base_url();?>js/admin/datatables/vfs_fonts.js"></script>
<script src="<?php echo base_url();?>js/admin/datatables/buttons.html5.min.js"></script>
<script src="<?php echo base_url();?>js/admin/datatables/buttons.print.min.js"></script>
<script src="<?php echo base_url();?>js/admin/datatables/dataTables.fixedHeader.min.js"></script>
<script src="<?php echo base_url();?>js/admin/datatables/dataTables.keyTable.min.js"></script>
<script src="<?php echo base_url();?>js/admin/datatables/dataTables.responsive.min.js"></script>
<script src="<?php echo base_url();?>js/admin/datatables/responsive.bootstrap.min.js"></script>
<script src="<?php echo base_url();?>js/admin/datatables/dataTables.scroller.min.js"></script>
<script src="<?php echo base_url();?>js/admin/jquery-validate/jquery.validate.min.js" type="text/javascript"></script>

<link type="text/css" href="<?php echo base_url();?>public/plugins/ckeditor/toolbarconfigurator/lib/codemirror/neo.css" />

<!-- daterangepicker -->
<script type="text/javascript" src="<?php echo base_url();?>js/admin/moment/moment.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>js/admin/datepicker/daterangepicker.js"></script>

<!-- pace -->
<script src="<?php echo base_url();?>js/admin/pace/pace.min.js"></script>

<script src="<?php echo base_url();?>public/js/facility.js"></script>
<script>
	TableManageButtons.init();
</script>
