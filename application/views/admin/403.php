<?php echo $this->load->view('/common/header'); ?>
<style>
.error {color: #FF0000;}
</style>
  <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
       
        <!-- Main content -->
        <section class="content">
        <?php  if($this->session->flashdata('after_login') != 1)  {  ?>
          <div class="error-page">
            <h2 class="headline text-yellow"> 403</h2>
            <div class="error-content">
              <h3><i class="fa fa-warning text-yellow"></i> Unautherized Access.</h3>
              <p>
              
                <h3>You are not authorized user to access this page. </h3>
              </p>
             
            </div><!-- /.error-content -->
          </div><!-- /.error-page -->

          <?php } ?>
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->
  <?php echo $this->load->view('/common/footer'); ?>    
  

  
 