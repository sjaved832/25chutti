<style>
    .error {color: #FF0000;}
    .no_padding{
		padding-left: 0px;
    }
    .custom_padding{
		padding-right: 0px;
		padding-left: 10px;
    }
	
    fieldset.scheduler-border {
		border: 1px groove #ddd !important;
		padding: 0 1.4em 1.4em 1.4em !important;
		margin: 0 0 1.5em 0 !important;
		-webkit-box-shadow:  0px 0px 0px 0px #000;
		box-shadow:  0px 0px 0px 0px #000;
	}
	
	.control-label{
		text-align: left !important;
	}
	.modal{
		height: 500px;
		overflow-y: auto;
	}
	.animate {
		-webkit-animation: animate_bg 5s;
		animation: animate_bg 5s;
		-webkit-animation-iteration-count: infinite;
		animation-iteration-count: infinite;
	}

	@keyframes animate_bg {
		0%   {color:red;}
		100% {color:blue;}
	}

	@keyframes animate_bg {
		0%   {color:red;}
		100% {color:blue;}
	}

	@-webkit-keyframes animate_bg {
		0%   {color:red;}
		100% {color:blue;}
	}
	.remove_img_preview {
		position:relative;
		top:5px;
		right:70px;
		background:white;
		
		border-radius:150px;
		font-size:2em;
		padding: 0.3em 0.6em;
		text-align:center;
		cursor:pointer;
		opacity: 0.5;
		
	}
	.remove_img_preview:before {
		content: "×";
		font-weight: bold;
		color:black;
	}
</style>

<!-- page content -->
<div class="right_col" role="main">
	<div class="">
		<div class="page-title">
            <div class="title_left">
				<h3>
                    Contact Queries
                </h3>
			</div>
		</div>
		<div class="clearfix"></div>
		
		<div class="row">
			<div class="col-md-12 col-sm-12 col-xs-12">
				<div class="x_panel">
					<div class="x_title">
						<h2>Contact Queries</h2>
						
						<div class="clearfix"></div>
						<div class="alert alert-danger alert-dismissable" id="errormsgDiv" style="display:none">
							<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
							<h4><i class="banner fa fa-ban"></i> Error!</h4>
							<h6 id="errormsg"></h6>
						</div>

						<div class="alert alert-success alert-dismissable" id="succmsgDiv" style="display:none">
							<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
							<h4>  <i class="banner fa fa-check"></i> Success!</h4>
							<h6 id="succmsg"></h6>
						</div>
					</div>
					<div class="x_content">
						<table>
							<thead>
								<tr>
									<th style="text-align:center">
										<div id="re" class="pull-right" style="background: #fff; cursor: pointer; padding: 5px 10px; border: 1px solid #ccc;">
											<i class="glyphicon glyphicon-calendar fa fa-calendar"></i>&nbsp;
											<span style="width:100%"></span> <b class="caret"></b>
											<input type="hidden" id="startDate" name="startDate" value="">
											<input type="hidden" id="endDate" name="endDate" value="">
										</div>
									</th>
								</tr>
							</thead>
						</table>
						<table id="datatable-buttons" class="table table-striped table-bordered">
							<thead>
								
								<tr>                                                                
                                    <th></th>                                    
                                    
                                    <th></th>
                                    <th></th>
                                    <th></th>                             
                                    <th></th>                             
                                    <th></th>
									<th></th>
									<th></th>
								</tr>
							</thead>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="row" id="contact_query_div" style="display:none;">
		<div class="col-md-12 col-sm-12 col-xs-12">
			<div class="x_panel">
                <div class="x_title">
					<h2>Contact Query</h2>
					<div class="clearfix"></div>
				</div>
                <div class="x_content">
					<table id="datatable-buttons1" class="table table-striped table-bordered">
						<tr>                                                                
							<td><label class="control-label" for="name">Name
							</label></td>                                    
							<td><div id="name" name="name">
							</div></td>
						</tr>
						
						
						<tr>                                                                
							<td><label class="control-label" for="email">Email Id
							</label>
							</td>                                    
							<td><div name="email" id="email" >
							</div></td>
						</tr>
						
						<tr>                                                                
							<td><label class="control-label" for="mobile">Contact Number
							</label>
							</td>                                    
							<td><div name="mobile" id="mobile" >
							</div></td>
						</tr>
						
						<tr>                                                                
							<td><label class="control-label" for="message">Message
							</label>
							</td>                                    
							<td><div name="message" id="message">
							</div></td>
						</tr>
						
						<tr>                                                                
							<td colspan = '2'>
								<button type="reset" class="btn btn-primary" id="cancel">Cancel</button>
							</td>
						</tr>
					</table>
				</div>
			</div>
		</div>
	</div>	
<script src="<?php echo base_url();?>js/bootstrap.min.js"></script>

<!-- bootstrap progress js -->
<script src="<?php echo base_url();?>js/progressbar/bootstrap-progressbar.min.js"></script>
<script src="<?php echo base_url();?>js/nicescroll/jquery.nicescroll.min.js"></script>
<!-- icheck -->
<script src="<?php echo base_url();?>js/icheck/icheck.min.js"></script>
<!-- tags -->
<script src="js/tags/jquery.tagsinput.min.js"></script>
<script src="<?php echo base_url();?>js/custom.js"></script>


<!-- Datatables -->
<!-- <script src="js/datatables/js/jquery.dataTables.js"></script>
<script src="js/datatables/tools/js/dataTables.tableTools.js"></script> -->

<!-- Datatables-->
<script src="<?php echo base_url();?>js/datatables/jquery.dataTables.min.js"></script>
<script src="<?php echo base_url();?>js/datatables/dataTables.bootstrap.js"></script>
<script src="<?php echo base_url();?>js/datatables/dataTables.buttons.min.js"></script>
<script src="<?php echo base_url();?>js/datatables/buttons.bootstrap.min.js"></script>
<script src="<?php echo base_url();?>js/datatables/jszip.min.js"></script>
<script src="<?php echo base_url();?>js/datatables/pdfmake.min.js"></script>
<script src="<?php echo base_url();?>js/datatables/vfs_fonts.js"></script>
<script src="<?php echo base_url();?>js/datatables/buttons.html5.min.js"></script>
<script src="<?php echo base_url();?>js/datatables/buttons.print.min.js"></script>
<script src="<?php echo base_url();?>js/datatables/dataTables.fixedHeader.min.js"></script>
<script src="<?php echo base_url();?>js/datatables/dataTables.keyTable.min.js"></script>
<script src="<?php echo base_url();?>js/datatables/dataTables.responsive.min.js"></script>
<script src="<?php echo base_url();?>js/datatables/responsive.bootstrap.min.js"></script>
<script src="<?php echo base_url();?>js/datatables/dataTables.scroller.min.js"></script>
<script src="<?php echo base_url();?>js/jquery-validate/jquery.validate.min.js" type="text/javascript"></script>

<script src="<?php echo base_url();?>/public/plugins/ckeditor/ckeditor.js"></script>
<script src="<?php echo base_url();?>/public/plugins/ckeditor/js/sample.js"></script>

<link type="text/css" href="<?php echo base_url();?>public/plugins/ckeditor/css/samples.css" />
<link type="text/css" href="<?php echo base_url();?>public/plugins/ckeditor/toolbarconfigurator/lib/codemirror/neo.css" />

<!-- daterangepicker -->
<script type="text/javascript" src="<?php echo base_url();?>js/moment/moment.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>js/datepicker/daterangepicker.js"></script>

<!-- pace -->
<script src="<?php echo base_url();?>js/pace/pace.min.js"></script>	
<script type="text/javascript">
	var write_access = <?=$this->session->userdata('write');?>;
	var delete_access = <?=$this->session->userdata('delete');?>;//alert(delete_access);
</script>	
<script src="<?php echo base_url();?>public/js/contact_queries.js"></script>
<script>
	
	TableManageButtons.init();
</script>		