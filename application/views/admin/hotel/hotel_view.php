<style>
    .error {color: #FF0000;}
    .no_padding{
	padding-left: 0px;
    }
    .custom_padding{
	padding-right: 0px;
	padding-left: 10px;
    }
	
    fieldset.scheduler-border {
	border: 1px groove #ddd !important;
	padding: 0 1.4em 1.4em 1.4em !important;
	margin: 0 0 1.5em 0 !important;
	-webkit-box-shadow:  0px 0px 0px 0px #000;
	box-shadow:  0px 0px 0px 0px #000;
	}
	
	.control-label{
	text-align: left !important;
	}
	.modal{
	height: 500px;
	overflow-y: auto;
	}
	.animate {
	-webkit-animation: animate_bg 5s;
	animation: animate_bg 5s;
	-webkit-animation-iteration-count: infinite;
	animation-iteration-count: infinite;
	}
	
	@keyframes animate_bg {
	0%   {color:red;}
	100% {color:blue;}
	}
	
	@keyframes animate_bg {
	0%   {color:red;}
	100% {color:blue;}
	}
	
	@-webkit-keyframes animate_bg {
	0%   {color:red;}
	100% {color:blue;}
	}
	.remove_img_preview {
	position:relative;
	top:5px;
	right:70px;
	background:white;
	
	border-radius:150px;
	font-size:2em;
	padding: 0.3em 0.6em;
	text-align:center;
	cursor:pointer;
	opacity: 0.5;
	
	}
	.remove_img_preview:before {
	content: "×";
	font-weight: bold;
	color:black;
	}
	
	
	
	#cat_form .controls {
	position: fixed;
	top: 0;
	left: 0;
	right: 0;
	background: #fff;
	z-index: 1;
	padding: 6px 10px;
	box-shadow: 0px 0px 10px rgba(0, 0, 0, 0.5);
	}
	
	#cat_form input[type=checkbox] {
	vertical-align: middle !important;
	}
	
	#cat_form .tree {
	margin: 2% auto;
	width: 80%;
	}
	
	#cat_form .tree ul {
	display: none;
	margin: 4px auto;
	margin-left: 6px;
	border-left: 1px dashed #dfdfdf;
	list-style: none;
	}
	
	
	#cat_form .tree li {
	padding: 12px 18px;
	cursor: pointer;
	vertical-align: middle;
	background: #fff;
	}
	
	#cat_form .tree li:first-child {
	border-radius: 3px 3px 0 0;
	}
	
	#cat_form .tree li:last-child {
	border-radius: 0 0 3px 3px;
	}
	
	#cat_form .tree .active,
	#cat_form .active li {
	background: #efefef;
	}
	
	#cat_form .tree label {
	cursor: pointer;
	}
	
	#cat_form .tree input[type=checkbox] {
	margin: -2px 6px 0 0px;
	}
	
	#cat_form .has > label {
	color: #000;
	}
	
	#cat_form .tree .total {
	color: #e13300;
	}
</style>

<!-- page content -->
<div class="right_col" role="main">
	<div class="">
		<div class="page-title">
            <div class="title_left">
				<h3>
                    Hotel Master
				</h3>
			</div>
			<div class="title_right" style="float: right !important;">
				<div class="col-md-2 col-sm-2 col-xs-12 form-group pull-right top_search" style="float: right !important;">
					<div class="input-group">
						<span class="buttons">
								<button class="btn btn-info btn-sm" type="button" id="add_new_hotel_btn" style="padding: none !important;">Add Hotel</button>
						</span>
					</div>
				</div>
			</div>
		</div>
		<div class="clearfix"></div>
		<div class="row">
			<div class="col-md-12 col-sm-12 col-xs-12">
				<div class="x_panel">
					<div class="x_title">
						<h2>Hotels</h2>
						
						<div class="clearfix"></div>
						<div class="alert alert-danger alert-dismissable" id="errormsgDiv" style="display:none">
							<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
							<h4><i class="banner fa fa-ban"></i> Error!</h4>
							<h6 id="errormsg"></h6>
						</div>
						
						<div class="alert alert-success alert-dismissable" id="succmsgDiv" style="display:none">
							<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
							<h4>  <i class="banner fa fa-check"></i> Success!</h4>
							<h6 id="succmsg"></h6>
						</div>
					</div>
					<div class="x_content">
						<table id="datatable-buttons" class="table table-striped table-bordered" style="width:100%">
							<thead>
								<tr>
 								    <th></th>
                                    <th></th>
                                    <th></th>
									<th></th>
									<th></th>
									<th></th>
								</tr>
							</thead>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="row" id="hotel_div" style="display:none;">
		<div class="col-md-12 col-sm-12 col-xs-12">
			<div class="x_panel">
                <div class="x_title">
					<h2>Hotel</h2>
					<div class="clearfix"></div>
				</div>
		
                <div class="x_content">
				    <!--tab panel starts here-->
					<div class="" role="tabpanel" data-example-id="togglable-tabs">
						<ul id="myTab" class="nav nav-tabs bar_tabs" role="tablist">
							<li role="presentation" class="active" id="basic_link"><a href="#tab_basic" id="basic_a" role="tab" data-toggle="tab" aria-expanded="true">Basic Info</a>
							</li>
							
							<li role="presentation" id="details_link" style="display:none;"><a href="#tab_details" id="details_a" role="tab" data-toggle="tab" aria-expanded="true">Detail Info</a>
							</li>
							
							<li role="presentation" class="" id="image_link" style="display:none;"><a href="#tab_image" role="tab" id="image_a" data-toggle="tab" aria-expanded="false">Images</a>
							</li>
						</ul>
						<div id="myTabContent" class="tab-content">
							<div role="tabpanel" class="tab-pane fade active in" id="tab_basic" aria-labelledby="basic_a">
								<form name="basic_form" id="basic_form" action="" method="post" accept-charset="utf-8" class="form-horizontal form-label-left">
									<input type="hidden" name="b_hotel_id" id="b_hotel_id" value="0" />
									<div class="form-group">
										<label class="control-label col-md-2 col-sm-3 col-xs-12" for="hotel_name">Hotel Name <span class="required">*</span>
										</label>
										<div class="col-md-4 col-sm-6 col-xs-12">
											<input type="text" id="hotel_name" name="hotel_name" required="required" class="form-control col-md-6 col-xs-12" placeholder="Enter Hotel Name">
										</div>
									</div>
									
									<div class="form-group">
										<label class="control-label col-md-2 col-sm-3 col-xs-12" for="price">Hotel Price <span class="required">*</span>
										</label>
										<div class="col-md-4 col-sm-6 col-xs-12">
											<input type="number" id="price" name="price" required="required" class="form-control col-md-6 col-xs-12" placeholder="Enter price">
										</div>
									</div>
									
									<div class="form-group">
										<label class="control-label col-md-2 col-sm-2 col-xs-12" for="state_id">State<span class="required">*</span>
										</label>
										<div class="col-md-4 col-sm-9 col-xs-12">
											<select required="required" class="form-control state_id" name="state_id" id="state_id" onchange="get_city_by_state(this.value);">
											  <option value="">Please Select Select</option>
											</select>
										</div>
									</div>

									<div class="form-group">
										<label class="control-label col-md-2 col-sm-2 col-xs-12" for="city_id">City<span class="required">*</span>
										</label>
										<div class="col-md-4 col-sm-9 col-xs-12">
											<select required="required" class="form-control city_id" name="city_id" id="city_id">
											  <option value="">Please Select City</option>
											</select>
										</div>
									</div>

									<div class="form-group">
										<label class="control-label col-md-2 col-sm-2 col-xs-12" for="package_id">Package<span class="required">*</span>
										</label>
										<div class="col-md-4 col-sm-9 col-xs-12">
											<select required="required" class="form-control city_id" name="package_id" id="package_id">
											  <option value="">Please Select Package</option>
											</select>
										</div>
									</div>
									
									<div class="form-group">
										<label class="control-label col-md-2 col-sm-2 col-xs-12" for="style"> Style </label>
										<div class="col-md-4 col-sm-9 col-xs-12">
											<input type="text" required="required" class="form-control" placeholder="Hotel Style" name="style" id="style"/>
										</div>
									</div>

									<div class="form-group">
										<label class="control-label col-md-2 col-sm-2 col-xs-12" for="setting"> Setting </label>
										<div class="col-md-4 col-sm-9 col-xs-12">
											<input type="text" required="required" class="form-control" placeholder="Hotel setting" name="setting" id="setting"/>
										</div>
									</div>
									
									<div class="form-group">
										<label class="control-label col-md-2 col-sm-3 col-xs-12">Type<span class="required">*</span></label>
										<div class="col-md-6 col-sm-6 col-xs-12">
											<input type="radio" class="flat" name="type" value="1" checked="" required />&nbsp; hotel&nbsp;&nbsp;
											<input type="radio" class="flat" name="type" value="0"/>&nbsp;&nbsp;Resort
										</div>
									</div>

									<div class="form-group">
										<label class="control-label col-md-2 col-sm-2 col-xs-12" for="extras"> Extra </label>
										<div class="col-md-4 col-sm-9 col-xs-12">
											<input type="text" required="required" class="form-control" placeholder="Hotel extra" name="extras" id="extras"/>
										</div>
									</div>

									<div class="form-group">
										<label class="control-label col-md-2 col-sm-2 col-xs-12" for="facilities_ids"> Facilities </label>
										<div class="col-md-6 col-sm-9 col-xs-12" id="facilities_div">
											
										</div>
									</div>
									
									<div class="form-group">
										<label class="control-label col-md-2 col-sm-3 col-xs-12">Active Status<span class="required">*</span></label>
										<div class="col-md-6 col-sm-6 col-xs-12">
											<input type="radio" class="flat" name="status" value="1" checked="" required />&nbsp; Active&nbsp;&nbsp;
											<input type="radio" class="flat" name="status" value="0"/>&nbsp;&nbsp;Inactive
										</div>
									</div>
									
									<div class="ln_solid"></div>
									<div class="form-group">
										<div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
											<button type="reset" class="btn btn-primary" id="basic_cancel">Cancel</button>
											<button type="submit" id="basic_submit" class="btn btn-success">Submit</button>
										</div>
									</div>
								</form>
							</div>
							
							<div role="tabpanel" class="tab-pane fade in" id="tab_details" aria-labelledby="basic_a">
								<form name="details_form" id="details_form" action="" method="post" enctype="multipart/form-data" accept-charset="utf-8" class="form-horizontal form-label-left">
									<input type="hidden" name="d_hotel_id" id="d_hotel_id" value="0" />
									
									<div class="form-group">
										<label class="control-label col-md-3 col-sm-3 col-xs-12" for="highlights">Highlights
										</label>
										<div class="col-md-6 col-sm-6 col-xs-12">
											<textarea class="form-control ckeditor" name="highlights" id="highlights" >
											</textarea>
										</div>
									</div>
									
									<div class="form-group">
										<label class="control-label col-md-3 col-sm-3 col-xs-12" for="offers"> Offers
										</label>
										<div class="col-md-6 col-sm-6 col-xs-12">
											<textarea class="form-control ckeditor" name="offers" id="offers">
											</textarea>
										</div>
									</div>
									
									<div class="form-group">
										<label class="control-label col-md-3 col-sm-3 col-xs-12" for="favourite_rooms">Favourite Rooms</label>
										<div class="col-md-6 col-sm-6 col-xs-12">
											<textarea class="form-control ckeditor" name="favourite_rooms" id="favourite_rooms" >
											</textarea>
										</div>
									</div>
									
									<div class="form-group">
										<label class="control-label col-md-3 col-sm-3 col-xs-12">Active Status<span class="required">*</span></label>
										<div class="col-md-6 col-sm-6 col-xs-12">
											<input type="radio" class="flat" name="status" value="1" checked="" required />&nbsp; Active&nbsp;&nbsp;
											<input type="radio" class="flat" name="status" value="0"/>&nbsp;&nbsp;Inactive
										</div>
									</div>
									
									<div class="ln_solid"></div>
									<div class="form-group">
										<div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
											<button type="reset" class="btn btn-primary" id="basic_cancel">Cancel</button>
											<button type="submit" id="basic_submit" class="btn btn-success">Submit</button>
										</div>
									</div>
								</form>
							
							</div>
							
							
							<div role="tabpanel" class="tab-pane fade" id="tab_image" aria-labelledby="image_a">
								<div class="">
									<div class="page-title">
										<div class="title_left">
											<h3>
												Gallery Images
											</h3>
										</div>
										<div class="title_right" style="float: right !important;">
											<div class="col-md-2 col-sm-2 col-xs-12 form-group pull-right top_search" style="float: right !important;">
												<div class="input-group">
													<span class="buttons">
														<button class="btn btn-info btn-sm" type="button" id="add_new_img_btn" style="padding: none !important;">Add Image Gallery</button>
													</span>
												</div>
											</div>
										</div>
									</div>
									<div class="clearfix"></div>
									<div class="row">
										<div class="col-md-12 col-sm-12 col-xs-12">
											<div class="x_panel">
												<div class="x_content">
													<table id="datatable-images" class="table table-striped table-bordered" style="width:100%">
										<thead>
															<tr>    
																<th></th> 
																<th></th>
																<th></th>
																<th></th>
																<th></th> 
															</tr>
														</thead>
													</table>
												</div>
											</div>
										</div>
									</div>
								</div>
								<form name="image_form" id="image_form" action="" method="post" enctype="multipart/form-data" accept-charset="utf-8" class="form-horizontal form-label-left" style="display:none;">
									<input type="hidden" name="i_hotel_id" id="i_hotel_id" value="0" />
									<input type="hidden" name="image_id" id="image_id" value="0" />
									<div class="form-group">
										<label class="control-label col-md-3 col-sm-3 col-xs-12" for="image_name">Image
										</label>
										<div class="col-md-2 col-sm-2 col-xs-12">
										<div id="wrapper">

		                                 <div id='menu_div'>

			                                <div id="form_div">
				                              <div id="file_div">
				                                <div>
													<span style="width:50%"><input type="file" id="image_name" name="image_name[]"></span>
													<span id="image_name_div" style="width:50%"></span>
												</div>
						                        <button type="button" onclick="add_file();" class="btn btn-sm btn-primary add_btn"><i class="fa fa-plus"></i></button>
				                             </div>
			                                </div>

		                                  </div>
	                                    </div>
	
										    <!--<div class="field_wrapper">
                                        <div>
                                          <input type="file" name="image_name[]" class="banner" value="" multiple>
                                          <a href="javascript:void(0);" class="btn btn-sm btn-primary add_button" title="Add field"><i class="fa fa-plus"></i></a>
                                        </div>
                                    </div>-->
										</div>
									</div>
									
									<div class="form-group">
										<label class="control-label col-md-3 col-sm-3 col-xs-12">Active Status <span class="required">*</span></label>
										<div class="col-md-6 col-sm-6 col-xs-12">
											<input type="radio" class="flat" name="img_status" value="1" checked="" required />&nbsp; Active&nbsp;&nbsp;
											<input type="radio" class="flat" name="img_status" value="0"/>&nbsp;&nbsp;Inactive
										</div>
									</div>
									<div class="ln_solid"></div>
									<div class="form-group">
										<div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
											<button type="reset" class="btn btn-primary" id="img_cancel">Cancel</button>
											<button type="submit" id="img_submit" class="btn btn-success">Submit</button>
										</div>
									</div>
								</form>
							</div>							
						</div>
						
						<div class="alert alert-danger alert-dismissable" id="errormsgDiv" style="display:none">
							<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
							<h4><i class="banner fa fa-ban"></i> Error!</h4>
							<h6 id="errormsg"></h6>
						</div>

						<div class="alert alert-success alert-dismissable" id="succmsgDiv" style="display:none">
							<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
							<h4>  <i class="banner fa fa-check"></i> Success!</h4>
							<h6 id="succmsg"></h6>
						</div>
						
					</div>
					<!-- tab panel ends here-->
				</div>
			</div>
		</div>
	</div>	
	
	
	<script src="<?php echo base_url();?>js/bootstrap.min.js"></script>
	
	<!-- bootstrap progress js -->
	<script src="<?php echo base_url();?>js/admin/progressbar/bootstrap-progressbar.min.js"></script>
	<script src="<?php echo base_url();?>js/admin/nicescroll/jquery.nicescroll.min.js"></script>
	<!-- icheck -->
	<script src="<?php echo base_url();?>js/admin/icheck/icheck.min.js"></script>
	<!-- tags -->
	<script src="<?php echo base_url();?>js/admin/tags/jquery.tagsinput.min.js"></script>
	<script src="<?php echo base_url();?>js/admin/custom.js"></script>
	
	
	<!-- Datatables -->
	<!-- <script src="js/datatables/js/jquery.dataTables.js"></script>
	<script src="js/datatables/tools/js/dataTables.tableTools.js"></script> -->
	
	<!-- Datatables-->
	<script src="<?php echo base_url();?>js/admin/datatables/jquery.dataTables.min.js"></script>
	<script src="<?php echo base_url();?>js/admin/datatables/dataTables.bootstrap.js"></script>
	<script src="<?php echo base_url();?>js/admin/datatables/dataTables.buttons.min.js"></script>
	<script src="<?php echo base_url();?>js/admin/datatables/buttons.bootstrap.min.js"></script>
	<script src="<?php echo base_url();?>js/admin/datatables/jszip.min.js"></script>
	<script src="<?php echo base_url();?>js/admin/datatables/pdfmake.min.js"></script>
	<script src="<?php echo base_url();?>js/admin/datatables/vfs_fonts.js"></script>
	<script src="<?php echo base_url();?>js/admin/datatables/buttons.html5.min.js"></script>
	<script src="<?php echo base_url();?>js/admin/datatables/buttons.print.min.js"></script>
	<script src="<?php echo base_url();?>js/admin/datatables/dataTables.fixedHeader.min.js"></script>
	<script src="<?php echo base_url();?>js/admin/datatables/dataTables.keyTable.min.js"></script>
	<script src="<?php echo base_url();?>js/admin/datatables/dataTables.responsive.min.js"></script>
	<script src="<?php echo base_url();?>js/admin/datatables/responsive.bootstrap.min.js"></script>
	<script src="<?php echo base_url();?>js/admin/datatables/dataTables.scroller.min.js"></script>
	<script src="<?php echo base_url();?>js/admin/jquery-validate/jquery.validate.min.js" type="text/javascript"></script>
	<script src="<?php echo base_url();?>js/admin/jquery-validate/additional-methods.min.js"></script>
	<script src="<?php echo base_url();?>public/plugins/ckeditor/ckeditor.js"></script>
	<script src="<?php echo base_url();?>public/plugins/ckeditor/js/sample.js"></script>
	
	<link type="text/css" href="<?php echo base_url();?>public/plugins/ckeditor/css/samples.css" />
	<link type="text/css" href="<?php echo base_url();?>public/plugins/ckeditor/toolbarconfigurator/lib/codemirror/neo.css" />
	
	<!-- daterangepicker -->
	<script type="text/javascript" src="<?php echo base_url();?>js/admin/moment/moment.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>js/admin/datepicker/daterangepicker.js"></script>
	
	<!-- pace -->
	<script src="<?php echo base_url();?>js/admin/pace/pace.min.js"></script>
    <script type="text/javascript">
		var hotel_upload_url = "<?php echo $upload_path;?>";
	</script>	
	<script src="<?php echo base_url();?>public/js/hotel.js"></script>
	<script type="text/javascript">
		var hotel_upload_url = "<?php echo $upload_path;?>";
	</script>
	<script>
		CKEDITOR.replace('highlights', {
		// Define the toolbar groups as it is a more accessible solution.
		toolbarGroups: [
		{"name":"basicstyles","groups":["basicstyles"]},
		{"name":"links","groups":["links"]},
		{"name":"paragraph","groups":["list","blocks"]},
		{"name":"document","groups":["mode"]},
		{"name":"insert","groups":["insert"]},
		{"name":"styles","groups":["styles"]},
		{"name":"about","groups":["about"]}
		],
		// Remove the redundant buttons from toolbar groups defined above.
		removeButtons: 'Underline,Strike,Subscript,Superscript,Anchor,Styles,Specialchar'
	} );
	
	CKEDITOR.replace('offers', {
		// Define the toolbar groups as it is a more accessible solution.
		toolbarGroups: [
		{"name":"basicstyles","groups":["basicstyles"]},
		{"name":"links","groups":["links"]},
		{"name":"paragraph","groups":["list","blocks"]},
		{"name":"document","groups":["mode"]},
		{"name":"insert","groups":["insert"]},
		{"name":"styles","groups":["styles"]},
		{"name":"about","groups":["about"]}
		],
		// Remove the redundant buttons from toolbar groups defined above.
		removeButtons: 'Underline,Strike,Subscript,Superscript,Anchor,Styles,Specialchar'
		} );
	
	CKEDITOR.replace('favourite_rooms', {
		// Define the toolbar groups as it is a more accessible solution.
		toolbarGroups: [
		{"name":"basicstyles","groups":["basicstyles"]},
		{"name":"links","groups":["links"]},
		{"name":"paragraph","groups":["list","blocks"]},
		{"name":"document","groups":["mode"]},
		{"name":"insert","groups":["insert"]},
		{"name":"styles","groups":["styles"]},
		{"name":"about","groups":["about"]}
		],
		// Remove the redundant buttons from toolbar groups defined above.
		removeButtons: 'Underline,Strike,Subscript,Superscript,Anchor,Styles,Specialchar'
		} );
	
		TableManageButtons.init();
		TableManageButtons1.init();
		
		function add_file()
		{
		 $("#file_div").append("<div><input type='file' name='image_name[]'><button type='button' onclick=remove_file(this); class='btn btn-sm btn-danger remove_btn'><i class='fa fa-minus'></button></div>");
		}
		function remove_file(ele)
		{
		 $(ele).parent().remove();
		}
	</script>	
	<script type="text/javascript">
		$.ajax({

			url:base_url+'api/facility_list',

			type:'POST',

			data:{'check_flag':1},

			success:function(response)
			{
				console.log(response.data);
				$.each(response.data,function(key,val){

				$('#facilities_div').append('<input type="checkbox" name="facilities_ids[]" value="'+val.facility_id+'"> '+val.facility_name+'<br>');
				});
			}
		});
	</script>		