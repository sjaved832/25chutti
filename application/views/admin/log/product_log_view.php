<?php echo $this->load->view('/common/header'); ?>

<link href="<?php echo base_url();?>public/plugins/datatables/dataTables.bootstrap.css" rel="stylesheet" type="text/css" />
<style>
.box-body{
    display: flex;
}
.error {
    color: #FF0000;
}
 #loading {
        width: 100%;
        height: 100%;
        top: 0px;
        left: 0px;
        position: fixed;
        display: block;
        opacity: .9;
        background-color: #fff;
        z-index: 99;
        text-align: center;
    }
    #loading-image1 {
        position: absolute;
        top: 250px;
        left: 50%;
        z-index: 600;
    }
   
</style>

<div class="content-wrapper">
  <section class="content-header">
      <h1>
        <small>Product Log</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?php echo base_url()?>index.php?/dashboard"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#" style="cursor:default"><i class=""></i> Logs</a></li>
        <li class="active">product log</li>
      </ol>
  </section>
  <section class="content">
      <div class="row">
          <div class="col-md-12">
              <div class="box box-primary">
                   <div class="alert alert-success alert-dismissable" id="succmsgDiv" style="display:none">
                      <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                      <h4><i class="icon fa fa-check"></i>Success!</h4>
                      <span id="succmsg"></span>
                  </div>

                   <div class="box-body" id="box_body">
                    <div class="row col-md-12 " id ="order_filter_form" class="order_filter_form_class">                      
                      
                      <div class="form-group col-xs-6" id="seller_multiselect">
                        <label>Select Seller :</label> 
                        <select class="form-control" name="seller_id" id="seller_id" multiple="multiple">
                        </select>
                      </div>

                      <div class="form-group col-xs-3">
                          <strong>Select date :</strong>
                          <div id="daterange"  class="col-md-2" style="width: 107% !important;background: #fff; cursor: pointer; padding: 5px 10px; border: 1px solid #ccc">
                              <i class="glyphicon glyphicon-calendar fa fa-calendar"></i>
                              <span></span> <b class="caret"></b>
                          </div>
                          <input type="hidden" name="fromdate" id="fromdate" /> 
                          <input type="hidden" name="todate" id="todate" /> 
                      </div>

                    </div><!-- /.box-body -->
                  </div>

                 
                  <div class="box">
                    <br>
                    <table id="hid_excel_content" style="display:none;"></table>
                    <table id="datatable_fixed_column" class="table table-bordered table-striped" width="100%" style='overflow:scroll;'>
                      <thead>
                       <tr>
                        <th></th>
                        <th><input type="text" name="seller_name" placeholder="Search Seller Name"></th>
                        <th><input type="text" name="product_code" placeholder="Search Product Code"></th>
                        <th><input type="text" name="product_name" placeholder="Search Product Name"></th>
                        <th><input type="text" name="brand_name" placeholder="Search Brand"></th>
                        <th><input type="text" name="packof" placeholder="Search Pack"></th>
                      </tr>
                        <tr  id="table_heading">                                                                
                          <th></th> 
                          <th></th> 
                          <th></th> 
                          <th></th> 
                          <th></th>  
                          <th></th>  
                          <th></th>  
                          <th></th>  
                        </tr>
                      </thead>
                      <tbody id="table2excel">
                        <tr> 
                          <td colspan="7" class="dataTables_empty">
                            <div style ="text-align:center;">
                              <img id="loading-image" src="<?php echo base_url();?>public/img/ajax-loader.gif" alt="Loading..."/>
                             Loading....              
                            </div> 
                          </td>
                        </tr>
                      </tbody>
                    </table>
                  </div>
              </div>
          </div>
      </div>
      <div id="loading" style="display:none;">
      <img  id="loading-image1" src="<?php echo base_url();?>public/img/loading-icon.gif" alt="Loading..."/>
  </div> 
  </section>
</div>
  
<?php echo $this->load->view('/common/footer'); ?>

<script src="<?php echo base_url();?>public/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="<?php echo base_url();?>public/plugins/datatables/dataTables.colVis.min.js"></script>
<script src="<?php echo base_url();?>public/plugins/datatables/dataTables.tableTools.min.js"></script>
<script src="<?php echo base_url();?>public/plugins/datatables/dataTables.bootstrap.min.js"></script>
<script src="<?php echo base_url();?>public/plugins/datatable-responsive/datatables.responsive.min.js"></script>
<script src="<?php echo base_url();?>public/plugins/table2excel/src/jquery.table2excel.js"></script>
<script src="<?php echo base_url();?>public/bootstrap/bootstrap-daterangepicker-master/moment.min.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>public/bootstrap/bootstrap-daterangepicker-master/daterangepicker.js" type="text/javascript"></script>
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>public/bootstrap/bootstrap-daterangepicker-master/daterangepicker-bs3.css"  />
<script src="<?php echo base_url();?>public/js/product_log.js"></script>