<?php echo $this->load->view('/common/header'); ?>

<link href="<?php echo base_url();?>public/plugins/datatables/dataTables.bootstrap.css" rel="stylesheet" type="text/css" />
<style>
.box-body{
    display: flex;
}
.error {
    color: #FF0000;
}
td.details-control {
    background: url('public/img/details_open.png') no-repeat center center;
    cursor: pointer;
}
tr.shown td.details-control {
    background: url('public/img/details_close.png') no-repeat center center;
}
</style>

<div class="content-wrapper">
  <section class="content-header">
      <h1>
        <small>Product Log</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?php echo base_url()?>index.php?/dashboard"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#" style="cursor:default"><i class=""></i> Logs</a></li>
        <li class="active">product log list</li>
      </ol>
  </section>
  <section class="content">
      <div class="row">
          <div class="col-md-12">
              <div class="box box-primary">
                   <div class="alert alert-success alert-dismissable" id="succmsgDiv" style="display:none">
                      <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                      <h4><i class="icon fa fa-check"></i>Success!</h4>
                      <span id="succmsg"></span>
                  </div>

                        <div class="box-header">                        
                        <div class="box-footer pull-right">
                           <a href="<?php echo site_url('/log/product_log');?>" class="btn-primary btn outlined mleft_no" >Back To Parent</a>
                           
                        </div>
                        
                    </div>
                  <div class="box">
                    <br>
                    <table id="datatable_fixed_column" class="table table-bordered table-striped display" width="100%"  cellspacing="0"  style='overflow:scroll;'>
                      <thead>
                        <tr>
                          <th></th>
                          <th></th>
                          <th></th>
                          <th><input type="text" name="audit_portal" placeholder="Search Audit Portal"></th>
                        </tr>                       
                        <tr>  
                          <th></th>    
                          <th></th>    
                          <th></th>  
                          <th></th>  
                          <th></th>  
                          <th></th>  
                        </tr>
                      </thead>
                      <tbody>
                        <tr> 
                          <td colspan="7" class="dataTables_empty">
                            <div style ="text-align:center;">
                              <img id="loading-image" src="<?php echo base_url();?>public/img/ajax-loader.gif" alt="Loading..."/>
                             Loading....              
                            </div> 
                          </td>
                        </tr>
                      </tbody>
                    </table>
                  </div>
              </div>
          </div>
      </div>
  </section>
</div>
    
<?php echo $this->load->view('/common/footer'); ?>

<script src="<?php echo base_url();?>public/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="<?php echo base_url();?>public/plugins/datatables/dataTables.colVis.min.js"></script>
<script src="<?php echo base_url();?>public/plugins/datatables/dataTables.tableTools.min.js"></script>
<script src="<?php echo base_url();?>public/plugins/datatables/dataTables.bootstrap.min.js"></script>
<script src="<?php echo base_url();?>public/plugins/datatable-responsive/datatables.responsive.min.js"></script>
<script type="text/javascript">
  var product_id             = '<?=$product_id;?>'
</script>
<script src="<?php echo base_url();?>public/js/product_list_log.js"></script>