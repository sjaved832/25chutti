<style>
    .error {color: #FF0000;}
    .no_padding{
		padding-left: 0px;
    }
    .custom_padding{
		padding-right: 0px;
		padding-left: 10px;
    }
	
    fieldset.scheduler-border {
		border: 1px groove #ddd !important;
		padding: 0 1.4em 1.4em 1.4em !important;
		margin: 0 0 1.5em 0 !important;
		-webkit-box-shadow:  0px 0px 0px 0px #000;
		box-shadow:  0px 0px 0px 0px #000;
	}
	
	.control-label{
		text-align: left !important;
	}
	.modal{
		height: 500px;
		overflow-y: auto;
	}
	.animate {
		-webkit-animation: animate_bg 5s;
		animation: animate_bg 5s;
		-webkit-animation-iteration-count: infinite;
		animation-iteration-count: infinite;
	}

	@keyframes animate_bg {
		0%   {color:red;}
		100% {color:blue;}
	}

	@keyframes animate_bg {
		0%   {color:red;}
		100% {color:blue;}
	}

	@-webkit-keyframes animate_bg {
		0%   {color:red;}
		100% {color:blue;}
	}
</style>

<!-- page content -->
<div class="right_col" role="main">
	<div class="">
		<div class="page-title">
            <div class="title_left">
				<h3>
                    Menu List
                </h3>
			</div>
			<div class="title_right" style="float: right !important;">
				<div class="col-md-2 col-sm-2 col-xs-12 form-group pull-right top_search" style="float: right !important;">
					<div class="input-group">
						<span class="buttons">
							<?php if($this->session->userdata('write')==1){?>
								<button class="btn btn-info btn-sm" type="button" id="add_new_btn" style="padding: none !important;">Add New Menu</button>
								<?php }else{ ?>
								<button class="btn btn-info btn-sm" type="button" id="add_new_btn" style="padding: none !important;" disabled>Add New Menu</button>
							<?php }?>
						</span>
					</div>
				</div>
			</div>
		</div>
		<div class="clearfix"></div>
		
		<div class="row">
			
			<div class="col-md-12 col-sm-12 col-xs-12">
				<div class="x_panel">
					<div class="x_title">
						<h2>Menu List</h2>
						
						<div class="clearfix"></div>
						<div class="alert alert-danger alert-dismissable" id="errormsgDiv" style="display:none">
							<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
							<h4><i class="icon fa fa-ban"></i> Error!</h4>
							<h6 id="errormsg"></h6>
						</div>

						<div class="alert alert-success alert-dismissable" id="succmsgDiv" style="display:none">
							<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
							<h4>  <i class="icon fa fa-check"></i> Success!</h4>
							<h6 id="succmsg"></h6>
						</div>
					</div>
					<div class="x_content">
						<table id="datatable-buttons" class="table table-striped table-bordered">
							<thead>
								
								<tr>                                                                
                                    <th></th>                                    
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>                             
                                </tr>
							</thead>
						</table>
					</div>
				</div>
			</div>
			
		</div>
	</div>
	<div class="row" id="admin_form_div" style="display:none;">
		<div class="col-md-12 col-sm-12 col-xs-12">
			<div class="x_panel">
                <div class="x_title">
					<h2>Menu Entry</h2>
					<div class="clearfix"></div>
				</div>
                <div class="x_content">
					<br />
					<form name="menumaster" id="menumaster" action="" method="" enctype="multipart/form-data" accept-charset="utf-8" class="form-horizontal form-label-left">
						<input type="hidden" name="id" id="id" value="0" />
						<div class="form-group">
							<label class="control-label col-md-2 col-sm-2 col-xs-12" for="menuname">Menu Name<span class="required">*</span>
							</label>
							<div class="col-md-9 col-sm-9 col-xs-12">
								<input type="text" id="menuname" name="menuname" required="required" class="form-control col-md-7 col-xs-12" placeholder="Enter Menu Name">
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-md-2 col-sm-2 col-xs-12" for="parent_menu">Parent Menu Name
							</label>
							<div class="col-md-9 col-sm-9 col-xs-12">
								<select class="select2 form-control" name="parent_menu" id="parent_menu" placeholder="Select Parent Menu Name">
									<option value=""></option>
								</select>
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-md-2 col-sm-2 col-xs-12" for="href">HREF
							</label>
							<div class="col-md-9 col-sm-9 col-xs-12">
								<input type="text" id="href" name="href" class="form-control col-md-7 col-xs-12" placeholder="Enter HREF">
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-md-2 col-sm-2 col-xs-12" for="sort_order">Sort Order
							</label>
							<div class="col-md-9 col-sm-9 col-xs-12">
								<input type="text" id="sort_order" name="sort_order" required="required" class="form-control col-md-7 col-xs-12" placeholder="Enter Sort Order">
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-md-2 col-sm-2 col-xs-12" for="controllername">Controller Name
							</label>
							<div class="col-md-9 col-sm-9 col-xs-12">
								<input type="text" id="controllername" name="controllername" class="form-control col-md-7 col-xs-12" placeholder="Enter Controller Name">
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-md-2 col-sm-2 col-xs-12">Display</label>
							<div class="col-md-9 col-sm-9 col-xs-12">
								<input type="radio" class="flat" name="display_menu" value="Y" checked="" required />&nbsp; Yes&nbsp;&nbsp;
								<input type="radio" class="flat" name="display_menu" value="N"/>&nbsp;&nbsp;NO
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-md-2 col-sm-2 col-xs-12">Is Default</label>
							<div class="col-md-9 col-sm-9 col-xs-12">
								<input type="radio" class="flat" name="is_default" value="N" checked="" required />&nbsp; NO&nbsp;&nbsp;
								<input type="radio" class="flat" name="is_default" value="Y"/>&nbsp;&nbsp;Yes
							</div>
						</div>
						<div class="ln_solid"></div>
						<div class="form-group">
							<div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
								<button type="reset" class="btn btn-primary" id="cancel">Cancel</button>
								<button type="submit" id="adminsubmit" class="btn btn-success">Submit</button>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
	<div id="delete_dialog" title="Confirmation Required" class="modal fade bs-example-modal-sm" tabindex="-1" role="dialog" aria-hidden="true">
	</div>
<script src="<?php echo base_url();?>js/bootstrap.min.js"></script>

<!-- bootstrap progress js -->
<script src="<?php echo base_url();?>js/progressbar/bootstrap-progressbar.min.js"></script>
<script src="<?php echo base_url();?>js/nicescroll/jquery.nicescroll.min.js"></script>
<!-- icheck -->
<script src="<?php echo base_url();?>js/icheck/icheck.min.js"></script>
<!-- tags -->
<script src="js/tags/jquery.tagsinput.min.js"></script>
<script src="<?php echo base_url();?>js/custom.js"></script>


<!-- Datatables -->
<!-- <script src="js/datatables/js/jquery.dataTables.js"></script>
<script src="js/datatables/tools/js/dataTables.tableTools.js"></script> -->
	
<!-- Datatables-->
<script src="<?php echo base_url();?>js/datatables/jquery.dataTables.min.js"></script>
<script src="<?php echo base_url();?>js/datatables/dataTables.bootstrap.js"></script>
<script src="<?php echo base_url();?>js/datatables/dataTables.buttons.min.js"></script>
<script src="<?php echo base_url();?>js/datatables/buttons.bootstrap.min.js"></script>
<script src="<?php echo base_url();?>js/datatables/jszip.min.js"></script>
<script src="<?php echo base_url();?>js/datatables/pdfmake.min.js"></script>
<script src="<?php echo base_url();?>js/datatables/vfs_fonts.js"></script>
<script src="<?php echo base_url();?>js/datatables/buttons.html5.min.js"></script>
<script src="<?php echo base_url();?>js/datatables/buttons.print.min.js"></script>
<script src="<?php echo base_url();?>js/datatables/dataTables.fixedHeader.min.js"></script>
<script src="<?php echo base_url();?>js/datatables/dataTables.keyTable.min.js"></script>
<script src="<?php echo base_url();?>js/datatables/dataTables.responsive.min.js"></script>
<script src="<?php echo base_url();?>js/datatables/responsive.bootstrap.min.js"></script>
<script src="<?php echo base_url();?>js/datatables/dataTables.scroller.min.js"></script>
<script src="<?php echo base_url();?>js/jquery-validate/jquery.validate.min.js" type="text/javascript"></script>

<script src="<?php echo base_url();?>/public/plugins/ckeditor/ckeditor.js"></script>
<script src="<?php echo base_url();?>/public/plugins/ckeditor/js/sample.js"></script>

<link type="text/css" href="<?php echo base_url();?>public/plugins/ckeditor/css/samples.css" />
<link type="text/css" href="<?php echo base_url();?>public/plugins/ckeditor/toolbarconfigurator/lib/codemirror/neo.css" />

<!-- daterangepicker -->
<script type="text/javascript" src="<?php echo base_url();?>js/moment/moment.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>js/datepicker/daterangepicker.js"></script>
	
<!-- pace -->
<script src="<?php echo base_url();?>js/pace/pace.min.js"></script>	
<script type="text/javascript">
	var write_access = <?=$this->session->userdata('write');?>;
	
	var delete_access = <?=$this->session->userdata('delete');?>;//alert(delete_access);
</script>	
<script src="<?php echo base_url();?>public/js/manage_navigation.js"></script>
<script src="<?php echo base_url();?>public/plugins/select2/select2.js"></script>

<script>
	TableManageButtons.init();
</script>
		