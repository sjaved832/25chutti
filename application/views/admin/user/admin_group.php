  <?php $this->load->view('/common/header');?>
  <title>Grade</title>

  <!-- Bootstrap core CSS -->

  <link href="<?php echo base_url();?>css/bootstrap.min.css" rel="stylesheet">

  <link href="<?php echo base_url();?>fonts/css/font-awesome.min.css" rel="stylesheet">
  <link href="<?php echo base_url();?>css/animate.min.css" rel="stylesheet">

  <!-- Custom styling plus plugins -->
  <link href="<?php echo base_url();?>css/custom.css" rel="stylesheet">
  <link href="<?php echo base_url();?>css/icheck/flat/green.css" rel="stylesheet">

  <link href="<?php echo base_url();?>/js/datatables/jquery.dataTables.min.css" rel="stylesheet" type="text/css" />
  <link href="<?php echo base_url();?>js/datatables/buttons.bootstrap.min.css" rel="stylesheet" type="text/css" />
  <link href="<?php echo base_url();?>js/datatables/fixedHeader.bootstrap.min.css" rel="stylesheet" type="text/css" />
  <link href="<?php echo base_url();?>js/datatables/responsive.bootstrap.min.css" rel="stylesheet" type="text/css" />
  <link href="<?php echo base_url();?>js/datatables/scroller.bootstrap.min.css" rel="stylesheet" type="text/css" />
  <script src="<?php echo base_url();?>js/jquery.min.js"></script>
</head>
      <!-- page content -->
<div class="right_col" role="main">
  <div class="">
    <div class="page-title">
            <div class="title_left">
        <h3>
                    User Groups
                    
        </h3>
      </div>
      <div class="title_right" style="float: right !important;">
        <div class="col-md-2 col-sm-2 col-xs-12 form-group pull-right top_search" style="float: right !important;">
          <div class="input-group">
            <span class="buttons">
              <?php if($this->session->userdata('write')==1){?>
                <button class="btn btn-info btn-sm" type="button" id="add_new_btn" style="padding: none !important;">Add New User Group</button>
              <?php }else{ ?>
                <button class="btn btn-info btn-sm" type="button" id="add_new_btn" style="padding: none !important;" disabled>Add New User Group</button>
              <?php }?>
            </span>
          </div>
        </div>
      </div>
    </div>

    <div class="clearfix"></div>
        <div class="alert alert-danger alert-dismissable" id="errormsgDiv" style="display:none">
      <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
      <h4><i class="icon fa fa-ban"></i> Error!</h4>
      <span id="errormsg"></span>
    </div>

         <div class="alert alert-success alert-dismissable" id="succmsgDiv" style="display:none">
      <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
      <h4>  <i class="icon fa fa-check"></i> Success!</h4>
      <span id="succmsg"></span>
    </div>
          <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
              <div class="x_panel">
                <div class="x_title">
                  <h2>User Groups</h2>
                  <div class="clearfix"></div>
                </div>
                <div class="x_content">
                  <table id="datatable-buttons" class="table table-striped table-bordered">
                    <thead>
                      <tr>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th></th>
                      </tr>
                    </thead>
                  </table>
                </div>
              </div>
            </div>
              </div>
            </div>


<div class="row" id="admin_form_div" style="display:none;">
    <div class="col-md-12 col-sm-12 col-xs-12">

      <div class="x_panel">
                <div class="x_title">
          <h2>User Group</h2>
          <div class="clearfix"></div>
        </div>
                <div class="x_content">
          <br />
           <div class="panel-heading">
                                  <span class="error pull-right">* marked are required fields.</span>
                                </div>
          <form role="form" name="adminmaster" id="adminmaster" action="<?php echo site_url('/admin_master/admin_entry_save'); ?>" method="post" >
                  <input type="hidden" name="id" id="id" value="0" />
                 <div class="box-body">
                  <div class="form-group">
                  <div class="form-group">
                      <label>User Group</label>
                       <span class="error">*</span>
                      <input type='text' class="form-control" id='adminname' name='adminname' placeholder="Enter User Group Name">
                    </div>
                    
                  </div><!-- /.box-body -->

                 </div>
                  <div class="box-footer">
                    <button type="submit" id="adminsubmit" class="btn btn-primary">Submit</button>
                  </div>
                </form>
        </div>
      </div>
    </div>
  </div>  


              <?php $this->load->view('/common/footer');?>
            <!-- /page content -->
          </div>

        <script src="<?php echo base_url();?>js/bootstrap.min.js"></script>

        <!-- bootstrap progress js -->
        <script src="<?php echo base_url();?>js/progressbar/bootstrap-progressbar.min.js"></script>
        <script src="<?php echo base_url();?>js/nicescroll/jquery.nicescroll.min.js"></script>
        <!-- icheck -->
        <script src="<?php echo base_url();?>js/icheck/icheck.min.js"></script>

        <script src="<?php echo base_url();?>js/custom.js"></script>


        <!-- Datatables -->
        <!-- <script src="js/datatables/js/jquery.dataTables.js"></script>
  <script src="js/datatables/tools/js/dataTables.tableTools.js"></script> -->

        <!-- Datatables-->
        <script src="<?php echo base_url();?>js/datatables/jquery.dataTables.min.js"></script>
        <script src="<?php echo base_url();?>js/datatables/dataTables.bootstrap.js"></script>
        <script src="<?php echo base_url();?>js/datatables/dataTables.buttons.min.js"></script>
        <script src="<?php echo base_url();?>js/datatables/buttons.bootstrap.min.js"></script>
        <script src="<?php echo base_url();?>js/datatables/jszip.min.js"></script>
        <script src="<?php echo base_url();?>js/datatables/pdfmake.min.js"></script>
        <script src="<?php echo base_url();?>js/datatables/vfs_fonts.js"></script>
        <script src="<?php echo base_url();?>js/datatables/buttons.html5.min.js"></script>
        <script src="<?php echo base_url();?>js/datatables/buttons.print.min.js"></script>
        <script src="<?php echo base_url();?>js/datatables/dataTables.fixedHeader.min.js"></script>
        <script src="<?php echo base_url();?>js/datatables/dataTables.keyTable.min.js"></script>
        <script src="<?php echo base_url();?>js/datatables/dataTables.responsive.min.js"></script>
        <script src="<?php echo base_url();?>js/datatables/responsive.bootstrap.min.js"></script>
        <script src="<?php echo base_url();?>js/datatables/dataTables.scroller.min.js"></script>
        <script src="<?php echo base_url();?>js/jquery-validate/jquery.validate.min.js" type="text/javascript"></script>

        <!-- pace -->
        <script src="<?php echo base_url();?>js/pace/pace.min.js"></script>
        <script src="<?php echo base_url();?>public/js/admin_master.js"></script>
<script type="text/javascript">
  var write_access = <?=$this->session->userdata('write');?>;
  
  var delete_access = <?=$this->session->userdata('delete');?>;//alert(delete_access);
</script>
</body>

</html>
