<style>
    .error {color: #FF0000;}
    .no_padding{
		padding-left: 0px;
    }
    .custom_padding{
		padding-right: 0px;
		padding-left: 10px;
    }
	
    fieldset.scheduler-border {
		border: 1px groove #ddd !important;
		padding: 0 1.4em 1.4em 1.4em !important;
		margin: 0 0 1.5em 0 !important;
		-webkit-box-shadow:  0px 0px 0px 0px #000;
		box-shadow:  0px 0px 0px 0px #000;
	}
	
	.control-label{
		text-align: left !important;
	}
	.modal{
		height: 500px;
		overflow-y: auto;
	}
	.animate {
		-webkit-animation: animate_bg 5s;
		animation: animate_bg 5s;
		-webkit-animation-iteration-count: infinite;
		animation-iteration-count: infinite;
	}

	@keyframes animate_bg {
		0%   {color:red;}
		100% {color:blue;}
	}

	@keyframes animate_bg {
		0%   {color:red;}
		100% {color:blue;}
	}

	@-webkit-keyframes animate_bg {
		0%   {color:red;}
		100% {color:blue;}
	}
</style>

<!-- page content -->
<div class="right_col" role="main">
	<div class="">
		<div class="page-title">
            <div class="title_left">
				<h3>
                    User Access Rights
                    
				</h3>
			</div>
			<div class="title_right" style="">
				<div class="col-md-4 col-sm-4 col-xs-12 form-group pull-right top_search" style="float: right !important;">
					<div class="form-group">
						<select class="form-control select2" name="group" id="group" onChange="get_rights(this.value);">
							<option value="">Select User Group</option>
						</select>
					</div>
				</div>
			</div>
		</div>
		<div class="clearfix"></div>
		
		<div class="row">
			
			<div class="col-md-12 col-sm-12 col-xs-12">
				<div class="x_panel">
					<div class="x_title">
						<h2>User Access Rights</h2>
						
						<div class="clearfix"></div>
						<div class="alert alert-danger alert-dismissable" id="errormsgDiv" style="display:none">
							<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
							<h4><i class="icon fa fa-ban"></i> Error!</h4>
							<h2 id="errormsg"></h2>
						</div>

						<div class="alert alert-success alert-dismissable" id="succmsgDiv" style="display:none">
							<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
							<h4>  <i class="icon fa fa-check"></i> Success!</h4>
							<h6 id="succmsg"></h6>
						</div>
					</div>
					<div class="x_content" id="user_form_div" style="display:none;">
						<div class="form-group" id="alert"> </div>
						<div class="col-md-12 col-sm-12 col-xs-12" id="container">
							
						</div>
					</div>
				</div>
			</div>
			
		</div>
	</div>
	
	<script src="<?php echo base_url();?>js/bootstrap.min.js"></script>
	
	<!-- bootstrap progress js -->
	<script src="<?php echo base_url();?>js/progressbar/bootstrap-progressbar.min.js"></script>
	<script src="<?php echo base_url();?>js/nicescroll/jquery.nicescroll.min.js"></script>
	<!-- icheck -->
	<script src="<?php echo base_url();?>js/icheck/icheck.min.js"></script>
	<!-- tags -->
	<script src="js/tags/jquery.tagsinput.min.js"></script>
	<script src="<?php echo base_url();?>js/custom.js"></script>
	
	
	<!-- Datatables -->
	<!-- <script src="js/datatables/js/jquery.dataTables.js"></script>
	<script src="js/datatables/tools/js/dataTables.tableTools.js"></script> -->
	
	<!-- Datatables-->
	<script src="<?php echo base_url();?>js/datatables/jquery.dataTables.min.js"></script>
	<script src="<?php echo base_url();?>js/datatables/dataTables.bootstrap.js"></script>
	<script src="<?php echo base_url();?>js/datatables/dataTables.buttons.min.js"></script>
	<script src="<?php echo base_url();?>js/datatables/buttons.bootstrap.min.js"></script>
	<script src="<?php echo base_url();?>js/datatables/jszip.min.js"></script>
	<script src="<?php echo base_url();?>js/datatables/pdfmake.min.js"></script>
	<script src="<?php echo base_url();?>js/datatables/vfs_fonts.js"></script>
	<script src="<?php echo base_url();?>js/datatables/buttons.html5.min.js"></script>
	<script src="<?php echo base_url();?>js/datatables/buttons.print.min.js"></script>
	<script src="<?php echo base_url();?>js/datatables/dataTables.fixedHeader.min.js"></script>
	<script src="<?php echo base_url();?>js/datatables/dataTables.keyTable.min.js"></script>
	<script src="<?php echo base_url();?>js/datatables/dataTables.responsive.min.js"></script>
	<script src="<?php echo base_url();?>js/datatables/responsive.bootstrap.min.js"></script>
	<script src="<?php echo base_url();?>js/datatables/dataTables.scroller.min.js"></script>
	<script src="<?php echo base_url();?>js/jquery-validate/jquery.validate.min.js" type="text/javascript"></script>
	
	<script src="<?php echo base_url();?>/public/plugins/ckeditor/ckeditor.js"></script>
	<script src="<?php echo base_url();?>/public/plugins/ckeditor/js/sample.js"></script>
	
	<link type="text/css" href="<?php echo base_url();?>public/plugins/ckeditor/css/samples.css" />
	<link type="text/css" href="<?php echo base_url();?>public/plugins/ckeditor/toolbarconfigurator/lib/codemirror/neo.css" />
	
	<!-- daterangepicker -->
	<script type="text/javascript" src="<?php echo base_url();?>js/moment/moment.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>js/datepicker/daterangepicker.js"></script>
	
	<!-- pace -->
	<script src="<?php echo base_url();?>js/pace/pace.min.js"></script>	
	<script type="text/javascript">
		var write_access = <?=$this->session->userdata('write');?>;
		
		var delete_access = <?=$this->session->userdata('delete');?>;//alert(delete_access);
	</script>	
	<script src="<?php echo base_url();?>public/js/user_access.js"></script>
	