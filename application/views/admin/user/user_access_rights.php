<form role="form" action="" method="post" >
  <div class="alert alert-danger alert-dismissable" id="errormsgDiv" style="display:none">
      <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
      <h4><i class="icon fa fa-ban"></i> Error!</h4>
      <span id="errormsg"></span>
  </div>
  <div class="alert alert-success alert-dismissable" id="succmsgDiv" style="display:none">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
    <h4>  <i class="icon fa fa-check"></i> Success!</h4>
    <span id="success_msg"></span>
  </div>
  <div class="box-body">
    <table  class="table table-bordered table-striped" width="100%">
      <thead>
        <tr>
          <th width="20%"></th>
          <th width="20%">Menu </th>
          <th width="20%">Read</th>
          <th width="20%">Write</th>
          <th width="20%">Delete</th>
        </tr>
      </thead>
     <tbody>
        <?php
        foreach($navigation_menu as $key => $val)
        {                    
          $id         = $val['id'];
          $parent_id  = $val['id'];
        ?>
          <tr class="parenttr"  data-parent-id = "<?=$parent_id;?>">
            <td class="parenttd" width="20%">
              <span class="parent">
                <a class="expand"><img src="<?php echo base_url();?>public/img/plus.gif?>" alt="Expand"></a>
                <a class="minimize"><img src="<?php echo base_url();?>public/img/minus.gif?>" alt="Collapse"></a>
              </span>
            </td>
            <td>
                <?=$val['name'];?>
                <input type="hidden" name="id[]" value="<?=$id;?>">
            </td>
            <td>
              <?php 
              if($val['is_default'] == 'Y')
              {
                 $value1        = array(
                  'name'        => 'read['.$id.']',
                  'class'       => 'pread',
                  'value'       => 1,
                  'disabled'    => 'disabled',
                  'checked'     => TRUE,
                  'style'       => 'margin:10px',
                );
                 echo form_checkbox($value1);
              }
              else
              {
                $read           = array_key_exists($key, $admin_access_rights)  ? ($admin_access_rights[$key]['read'])  : 0;
            
                $flag           = ($read == '1') ? TRUE : FALSE ;
              
                $value1         = array(
                  'name'        => 'read['.$id.']',
                  'class'       => 'pread',
                  'value'       => 1,
                  'checked'     => $flag,
                  'style'       => 'margin:10px',
                );
                echo form_checkbox($value1);
              }
              ?>
            </td>
            <td>
              <?php 
              if($val['is_default'] == 'Y')
              {
                $value2         = array(
                  'name'        => 'write['.$id.']',
                  'class'       => 'pwrite',
                  'value'       => 1,
                  'disabled'    => 'disabled',
                  'checked'     => TRUE,
                  'style'       => 'margin:10px',
                );
                echo form_checkbox($value2);
              }
              else
              {
                $write          = array_key_exists($key, $admin_access_rights)?($admin_access_rights[$key]['write']) : 0;

                $flag1          = ($write == '1') ? TRUE : FALSE ;
                 
                $value2         = array(
                  'name'        => 'write['.$id.']',
                  'class'       => 'pwrite',
                  'value'       => 1,
                  'checked'     => $flag1,
                  'style'       => 'margin:10px',
                );
                echo form_checkbox($value2);
              }
              ?>
            </td>
            <td>
              <?php
              if($val['is_default'] == 'Y')
              {
                 $value2        = array(
                  'name'        => 'delete['.$id.']',
                  'class'       => 'pdelete',
                  'value'       => 1,
                  'disabled'    => 'disabled',
                  'checked'     => TRUE,
                  'style'       => 'margin:10px',
                );
                echo form_checkbox($value2);
              }
              else
              {
                $delete         = array_key_exists($key, $admin_access_rights)? ($admin_access_rights[$key]['delete']) : 0;
              
                $flag2          = ($delete == '1') ? TRUE : FALSE ;
             
                $value3         = array(
                  'name'        => 'delete['.$id.']',
                    'class'     => 'pdelete',
                    'value'     => 1,
                    'checked'   => $flag2,
                    'style'     => 'margin:10px',
                );

              echo form_checkbox($value3);
            }
              ?>
            </td>
          </tr>
        <?php

          if(isset($val['child_menus']) && count($val['child_menus']) > 0)
          {
            foreach($val['child_menus'] as $child_key => $child_value)
            {
                $child_id         = $child_value['id'];
                  
        ?>
        
          <tr class="childtr hide" data-parent-id = "<?=$parent_id;?>">
            <td></td>
            <td class="childtd"><span class="child"><?=$child_value['name'];?><input type="hidden" name="id[]" value="<?=$child_id;?>"></span></td>
            <td>
              <?php

                if($child_value['is_default'] == 'Y')
                {
                   $value1        = array(
                    'name'        => 'read['.$child_id.']',
                    'class'       => 'cread',
                    'value'       => 1,
                    'disabled'    => 'disabled',
                    'checked'     => TRUE,
                    'style'       => 'margin:10px',
                  );
                   echo form_checkbox($value1);
                }
                else
                {
                  $read           = array_key_exists($child_id, $admin_access_rights)  ? ($admin_access_rights[$child_id]['read'])  : $read = 0;
              
                  $flag3          = ($read == '1') ? TRUE : FALSE;
                  
                  $value1         = array(
                    'name'        => 'read['.$child_id.']',
                    'class'       => 'cread',
                   //'value'      => 1,
                    'value'       => $read,
                    'checked'     => $flag3,
                    'style'       => 'margin:10px',
                  );
                  echo form_checkbox($value1);
                }
              ?>
            </td>
            <td>
              <?php 
                if($child_value['is_default'] == 'Y')
                {
                   $value2        = array(
                    'name'        => 'write['.$child_id.']',
                    'class'       => 'cwrite',
                    'value'       => 1,
                    'disabled'    => 'disabled',
                    'checked'     => TRUE,
                    'style'       => 'margin:10px',
                  );
                   echo form_checkbox($value2);
                }
                else
                {
                  $write          =  array_key_exists($child_id, $admin_access_rights)?($admin_access_rights[$child_id]['write']) : 0;

                  $flag4          = ($write == '1')? TRUE : FALSE;
                            
                  $value2         = array(
                    'name'        => 'write['.$child_id.']',
                    'class'       => 'cwrite',
                   // 'value'       => 1,
                    'value'       => $write,
                    'checked'     => $flag4,
                    'style'       => 'margin:10px',
                  );
                  echo form_checkbox($value2);
                }
              ?>
            </td>
            <td>
              <?php 
                if($child_value['is_default'] == 'Y')
                {
                  $value3         = array(
                    'name'        => 'delete['.$child_id.']',
                    'class'       => 'cdelete',
                    'value'       => 1,
                    'disabled'    => 'disabled',
                    'checked'     => TRUE,
                    'style'       => 'margin:10px',
                  );
                   echo form_checkbox($value3);
                }
                else
                {
                  $delete         = array_key_exists($child_id, $admin_access_rights)?( $admin_access_rights[$child_id]['delete']) : 0;
                  
                  $flag5          = ($delete == '1') ? TRUE : FALSE;
                       
                  $value3         = array(
                    'name'        => 'delete['.$child_id.']',
                    'class'       => 'cdelete',
                   //'value'       => 1,
                    'value'       => $delete,
                    'checked'     => $flag5,
                    'style'       => 'margin:10px',
                  );
                  echo form_checkbox($value3);
                }
              ?>
            </td>
          </tr>
        <?php
           }
          }
        }
        ?>
      </tbody>
    </table>
  </div>
  <div class="box-footer">
    <?php if($this->session->userdata('write') == 1){ ?>
      <button type="button" id="submit" class="btn btn-primary">Submit</button>
    <?php } else{ ?>
      <button type="button" disabled id="submit" class="btn btn-primary">Submit</button>
    <?php } ?>
  </div>
</form>
<script type="text/javascript">
$(document).ready(function(){
  var unchecklength;
  var length;

  $('.minimize').css('display','none');

  $(".parenttd .expand").click(function() {
    var parent_id         = $(this).parents('tr').attr('data-parent-id');
    $('.childtr[data-parent-id = '+parent_id+']').removeClass('hide');
    $(this).hide();
    $(this).parent().find(".minimize").show();
  });

  $(".parenttd .minimize").click(function() {
    var parent_id         = $(this).parents('tr').attr('data-parent-id');
    $('.childtr[data-parent-id = '+parent_id+']').addClass('hide');
    $(this).hide();
    $(this).parent().find(".expand").show();
  });

  $('.parenttr .pread').click(function(){
    var parent_id       = $(this).parents('tr').attr('data-parent-id');
    $('.childtr[data-parent-id = '+parent_id+']').find(".cread").prop('checked', this.checked);

    if($(this).prop('checked') == false)
    {
      $('tr[data-parent-id = '+parent_id+']').find('input[type="checkbox"]:checked').prop('checked',false);
    }
  });

  $('.parenttr .pwrite').click(function(){
    var parent_id       = $(this).parents('tr').attr('data-parent-id');
    $('.childtr[data-parent-id = '+parent_id+']').find(".cwrite").prop('checked', this.checked); 
    
    if($(this).prop('checked') == true)
    {
      $('tr[data-parent-id = '+parent_id+']').find('.pread').prop('checked',true);
      $('.childtr[data-parent-id = '+parent_id+']').find(".cread").prop('checked', this.checked); 
    }
  });

  $('.parenttr .pdelete').click(function(){
    var parent_id       = $(this).parents('tr').attr('data-parent-id');
    $('.childtr[data-parent-id = '+parent_id+']').find(".cdelete").prop('checked', this.checked); 
    
    if($(this).prop('checked') == true)
    {
      $('tr[data-parent-id = '+parent_id+']').find('.pread').prop('checked',true);
      $('.childtr[data-parent-id = '+parent_id+']').find(".cread").prop('checked', this.checked); 
    }
  });

  $('.childtr .cread').click(function(){

    var parent_id       = $(this).parents('tr').attr('data-parent-id');
    var totalCheckboxes = $('.childtr[data-parent-id = '+parent_id+']').find('.cread').length;

    length              = $('.childtr[data-parent-id = '+parent_id+']').find('input:checkbox[class=cread]:checked').length;
    unchecklength       = $('.childtr[data-parent-id = '+parent_id+']').find('input:checkbox[class=cread]:not(:checked)').length;
    
    if(totalCheckboxes == length)
    {
      $('.parenttr[data-parent-id = '+parent_id+']').find(".pread").prop('checked', true);
    }
    else
    {
      if(totalCheckboxes == unchecklength)
      {
        $('.parenttr[data-parent-id = '+parent_id+']').find(".pread").prop('checked', false);
      }
      else
      {
        $('.parenttr[data-parent-id = '+parent_id+']').find(".pread").prop('checked', true);
      }      
    }

    if($(this).prop('checked') == false && $(this).parent().parent().find(".cwrite").prop('checked') == true)
    {
      $(this).parent().parent().find(".cwrite").trigger('click');
    }

    if($(this).prop('checked') == false && $(this).parent().parent().find(".cdelete").prop('checked') == true)
    {
      $(this).parent().parent().find(".cdelete").trigger('click');
    }

  });

  $('.childtr .cwrite').click(function(){

    var parent_id       = $(this).parents('tr').attr('data-parent-id');
    var totalCheckboxes = $('.childtr[data-parent-id = '+parent_id+']').find('.cwrite').length;
    
    length              = $('.childtr[data-parent-id = '+parent_id+']').find('input:checkbox[class=cwrite]:checked').length;
    unchecklength       = $('.childtr[data-parent-id = '+parent_id+']').find('input:checkbox[class=cwrite]:not(:checked)').length;
 
    if(totalCheckboxes  == length)
    {
      $('.parenttr[data-parent-id = '+parent_id+']').find(".pwrite").prop('checked', true);
    }
    else
    {
      if(totalCheckboxes == unchecklength)
      {
        $('.parenttr[data-parent-id = '+parent_id+']').find(".pwrite").prop('checked', false);
      }
      else
      {
        $('.parenttr[data-parent-id = '+parent_id+']').find(".pwrite").prop('checked', true);
      }
    }

    if($(this).prop('checked') == true && $(this).parent().parent().find(".cread").prop('checked') == false)
    {
      $(this).parent().parent().find(".cread").trigger('click');
    }

  });

  $('.childtr .cdelete').click(function(){
    var child_id          = $(this).parents('tr').attr('data-parent-id');
    var totalCheckboxes   = $('.childtr[data-parent-id = '+child_id+']').find('.cdelete').length;
    
    length                = $('.childtr[data-parent-id = '+child_id+']').find('input:checkbox[class=cdelete]:checked').length;
    unchecklength         = $('.childtr[data-parent-id = '+child_id+']').find('input:checkbox[class=cdelete]:not(:checked)').length;

    if(totalCheckboxes    == length)
    {
      $('.parenttr[data-parent-id = '+child_id+']').find(".pdelete").prop('checked', true);
    }
    else
    {
      if(totalCheckboxes  == unchecklength)
      {
        $('.parenttr[data-parent-id = '+child_id+']').find(".pdelete").prop('checked', false);
      }
      else
      {
        $('.parenttr[data-parent-id = '+child_id+']').find(".pdelete").prop('checked', true);
      }
    }

    if($(this).prop('checked') == true && $(this).parent().parent().find(".cread").prop('checked') == false)
    {
      $(this).parent().parent().find(".cread").trigger('click');
    }

  });

  $("#submit").click(function(){

    var disabled_read     = $(".pread" ).attr( "name" );
    var disabled_write    = $(".pwrite").attr('name');
    var disabled_delete   = $(".pdelete").attr('name');

    var post_data         = $( "form" ).serialize() + "&"+disabled_read+"=1" + "&"+disabled_write+"=1" + "&"+disabled_delete+"=1";
    var group             = $('#group').val();
    $.ajax({
      type: "POST",
      url: "<?php echo base_url();?>user_access/update_admin_access_rights/"+group,
      data: post_data,//$( "form" ).serialize(),
      dataType:'json',
      success : function(response){

        if(response.flag == "success")
        {
          $("#succmsgDiv").show();

          if($("#action").val() == 'add')
          {
            $("#success_msg").html("Access Rights Saved Successfully");
            $("#action").val('edit');
            $('html, body').animate({
              scrollTop: $("#succmsgDiv").offset().top
            }, 1000);         
          }
          else
          {
            $("#success_msg").html("Access Rights Updated Successfully");
          }

          $("#succmsgDiv").fadeOut(4000);
          $('html, body').animate({
              scrollTop: $("#succmsgDiv").offset().top
          }, 1000);
        } 
      }     
    });
  });
});
</script>