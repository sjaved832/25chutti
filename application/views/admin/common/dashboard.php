<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>css/admin/maps/jquery-jvectormap-2.0.3.css" />
<link href="<?php echo base_url();?>css/admin/floatexamples.css" rel="stylesheet" type="text/css" />
<script src="<?php echo base_url();?>js/admin/nprogress.js"></script>
<!-- page content -->
<div class="right_col" role="main">


	<div class="row">
	</div>
	<br />

	<div class="row">
	</div>


	<div class="row">
		<div class="col-md-8 col-sm-8 col-xs-12">
			<div class="row">
			</div>
			<div class="row">
			</div>
		</div>

	</div>
	<script src="<?php echo base_url(); ?>js/bootstrap.min.js"></script>
	<!-- bootstrap progress js -->
	<script src="<?php echo base_url(); ?>js/admin/progressbar/bootstrap-progressbar.min.js"></script>
	<script src="<?php echo base_url(); ?>js/admin/nicescroll/jquery.nicescroll.min.js"></script>
	<!-- icheck -->
	<script src="<?php echo base_url(); ?>js/admin/icheck/icheck.min.js"></script>
	<!-- daterangepicker -->
	<script type="text/javascript" src="<?php echo base_url(); ?>js/admin/moment/moment.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url(); ?>js/admin/datepicker/daterangepicker.js"></script>
	<!-- chart js -->
	<script src="<?php echo base_url(); ?>js/admin/chartjs/chart.min.js"></script>

	<script src="<?php echo base_url(); ?>js/admin/custom.js"></script>

	<!-- flot js -->
	<!--[if lte IE 8]><script type="text/javascript" src="js/excanvas.min.js"></script><![endif]-->
	<script type="text/javascript" src="<?php echo base_url(); ?>js/admin/flot/jquery.flot.js"></script>
	<script type="text/javascript" src="<?php echo base_url(); ?>js/admin/flot/jquery.flot.pie.js"></script>
	<script type="text/javascript" src="<?php echo base_url(); ?>js/admin/flot/jquery.flot.orderBars.js"></script>
	<script type="text/javascript" src="<?php echo base_url(); ?>js/admin/flot/jquery.flot.time.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url(); ?>js/admin/flot/date.js"></script>
	<script type="text/javascript" src="<?php echo base_url(); ?>js/admin/flot/jquery.flot.spline.js"></script>
	<script type="text/javascript" src="<?php echo base_url(); ?>js/admin/flot/jquery.flot.stack.js"></script>
	<script type="text/javascript" src="<?php echo base_url(); ?>js/admin/flot/curvedLines.js"></script>
	<script type="text/javascript" src="<?php echo base_url(); ?>js/admin/flot/jquery.flot.resize.js"></script>


	<!-- worldmap -->
	<script type="text/javascript" src="<?php echo base_url(); ?>js/admin/maps/jquery-jvectormap-2.0.3.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url(); ?>js/admin/maps/gdp-data.js"></script>
	<script type="text/javascript" src="<?php echo base_url(); ?>js/admin/maps/jquery-jvectormap-world-mill-en.js"></script>
	<script type="text/javascript" src="<?php echo base_url(); ?>js/admin/maps/jquery-jvectormap-us-aea-en.js"></script>
	<!-- pace -->
	<script src="<?php echo base_url(); ?>js/admin/pace/pace.min.js"></script>


	<!-- /datepicker -->
<!-- /footer content -->
