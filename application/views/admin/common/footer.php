<!-- footer content -->
<footer>
	<div class="copyright-info">
		<p class="pull-right"><a href="<?php echo base_url();?>">25 Chutti !</a>
		</p>
	</div>
	<div class="clearfix"></div>
</footer>
<!-- /footer content -->

</div>
<!-- /page content -->
</div>

</div>

<div id="custom_notifications" class="custom-notifications dsp_none">
	<ul class="list-unstyled notifications clearfix" data-tabbed_notifications="notif-group">
	</ul>
	<div class="clearfix"></div>
	<div id="notif-group" class="tabbed_notifications"></div>
</div>
<script src="<?php echo base_url();?>public/plugins/select2/select2.js"></script>

<script type="text/javascript">
	$(document).ready(function(){
		$(".select2").select2();
	});
</script>


</body>

</html>
