<!DOCTYPE html>
<html lang="en">

	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<!-- Meta, title, CSS, favicons, etc. -->
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<link rel="icon" href="<?php echo base_url();?>images/favicon.png" type="image/png">
		<title>25 Chutti Admin !</title>

		<!-- Bootstrap core CSS -->

		<link href="<?php echo base_url();?>css/bootstrap.min.css" rel="stylesheet">

		<link href="<?php echo base_url();?>css/font-awesome.min.css" rel="stylesheet">
		<link href="<?php echo base_url();?>css/animate.min.css" rel="stylesheet">
		<link type="text/css" href="<?php echo base_url();?>public/plugins/ckeditor/css/samples.css" />
		<link type="text/css" href="<?php echo base_url();?>public/plugins/ckeditor/toolbarconfigurator/lib/codemirror/neo.css" />
		<!-- Custom styling plus plugins -->
		<link href="<?php echo base_url();?>css/admin/custom.css" rel="stylesheet">
		<link href="<?php echo base_url();?>css/admin/icheck/flat/green.css" rel="stylesheet">

		<link href="<?php echo base_url();?>js/admin/datatables/jquery.dataTables.min.css" rel="stylesheet" type="text/css" />
		<link href="<?php echo base_url();?>js/admin/datatables/buttons.bootstrap.min.css" rel="stylesheet" type="text/css" />
		<link href="<?php echo base_url();?>js/admin/datatables/fixedHeader.bootstrap.min.css" rel="stylesheet" type="text/css" />
		<link href="<?php echo base_url();?>js/admin/datatables/responsive.bootstrap.min.css" rel="stylesheet" type="text/css" />
		<link href="<?php echo base_url();?>js/admin/datatables/scroller.bootstrap.min.css" rel="stylesheet" type="text/css" />

		<script type="text/javascript" src="<?php echo base_url();?>js/jquery-2.2.4.min.js"></script>
		<!--<link href="<?php //echo base_url();?><!--css/select2.min.css" rel="stylesheet" type="text/css" />-->
		<!--<link href="<?php echo base_url();?>public/plugins/select2/select2.css" rel="stylesheet" type="text/css" />
		<link href="<?php echo base_url();?>public/plugins/select2/select2-bootstrap.css" rel="stylesheet" type="text/css" />-->
		<script>
			var base_url = "<?php echo base_url();?>";
			var site_url = "<?php echo site_url();?>";
		</script>
	</head>


	<body class="nav-md">

		<div class="container body">


			<div class="main_container">

				<div class="col-md-3 left_col">
					<div class="left_col scroll-view">

						<div class="navbar nav_title" style="border: 0;">
								<a href="<?php echo base_url();?>admin/dashboard" class="site_title"><span>25 Chutti<!--<img src="<?php echo base_url();?>images/logo.png">--></span></a>
						</div>
						<div class="clearfix"></div>

						<!-- menu prile quick info -->
						<div class="profile">
							<div class="profile_pic">
								<img src="<?php echo base_url();?>img/img.jpg" alt="..." class="img-circle profile_img">
							</div>
							<div class="profile_info">
								<span>Welcome,</span>
								<h2><?php //echo $this->session->userdata('username'); ?></h2>
							</div>
						</div>
						<!-- /menu prile quick info -->

						<!-- sidebar menu -->
						<div id="sidebar-menu" class="main_menu_side hidden-print main_menu">

							<div class="menu_section">
								<h3>MENU</h3>
								<ul class="nav side-menu">

									<li class="current-page"><a href="<?php echo base_url();?>admin/dashboard">Dashboard</a></li>
							
									<li><a href="<?php echo base_url();?>admin/hotel">Hotel</a></li>

									<li><a href="<?php echo base_url();?>admin/facility">Facility</a></li>

									<li><a href="<?php echo base_url();?>admin/packages">Package</a></li>

									<li><a href="<?php echo base_url();?>admin/state">State</a></li>
										
									<li><a href="<?php echo base_url();?>admin/city">City</a></li>
								</ul>
							</div>
						</div>
						<!-- /sidebar menu -->

						<!-- /menu footer buttons -->
						<div class="sidebar-footer hidden-small">
							<a data-toggle="tooltip" data-placement="top" title="Logout" href="<?php echo base_url();?>auth/logout">
								<span class="glyphicon glyphicon-off" aria-hidden="true"></span>
							</a>
						</div>
						<!-- /menu footer buttons -->
					</div>
				</div>

				<!-- top navigation -->
				<div class="top_nav">

					<div class="nav_menu">
						<nav class="" role="navigation">
							<div class="nav toggle">
								<a id="menu_toggle"><i class="fa fa-bars"></i></a>
							</div>

							<ul class="nav navbar-nav navbar-right">
								<li class="">
									<a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
										<img src="<?php echo base_url();?>img/img.jpg" alt="">
										<span class=" fa fa-angle-down"></span>
									</a>
									<ul class="dropdown-menu dropdown-usermenu animated fadeInDown pull-right">
										<li><a href="<?php echo base_url();?>auth/logout"><i class="fa fa-sign-out pull-right"></i> Log Out</a>
										</li>
									</ul>
								</li>

							</ul>
						</nav>
					</div>

				</div>
			<!-- /top navigation -->

<script type="text/javascript">

	function gotopage(url){
		window.location.replace(url);
		//alert(url)
	}
</script>
