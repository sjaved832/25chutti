
        <!-- Contact Area Start Here -->
        <section class="s-space-bottom-full bg-accent-shadow-body on-how">
            <div class="container">
                <div class="breadcrumbs-area">
                    <ul>
                        <li><a href="#">Home</a> -</li>
                        <li class="active">Contact Page</li>
                    </ul>
                </div>
            </div>
            <div class="container">
                <div class="row">
                    <div class="col-lg-9 col-md-8 col-sm-8 col-xs-12">
                        <div class="gradient-wrapper mb--xs">
                            <div class="gradient-title">
                                <h2>Contact With us</h2>
                            </div>
                            <div class="contact-layout1 gradient-padding">
                                <div class="google-map-area">
                                    <div id="googleMap" style="width:100%; height:400px;"></div>
                                </div>
                                <p>If you did not find the answer to your question or problem, please get in touch with us using the form below and we will respond to your message as soon as possible.</p>
                                <form id="contact-form" class="contact-form">
                                    <fieldset>
                                        <div class="row">
                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                <div class="form-group">
                                                    <input type="text" placeholder="Your Name" class="form-control" name="name" id="form-name" data-error="Name field is required" required>
                                                    <div class="help-block with-errors"></div>
                                                </div>
                                            </div>
                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                <div class="form-group">
                                                    <input type="email" placeholder="Your E-mail" class="form-control" name="email" id="form-email" data-error="Email field is required" required>
                                                    <div class="help-block with-errors"></div>
                                                </div>
                                            </div>
                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                <div class="form-group">
                                                    <input type="text" placeholder="Subject" class="form-control" name="subject" id="form-subject" data-error="Subject field is required" required>
                                                    <div class="help-block with-errors"></div>
                                                </div>
                                            </div>
                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                <div class="form-group">
                                                    <textarea placeholder="Message" class="textarea form-control" name="message" id="form-message" rows="7" cols="20" data-error="Message field is required" required></textarea>
                                                    <div class="help-block with-errors"></div>
                                                </div>
                                            </div>
                                            <div class="col-lg-4 col-md-4 col-sm-6 col-sm-12">
                                                <div class="form-group">
                                                    <button type="submit" class="cp-default-btn-sm">Send Message</button>
                                                </div>
                                            </div>
                                            <div class="col-lg-8 col-md-8 col-sm-6 col-sm-12">
                                                <div class='form-response'></div>
                                            </div>
                                        </div>
                                    </fieldset>
                                </form>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                        <div class="sidebar-item-box">
                            <ul class="sidebar-more-option">
                                <li>
                                    <a href="post-ad.html"><img src="<?php echo base_url();?>img/banner/more1.png" alt="more" class="img-responsive">Post a Free Ad</a>
                                </li>
                                <li>
                                    <a href="#"><img src="<?php echo base_url();?>img/banner/more2.png" alt="more" class="img-responsive">Manage Product</a>
                                </li>
                                <li>
                                    <a href="favourite-ad-list.html"><img src="<?php echo base_url();?>img/banner/more3.png" alt="more" class="img-responsive">Favorite Ad list</a>
                                </li>
                            </ul>
                        </div>
                        
                    </div>
                </div>
            </div>
        </section>
        <!-- Contact Area End Here -->
     
    <!-- jquery-->
    <script src="<?php echo base_url();?>js/jquery-2.2.4.min.js" type="text/javascript"></script>
    <!-- Plugins js -->
    <script src="<?php echo base_url();?>js/plugins.js" type="text/javascript"></script>
    <!-- Bootstrap js -->
    <script src="<?php echo base_url();?>js/bootstrap.min.js" type="text/javascript"></script>
    <!-- WOW JS -->
    <script src="<?php echo base_url();?>js/wow.min.js"></script>
    <!-- Owl Cauosel JS -->
    <script src="<?php echo base_url();?>vendor/OwlCarousel/owl.carousel.min.js" type="text/javascript"></script>
    <!-- Meanmenu Js -->
    <script src="<?php echo base_url();?>js/jquery.meanmenu.min.js" type="text/javascript"></script>
    <!-- Srollup js -->
    <script src="<?php echo base_url();?>js/jquery.scrollUp.min.js" type="text/javascript"></script>
    <!-- jquery.counterup js -->
    <script src="<?php echo base_url();?>js/jquery.counterup.min.js"></script>
    <script src="<?php echo base_url();?>js/waypoints.min.js"></script>
    <!-- Select2 Js -->
    <script src="<?php echo base_url();?>js/select2.min.js" type="text/javascript"></script>
    <!-- Isotope js -->
    <script src="<?php echo base_url();?>js/isotope.pkgd.min.js" type="text/javascript"></script>
    <!-- Google Map js -->
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBgREM8KO8hjfbOC0R_btBhQsEQsnpzFGQ"></script>
    <!-- Validator js -->
    <script src="<?php echo base_url();?>js/validator.min.js" type="text/javascript"></script>
    <!-- Custom Js -->
    <script src="<?php echo base_url();?>js/main.js" type="text/javascript"></script>
	<script src="<?php echo base_url();?>js/modernizr-2.8.3.min.js"></script>
